package facci.pm.velezbriones.misprimerosacordes21;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.UUID;

import facci.pm.velezbriones.misprimerosacordes21.Clases.Sesion;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.Usuario;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PrincipalActivity extends AppCompatActivity implements View.OnClickListener {

    private Sesion sesion;
    private EditText nombre;
    private TextView guardar;
    //private Button guardar;
    private String numero = "";
    private CardView CVUno, CVDos, CVTres, CVCuatro, CVCinco, CVSeis,
            CVSiete, CVOcho, CVNueve, CVDiez, CVOnce, CVDoce, CVTrece, CVCatorce, CVQuince;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference, databaseReference1;
    private int entero = 10, contadorPrincipal;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_principal);

//        recibirDatos();

        int i = getIntent().getIntExtra("a", 0);
        sesion = new Sesion(this);

            if (sesion.isLoggedIn()) {
                 if (i == 0) {
                    startActivity(new Intent(this, MainActivity.class));
                    finish();
                }else {

                 }
            }else {
             //   HomeAlert();
                InitialView();
            }

    }

    @Override
    protected void onResume() {
        super.onResume();
        InitialView();
    }

    private void InitialView(){
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("USUARIOS");

        databaseReference1 = firebaseDatabase.getReference("ESCRIBE_LOGROS_MENORES_P");

        nombre = (EditText) findViewById(R.id.TXTNombreS);
        guardar = (TextView) findViewById(R.id.BTNGuardarS);
        guardar.setOnClickListener(this);
        //busca ese enable de a verga, no eniendo, tu nada entiendes chch, pon ctrl + f
        CVUno = (CardView) findViewById(R.id.Uno);
        CVDos = (CardView) findViewById(R.id.Dos);
        CVTres = (CardView) findViewById(R.id.Tres);
        CVCuatro = (CardView) findViewById(R.id.Cuatro);
        CVCinco = (CardView) findViewById(R.id.Cinco);
        CVSeis = (CardView) findViewById(R.id.Seis);
        CVSiete = (CardView) findViewById(R.id.Siete);
        CVOcho = (CardView) findViewById(R.id.Ocho);
        CVNueve = (CardView) findViewById(R.id.Nueve);
        CVDiez = (CardView) findViewById(R.id.Diez);
        CVOnce = (CardView) findViewById(R.id.Once);
        CVDoce = (CardView) findViewById(R.id.Doce);
        CVTrece = (CardView) findViewById(R.id.Trece);
        CVCatorce = (CardView) findViewById(R.id.Catorce);
        CVQuince = (CardView) findViewById(R.id.Quince);

        CVUno.setOnClickListener(this);
        CVDos.setOnClickListener(this);
        CVTres.setOnClickListener(this);
        CVCuatro.setOnClickListener(this);
        CVCinco.setOnClickListener(this);
        CVSeis.setOnClickListener(this);
        CVSiete.setOnClickListener(this);
        CVOcho.setOnClickListener(this);
        CVNueve.setOnClickListener(this);
        CVDiez.setOnClickListener(this);
        CVOnce.setOnClickListener(this);
        CVDoce.setOnClickListener(this);
        CVTrece.setOnClickListener(this);
        CVCatorce.setOnClickListener(this);
        CVQuince.setOnClickListener(this);



    }

   /* private void HomeAlert(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View primera = layoutInflater.inflate(R.layout.primera_informacion, null);
        builder.setPositiveButton("ENTENDIDO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();


            }
        });
        builder.setView(primera);
        builder.show();

    }*/

    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.BTNGuardarS:
                if (isOnlineNet()==true){
                if (numero.isEmpty()){
                    Toast.makeText(this, "ELIGE UN ICONO", Toast.LENGTH_SHORT).show();

                }else {
                    if (sesion.isLoggedIn()) {
                        SharedPreferences sharedPreferences = getSharedPreferences(sesion.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.clear();
                        editor.commit();

                        //actualizar notas menores, el nombre y la foto
                        HashMap<String, Object> objectHashMapUdate = new HashMap<>();
                        objectHashMapUdate.put("enombreMenor", nombre.getText().toString().trim());
                        objectHashMapUdate.put("enombreMenorImg", numero.trim());
                        databaseReference1.child(getIntent().getStringExtra("id")).updateChildren(objectHashMapUdate);


                        //actualizar usuario
                        HashMap<String, Object> objectHashMap = new HashMap<>();
                        objectHashMap.put("nombre", nombre.getText().toString().trim());
                        objectHashMap.put("foto", numero);
                        databaseReference.child(getIntent().getStringExtra("id")).updateChildren(objectHashMap);



                        sesion.storeUserName(nombre.getText().toString().trim(), numero, getIntent().getStringExtra("id"));
                        startActivity(new Intent(this, MainActivity.class));
                        finish();
                    } else {
                        final String id = UUID.randomUUID().toString();
                        Usuario usuario = new Usuario();
                        usuario.setNombre(nombre.getText().toString().trim());
                        usuario.setFoto(numero);
                        usuario.setId(id);
                        databaseReference.child(id).setValue(usuario).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                sesion.storeUserName(nombre.getText().toString().trim(), numero, id);
                                startActivity(new Intent(PrincipalActivity.this, MainActivity.class));
                                finish();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                            }
                        });
                    }
                }
                }else
                    Toast.makeText(this, R.string.sinaccesoainternet, Toast.LENGTH_SHORT).show();
                break;
            case R.id.Uno:
                numero = "1";
                CVUno.setCardElevation(entero);
                break;
            case R.id.Dos:
                numero = "2";

                break;
            case R.id.Tres:
                numero = "3";
                break;
            case R.id.Cuatro:
                numero = "4";
                break;
                case R.id.Cinco:
                    numero = "5";
                break;
            case R.id.Seis:
                numero = "6";
                break;
            case R.id.Siete:
                numero = "7";
                break;
            case R.id.Ocho:
                numero = "8";
                break;
            case R.id.Nueve:
                numero = "9";
                break;
            case R.id.Diez:
                numero = "10";
                break;
            case R.id.Once:
                numero = "11";
                break;
            case R.id.Doce:
                numero = "12";
                break;
            case R.id.Trece:
                numero = "13";
                break;
            case R.id.Catorce:
                numero = "14";
                break;
            case R.id.Quince:
                numero = "15";
                break;

        }
    }
    //metodo de tipo booleano para comprobar la conexion a internet
    public Boolean isOnlineNet() {
        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");
            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(PrincipalActivity.this, MainActivity.class));
        finish();
    }

    private void recibirDatos(){
        Bundle extras = getIntent().getExtras();
        String d1 = extras.getString("a");
        nombre.setText(d1);
    }
}
