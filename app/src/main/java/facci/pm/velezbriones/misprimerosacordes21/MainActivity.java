package facci.pm.velezbriones.misprimerosacordes21;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.HashMap;

import facci.pm.velezbriones.misprimerosacordes21.AdivinaLaNota.AdivinaLaNota;
import facci.pm.velezbriones.misprimerosacordes21.Clases.Sesion;
import facci.pm.velezbriones.misprimerosacordes21.Desarrolladores.Desarrolladores;
import facci.pm.velezbriones.misprimerosacordes21.EscribeLaNota.EscribeLaNota;
import facci.pm.velezbriones.misprimerosacordes21.EscuchaNotasMusicales.EscuchaNotasMusicales;
import facci.pm.velezbriones.misprimerosacordes21.Logros.VerLogros;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private LinearLayout escucha, escribe, adivina, desarroladores, logros, editPerfil;
    private TextView name;
    private ImageView userfoto, verPerfil;
    private Sesion sesion;
    private HashMap<String, String> user;

//cambiar
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //de aqui
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        //hasta aqui


        setContentView(R.layout.activity_main);


        sesion = new Sesion(this);
        user = sesion.LoggedInUser();
        name = (TextView)findViewById(R.id.UserName);
        name.setText(user.get(sesion.USER_NAME));
        userfoto = (ImageView)findViewById(R.id.ImgUser);


        int numero = Integer.valueOf(user.get(sesion.USER_ICONO));
        if (numero == 1){
            userfoto.setImageResource(R.drawable.fantasma1);
        }if (numero == 2){
            userfoto.setImageResource(R.drawable.fantasma2);
        }if (numero == 3){
            userfoto.setImageResource(R.drawable.fantasma3);
        }if (numero == 4){
            userfoto.setImageResource(R.drawable.fantasma4);
        }if (numero == 5){
            userfoto.setImageResource(R.drawable.fantasma5);
        }if (numero == 6){
            userfoto.setImageResource(R.drawable.fantasma6);
        }if (numero == 7){
            userfoto.setImageResource(R.drawable.fantasma7);
        }if (numero == 8){
            userfoto.setImageResource(R.drawable.fantasma8);
        }if (numero == 9){
            userfoto.setImageResource(R.drawable.fantasma9);
        }if (numero == 10){
            userfoto.setImageResource(R.drawable.fantasma10);
        }if (numero == 11){
            userfoto.setImageResource(R.drawable.fantasma11);
        }if (numero == 12){
            userfoto.setImageResource(R.drawable.fantasma12);
        }if (numero == 13){
            userfoto.setImageResource(R.drawable.fantasma13);
        }if (numero == 14){
            userfoto.setImageResource(R.drawable.fantasma14);
        }if (numero == 15){
            userfoto.setImageResource(R.drawable.fantasma15);
        }





        escucha = (LinearLayout)findViewById(R.id.LLEscucha);
        escribe = (LinearLayout)findViewById(R.id.LLEscribe);
        adivina = (LinearLayout)findViewById(R.id.LLAdivina);
        desarroladores = (LinearLayout)findViewById(R.id.LLDesarrolladores);
        editPerfil = (LinearLayout)findViewById(R.id.editt);
        logros = (LinearLayout)findViewById(R.id.LLLogros);

        verPerfil = (ImageView)findViewById(R.id.IMGPerfil);

        escucha.setOnClickListener(this);
        escribe.setOnClickListener(this);
        adivina.setOnClickListener(this);
        desarroladores.setOnClickListener(this);
        editPerfil.setOnClickListener(this);
        logros.setOnClickListener(this);

        verPerfil.setOnClickListener(this);

    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.IMGPerfil:
                startActivity(new Intent(MainActivity.this, Perfil.class));

                break;


            case R.id.LLEscucha:
                startActivity(new Intent(MainActivity.this, EscuchaNotasMusicales.class));
                break;
            case R.id.LLEscribe:
                startActivity(new Intent(MainActivity.this, EscribeLaNota.class));
                break;
            case R.id.LLAdivina:
                startActivity(new Intent(MainActivity.this, AdivinaLaNota.class));
                break;

            case R.id.LLDesarrolladores:
                startActivity(new Intent(MainActivity.this, Desarrolladores.class));
                break;

            case R.id.LLLogros:
                startActivity(new Intent(MainActivity.this, VerLogros.class));
                break;
            case R.id.editt:

                    Intent intent = new Intent(MainActivity.this, PrincipalActivity.class);
                    intent.putExtra("a", 1);
                    intent.putExtra("id", user.get(sesion.USER_ID));
                    startActivity(intent);
                    finish();

                break;
        }
    }
    //metodo de tipo booleano para comprobar la conexion a internet
    public Boolean isOnlineNet() {
        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");
            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }



}
