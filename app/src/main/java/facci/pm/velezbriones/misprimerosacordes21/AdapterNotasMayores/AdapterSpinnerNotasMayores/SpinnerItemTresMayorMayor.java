package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores;

public class SpinnerItemTresMayorMayor {
    private String NombreItemTresMayor;
    private int ImagenItemTresMayor;

    public SpinnerItemTresMayorMayor (String nombreItemTresMayor, int imagenItemTresMayor){
        this.NombreItemTresMayor = nombreItemTresMayor;
        this.ImagenItemTresMayor = imagenItemTresMayor;
    }

    public String getNombreItemTresMayor() {
        return NombreItemTresMayor;
    }

    public int getImagenItemTresMayor() {
        return ImagenItemTresMayor;
    }
}
