package facci.pm.velezbriones.misprimerosacordes21.AdivinaLaNota;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;

import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.AdapterSpinerCuatroMenorMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.AdapterSpinnerCincoMenorMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.AdapterSpinnerDosMenorMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.AdapterSpinnerSeisMenorMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.AdapterSpinnerSieteMenorMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.AdapterSpinnerTresMenorMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.AdapterSpinnerUnoMenorMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.SpinnerItemNotasMenores.SpinnerItemCincoMenorMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.SpinnerItemNotasMenores.SpinnerItemCuatroMenorMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.SpinnerItemNotasMenores.SpinnerItemDosMenorMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.SpinnerItemNotasMenores.SpinnerItemSeisMenorMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.SpinnerItemNotasMenores.SpinnerItemSieteMenorMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.SpinnerItemNotasMenores.SpinnerItemTresMenorMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.SpinnerItemNotasMenores.SpinnerItemUnoMenorMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerCincoMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerCuatroMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerDosMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerSeisMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerSieteMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerTresMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerUnoMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7.SpinnerItenCuatroMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7.SpinnerItenDosMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7.SpinnerItenSeisMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7.SpinnerItenSieteMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7.SpinnerItenTresMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7.SpinnerItenUnoMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7.SpinneritemCincoMenor;
import facci.pm.velezbriones.misprimerosacordes21.Clases.Sesion;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosAdivinaNotasMenores;
import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AdivinaNotasMenor extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout LLunomenorMenor, LLdosmenorMenor, LLtresmenorMenor, LLcuatromenorMenor, LLcincomenorMenor, LLseismenorMenor, LLsietemenorMenor;
    /*Creamos instancias de tipo String y las inicializamos en blanco*/
    private String SPdomenorMenor = "";
    private String SPremenorMenor = "";
    private String SPmimenorMenor = "";
    private String SPsolmenorMenor = "";
    private String SPlamenorMenor = "";
    private String SPsimenorMenor = "";
    private String SPfamenorMenor = "";
    private int contadorAdivinaNotasMenorMenor, contadorPruebaAdivinaNotasMenor;


    //Todo lo que tenga que ver con firebase :v
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference logros_Adivina_Notas_Menores, users;
    private FirebaseAuth.AuthStateListener authStateListener;
    private String userId;

    //local data
    private Sesion sesion;
    private HashMap<String, String> user;




    /* Necesitamos una lista que deseamos que muestre el spinner. Agarramos los métodos de la clase creada (SpinnerItenUnoMenor)
   también llamamos a esta clase los métodos de la clase AdapterSpinnerUno para poder implementarlos.
   Asi mismo con los demás...
   */
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItemUnoMenorMenor> spinnerItemUnoMenorMenors;
    private AdapterSpinnerUnoMenorMenor adapterSpinnerUnoMenorMenor;

    private  ArrayList<SpinnerItemDosMenorMenor> spinnerItemDosMenorMenors;
    private AdapterSpinnerDosMenorMenor adapterSpinnerDosMenorMenor;

    private ArrayList<SpinnerItemTresMenorMenor> spinnerItemTresMenorMenors;
    private AdapterSpinnerTresMenorMenor adapterSpinnerTresMenorMenor;

    private ArrayList<SpinnerItemCuatroMenorMenor> spinnerItemCuatroMenorMenors;
    private AdapterSpinerCuatroMenorMenor adapterSpinerCuatroMenorMenor;

    private ArrayList<SpinnerItemCincoMenorMenor> spinnerItemCincoMenorMenors;
    private AdapterSpinnerCincoMenorMenor adapterSpinnerCincoMenorMenor;

    private ArrayList<SpinnerItemSeisMenorMenor> spinnerItemSeisMenorMenors;
    private AdapterSpinnerSeisMenorMenor adapterSpinnerSeisMenorMenor;

    private ArrayList<SpinnerItemSieteMenorMenor> spinnerItemSieteMenorMenors;
    private AdapterSpinnerSieteMenorMenor adapterSpinnerSieteMenorMenor;

    /* Instanciamos cada objeto de la clase privada Spinner */
    private Spinner spinnerUnoMenorMenor, spinnerDosMenorMenor, spinnerTresMenorMenor, spinnerCuatroMenorMenor,
            spinnerCincoMenorMenor, spinnerSeisMenorMenor, spinnerSieteMenorMenor;

    /* Instanciamos cada objeto de la clase privada Imageview y MediaPlayer */
    private ImageView unoMenorMenor, dosMenorMenor, tresMenorMenor, cuatroMenorMenor, cincoMenorMenor, seisMenorMenor, sieteMenorMenor;
    private MediaPlayer MPunoMenorMenor, MPdosMenorMenor, MPtresMenorMenor, MPcuatroMenorMenor, MPcincoMenorMenor, MPseisMenorMenor, MPsieteMenorMenor;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adivina_notas_menor);

        sesion = new Sesion(this);
        user = sesion.LoggedInUser();


        /*A cada referencia de LinarLiout lo enlazamos con si id*/
        LLunomenorMenor = (LinearLayout)findViewById(R.id.LLUnoMenorMenor);
        LLdosmenorMenor = (LinearLayout)findViewById(R.id.LLDosMenorMenor);
        LLtresmenorMenor = (LinearLayout)findViewById(R.id.LLTresMenorMenor);
        LLcuatromenorMenor = (LinearLayout)findViewById(R.id.LLCuatroMenorMenor);
        LLcincomenorMenor = (LinearLayout)findViewById(R.id.LLCincoMenorMenor);
        LLseismenorMenor = (LinearLayout)findViewById(R.id.LLSeisMenorMenor);
        LLsietemenorMenor = (LinearLayout)findViewById(R.id.LLSieteMenorMenor);

        /* A cada referencia de MediaPlayer la enlazamos con su id */
        MPunoMenorMenor = MediaPlayer.create(this, R.raw.domenor);
        MPdosMenorMenor = MediaPlayer.create(this, R.raw.remenor);
        MPtresMenorMenor = MediaPlayer.create(this, R.raw.mimenor);
        MPcuatroMenorMenor = MediaPlayer.create(this, R.raw.famenor);
        MPcincoMenorMenor = MediaPlayer.create(this, R.raw.solmenor);
        MPseisMenorMenor = MediaPlayer.create(this, R.raw.lamenor);
        MPsieteMenorMenor = MediaPlayer.create(this, R.raw.simenor);

        /* A cada referencia de las ImageView las enlazamos con su id */
        unoMenorMenor = (ImageView)findViewById(R.id.SPImagenUnoMenorMenor);
        dosMenorMenor = (ImageView)findViewById(R.id.SPImagenDosMenorMenor);
        tresMenorMenor = (ImageView)findViewById(R.id.SPImagenTresMenorMenor);
        cuatroMenorMenor = (ImageView)findViewById(R.id.SPImagenCuatroMenorMenor);
        cincoMenorMenor = (ImageView)findViewById(R.id.SPImagenCincoMenorMenor);
        seisMenorMenor = (ImageView)findViewById(R.id.SPImagenSeisMenorMenor);
        sieteMenorMenor = (ImageView)findViewById(R.id.SPImagenSieteMenorMenor);

        //Asociamos el lístener a los botones.
        unoMenorMenor.setOnClickListener(this);
        dosMenorMenor.setOnClickListener(this);
        tresMenorMenor.setOnClickListener(this);
        cuatroMenorMenor.setOnClickListener(this);
        cincoMenorMenor.setOnClickListener(this);
        seisMenorMenor.setOnClickListener(this);
        sieteMenorMenor.setOnClickListener(this);

        //Asociamos el listener a los LinearLiout
        LLunomenorMenor.setOnClickListener(this);
        LLdosmenorMenor.setOnClickListener(this);
        LLtresmenorMenor.setOnClickListener(this);
        LLcuatromenorMenor.setOnClickListener(this);
        LLcincomenorMenor.setOnClickListener(this);
        LLseismenorMenor.setOnClickListener(this);

        // Inicializamos los métodos para cada Spinner
        SpinnerUnoMenorPersonalizadoMenor();
        SpinnerDosMenorPersonalizadoMenor();
        SpinnerTresMenorPersonalizadoMenor();
        SpinnerCuatroMenorPersonalizadoMenor();
        SpinnerCincoMenorPersonalizadoMenor();
        SpinnerSeisMenorPersonalizadoMenor();
        SpinnerSieteMenorPersonalizadoMenor();



        /* A cada refrencia de los spinner lo enlazamos con su id */
        spinnerUnoMenorMenor = (Spinner) findViewById(R.id.SPUnoMenorMenor);
        spinnerDosMenorMenor = (Spinner)findViewById(R.id.SPDosMenorMenor);
        spinnerTresMenorMenor = (Spinner)findViewById(R.id.SPTresMenorMenor);
        spinnerCuatroMenorMenor = (Spinner)findViewById(R.id.SPCuatroMenorMenor);
        spinnerCincoMenorMenor = (Spinner)findViewById(R.id.SPCincoMenorMenor);
        spinnerSeisMenorMenor = (Spinner)findViewById(R.id.SPSeisMenorMenor);
        spinnerSieteMenorMenor = (Spinner)findViewById(R.id.SPSieteMenorMenor);

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerUnoMenorMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemUnoMenorMenor clickedItem = (SpinnerItemUnoMenorMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemUNoMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.simenor))){

                    if (SPsimenorMenor==""){
                        SPsimenorMenor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPsimenorMenor += " - " + "CORRECTO: " + clickedCountryName;
                    }

                    LLunomenorMenor.setBackgroundResource(R.color.botoncolor);
                    spinnerUnoMenorMenor.setEnabled(false);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {
                    if (SPsimenorMenor==""){
                        SPsimenorMenor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPsimenorMenor += " - " + "INCORRECTO: " + clickedCountryName;
                    }

                    LLunomenorMenor.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerDosMenorMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemDosMenorMenor clickedItem = (SpinnerItemDosMenorMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemDosMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.lamenor))){

                    if (SPlamenorMenor==""){
                        SPlamenorMenor = "CORRECTO: " + clickedCountryName;
                    }else {

                        SPlamenorMenor += " - " + "CORRECTO: " + clickedCountryName;
                    }

                    LLdosmenorMenor.setBackgroundResource(R.color.botoncolor);
                    Correcto();
                    spinnerDosMenorMenor.setEnabled(false);
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {

                    if (SPlamenorMenor==""){
                        SPlamenorMenor = "INCORRECTO: " + clickedCountryName;
                    }else {

                        SPlamenorMenor += " - " + "INCORRECTO: " + clickedCountryName;
                    }

                    LLdosmenorMenor.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerTresMenorMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemTresMenorMenor clickedItem = (SpinnerItemTresMenorMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemTresMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.solmenor))){


                    if (SPsolmenorMenor== ""){
                        SPsolmenorMenor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPsolmenorMenor += " - " + "CORRECTO: "+ clickedCountryName;
                    }
                    LLtresmenorMenor.setBackgroundResource(R.color.botoncolor);
                    spinnerTresMenorMenor.setEnabled(false);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {
                    if (SPsolmenorMenor== ""){
                        SPsolmenorMenor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPsolmenorMenor += " - " + "INCORRECTO: "+ clickedCountryName;
                    }

                    LLtresmenorMenor.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerCuatroMenorMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemCuatroMenorMenor clickedItem = (SpinnerItemCuatroMenorMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemCuatroMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.famenor))){

                    if (SPfamenorMenor ==""){
                        SPfamenorMenor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPfamenorMenor += " - " + "CORRECTO: " + clickedCountryName;
                    }
                    LLcuatromenorMenor.setBackgroundResource(R.color.botoncolor);
                    spinnerCuatroMenorMenor.setEnabled(false);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {


                    if (SPfamenorMenor ==""){
                        SPfamenorMenor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPfamenorMenor += " - " + "INCORRECTO: " + clickedCountryName;
                    }
                    LLcuatromenorMenor.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerCincoMenorMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemCincoMenorMenor clickedItem = (SpinnerItemCincoMenorMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreitemCincoMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.mimenor))){

                    if (SPmimenorMenor == ""){
                        SPmimenorMenor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPmimenorMenor += " - " + "CORRECTO: " + clickedCountryName;
                    }
                    LLcincomenorMenor.setBackgroundResource(R.color.botoncolor);
                    spinnerCincoMenorMenor.setEnabled(false);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {

                    if (SPmimenorMenor == ""){
                        SPmimenorMenor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPmimenorMenor += " - " + "INCORRECTO: " + clickedCountryName;
                    }

                    LLcincomenorMenor.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerSeisMenorMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemSeisMenorMenor clickedItem = (SpinnerItemSeisMenorMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemSeisMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.remenor))){
                    if (SPremenorMenor == ""){
                        SPremenorMenor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPremenorMenor += " - " + "CORRECTO: " + clickedCountryName;
                    }

                    LLseismenorMenor.setBackgroundResource(R.color.botoncolor);
                    spinnerSeisMenorMenor.setEnabled(false);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {
                    if (SPremenorMenor == ""){
                        SPremenorMenor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPremenorMenor += " - " + "INCORRECTO: " + clickedCountryName;
                    }
                    LLseismenorMenor.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerSieteMenorMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemSieteMenorMenor clickedItem = (SpinnerItemSieteMenorMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemSieteMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.domenor))){

                    if (SPdomenorMenor == ""){
                        SPdomenorMenor = "CORRECTO: " + clickedCountryName;
                    }else{
                        SPdomenorMenor += " - " + "CORRECTO: " +clickedCountryName;
                    }

                    LLsietemenorMenor.setBackgroundResource(R.color.botoncolor);
                    spinnerSieteMenorMenor.setEnabled(false);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {

                    if (SPdomenorMenor == ""){
                        SPdomenorMenor = "INCORRECTO: " + clickedCountryName;
                    }else{
                        SPdomenorMenor += " - " + "INCORRECTO: " +clickedCountryName;
                    }

                    LLsietemenorMenor.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        /* Asignados el origen de los datos desde los recursos. */
        adapterSpinnerUnoMenorMenor = new AdapterSpinnerUnoMenorMenor(this, spinnerItemUnoMenorMenors);
        adapterSpinnerDosMenorMenor = new AdapterSpinnerDosMenorMenor(this, spinnerItemDosMenorMenors);
        adapterSpinnerTresMenorMenor = new AdapterSpinnerTresMenorMenor(this, spinnerItemTresMenorMenors);
        adapterSpinerCuatroMenorMenor = new AdapterSpinerCuatroMenorMenor(this, spinnerItemCuatroMenorMenors);
        adapterSpinnerCincoMenorMenor = new AdapterSpinnerCincoMenorMenor(this, spinnerItemCincoMenorMenors);
        adapterSpinnerSeisMenorMenor = new AdapterSpinnerSeisMenorMenor(this, spinnerItemSeisMenorMenors);
        adapterSpinnerSieteMenorMenor = new AdapterSpinnerSieteMenorMenor(this, spinnerItemSieteMenorMenors);


        /* Seteamos el adaptador */
        spinnerUnoMenorMenor.setAdapter(adapterSpinnerUnoMenorMenor);
        spinnerDosMenorMenor.setAdapter(adapterSpinnerDosMenorMenor);
        spinnerTresMenorMenor.setAdapter(adapterSpinnerTresMenorMenor);
        spinnerCuatroMenorMenor.setAdapter(adapterSpinerCuatroMenorMenor);
        spinnerCincoMenorMenor.setAdapter(adapterSpinnerCincoMenorMenor);
        spinnerSeisMenorMenor.setAdapter(adapterSpinnerSeisMenorMenor);
        spinnerSieteMenorMenor.setAdapter(adapterSpinnerSieteMenorMenor);


        // firebase

        /* Obtenemos la instancia para acceder a la base de datos "firebase"*/
        firebaseDatabase = FirebaseDatabase.getInstance();

        /* Obtenemos instancia para autenticar el usuario*/
        firebaseAuth = FirebaseAuth.getInstance();

        /* Obtenemos instancia y referencias del almacenamiento de firebase */
        /* Obetenemos de la base de datos el usuario actual. */

        /* Le damos el nombre a la tabla que guardará los datos en Firebase*/
        logros_Adivina_Notas_Menores = firebaseDatabase.getReference("ADIVINA_LOGROS_MENORES");


    }

    /* Creamos un arreglo para el primer spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerUnoMenorPersonalizadoMenor() {
        spinnerItemUnoMenorMenors = new ArrayList<>();
        spinnerItemUnoMenorMenors.add(new SpinnerItemUnoMenorMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItemUnoMenorMenors.add(new SpinnerItemUnoMenorMenor(getString(R.string.simenor), R.drawable.ic_nota_dp));
        spinnerItemUnoMenorMenors.add(new SpinnerItemUnoMenorMenor(getString(R.string.domenor), R.drawable.ic_nota_dp));
        spinnerItemUnoMenorMenors.add(new SpinnerItemUnoMenorMenor(getString(R.string.famenor), R.drawable.ic_nota_dp));
        spinnerItemUnoMenorMenors.add(new SpinnerItemUnoMenorMenor(getString(R.string.remenor), R.drawable.ic_nota_dp));
        spinnerItemUnoMenorMenors.add(new SpinnerItemUnoMenorMenor(getString(R.string.solmenor), R.drawable.ic_nota_dp));
        spinnerItemUnoMenorMenors.add(new SpinnerItemUnoMenorMenor(getString(R.string.mimenor), R.drawable.ic_nota_dp));
        spinnerItemUnoMenorMenors.add(new SpinnerItemUnoMenorMenor(getString(R.string.solmenor), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el segundo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerDosMenorPersonalizadoMenor() {
        spinnerItemDosMenorMenors = new ArrayList<>();
        spinnerItemDosMenorMenors.add(new SpinnerItemDosMenorMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItemDosMenorMenors.add(new SpinnerItemDosMenorMenor(getString(R.string.mimenor), R.drawable.ic_nota_dp));
        spinnerItemDosMenorMenors.add(new SpinnerItemDosMenorMenor(getString(R.string.lamenor), R.drawable.ic_nota_dp));
        spinnerItemDosMenorMenors.add(new SpinnerItemDosMenorMenor(getString(R.string.solmenor), R.drawable.ic_nota_dp));
        spinnerItemDosMenorMenors.add(new SpinnerItemDosMenorMenor(getString(R.string.domenor), R.drawable.ic_nota_dp));
        spinnerItemDosMenorMenors.add(new SpinnerItemDosMenorMenor(getString(R.string.simenor), R.drawable.ic_nota_dp));
        spinnerItemDosMenorMenors.add(new SpinnerItemDosMenorMenor(getString(R.string.famenor), R.drawable.ic_nota_dp));
        spinnerItemDosMenorMenors.add(new SpinnerItemDosMenorMenor(getString(R.string.remenor), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el tercer spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerTresMenorPersonalizadoMenor() {
        spinnerItemTresMenorMenors = new ArrayList<>();
        spinnerItemTresMenorMenors.add(new SpinnerItemTresMenorMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItemTresMenorMenors.add(new SpinnerItemTresMenorMenor(getString(R.string.remenor), R.drawable.ic_nota_dp));
        spinnerItemTresMenorMenors.add(new SpinnerItemTresMenorMenor(getString(R.string.solmenor), R.drawable.ic_nota_dp));
        spinnerItemTresMenorMenors.add(new SpinnerItemTresMenorMenor(getString(R.string.mimenor), R.drawable.ic_nota_dp));
        spinnerItemTresMenorMenors.add(new SpinnerItemTresMenorMenor(getString(R.string.lamenor), R.drawable.ic_nota_dp));
        spinnerItemTresMenorMenors.add(new SpinnerItemTresMenorMenor(getString(R.string.famenor), R.drawable.ic_nota_dp));
        spinnerItemTresMenorMenors.add(new SpinnerItemTresMenorMenor(getString(R.string.domenor), R.drawable.ic_nota_dp));
        spinnerItemTresMenorMenors.add(new SpinnerItemTresMenorMenor(getString(R.string.simenor), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el cuarto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerCuatroMenorPersonalizadoMenor() {
        spinnerItemCuatroMenorMenors= new ArrayList<>();
        spinnerItemCuatroMenorMenors.add(new SpinnerItemCuatroMenorMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItemCuatroMenorMenors.add(new SpinnerItemCuatroMenorMenor(getString(R.string.domenor), R.drawable.ic_nota_dp));
        spinnerItemCuatroMenorMenors.add(new SpinnerItemCuatroMenorMenor(getString(R.string.famenor), R.drawable.ic_nota_dp));
        spinnerItemCuatroMenorMenors.add(new SpinnerItemCuatroMenorMenor(getString(R.string.lamenor), R.drawable.ic_nota_dp));
        spinnerItemCuatroMenorMenors.add(new SpinnerItemCuatroMenorMenor(getString(R.string.remenor), R.drawable.ic_nota_dp));
        spinnerItemCuatroMenorMenors.add(new SpinnerItemCuatroMenorMenor(getString(R.string.simenor), R.drawable.ic_nota_dp));
        spinnerItemCuatroMenorMenors.add(new SpinnerItemCuatroMenorMenor(getString(R.string.solmenor), R.drawable.ic_nota_dp));
        spinnerItemCuatroMenorMenors.add(new SpinnerItemCuatroMenorMenor(getString(R.string.mimenor), R.drawable.ic_nota_dp));
    }


    /* Creamos un arreglo para el quinto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerCincoMenorPersonalizadoMenor() {
        spinnerItemCincoMenorMenors = new ArrayList<>();
        spinnerItemCincoMenorMenors.add(new SpinnerItemCincoMenorMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItemCincoMenorMenors.add(new SpinnerItemCincoMenorMenor(getString(R.string.famenor), R.drawable.ic_nota_dp));
        spinnerItemCincoMenorMenors.add(new SpinnerItemCincoMenorMenor(getString(R.string.lamenor), R.drawable.ic_nota_dp));
        spinnerItemCincoMenorMenors.add(new SpinnerItemCincoMenorMenor(getString(R.string.domenor), R.drawable.ic_nota_dp));
        spinnerItemCincoMenorMenors.add(new SpinnerItemCincoMenorMenor(getString(R.string.simenor), R.drawable.ic_nota_dp));
        spinnerItemCincoMenorMenors.add(new SpinnerItemCincoMenorMenor(getString(R.string.remenor), R.drawable.ic_nota_dp));
        spinnerItemCincoMenorMenors.add(new SpinnerItemCincoMenorMenor(getString(R.string.solmenor), R.drawable.ic_nota_dp));
        spinnerItemCincoMenorMenors.add(new SpinnerItemCincoMenorMenor(getString(R.string.mimenor), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el sexto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerSeisMenorPersonalizadoMenor() {
        spinnerItemSeisMenorMenors = new ArrayList<>();
        spinnerItemSeisMenorMenors.add(new SpinnerItemSeisMenorMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItemSeisMenorMenors.add(new SpinnerItemSeisMenorMenor(getString(R.string.simenor), R.drawable.ic_nota_dp));
        spinnerItemSeisMenorMenors.add(new SpinnerItemSeisMenorMenor(getString(R.string.lamenor), R.drawable.ic_nota_dp));
        spinnerItemSeisMenorMenors.add(new SpinnerItemSeisMenorMenor(getString(R.string.solmenor), R.drawable.ic_nota_dp));
        spinnerItemSeisMenorMenors.add(new SpinnerItemSeisMenorMenor(getString(R.string.famenor), R.drawable.ic_nota_dp));
        spinnerItemSeisMenorMenors.add(new SpinnerItemSeisMenorMenor(getString(R.string.mimenor), R.drawable.ic_nota_dp));
        spinnerItemSeisMenorMenors.add(new SpinnerItemSeisMenorMenor(getString(R.string.remenor), R.drawable.ic_nota_dp));
        spinnerItemSeisMenorMenors.add(new SpinnerItemSeisMenorMenor(getString(R.string.domenor), R.drawable.ic_nota_dp));
    }
    /* Creamos un arreglo para el septimo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerSieteMenorPersonalizadoMenor() {
        spinnerItemSieteMenorMenors = new ArrayList<>();
        spinnerItemSieteMenorMenors.add(new SpinnerItemSieteMenorMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItemSieteMenorMenors.add(new SpinnerItemSieteMenorMenor(getString(R.string.domenor), R.drawable.ic_nota_dp));
        spinnerItemSieteMenorMenors.add(new SpinnerItemSieteMenorMenor(getString(R.string.famenor), R.drawable.ic_nota_dp));
        spinnerItemSieteMenorMenors.add(new SpinnerItemSieteMenorMenor(getString(R.string.remenor), R.drawable.ic_nota_dp));
        spinnerItemSieteMenorMenors.add(new SpinnerItemSieteMenorMenor(getString(R.string.solmenor), R.drawable.ic_nota_dp));
        spinnerItemSieteMenorMenors.add(new SpinnerItemSieteMenorMenor(getString(R.string.mimenor), R.drawable.ic_nota_dp));
        spinnerItemSieteMenorMenors.add(new SpinnerItemSieteMenorMenor(getString(R.string.solmenor), R.drawable.ic_nota_dp));
        spinnerItemSieteMenorMenors.add(new SpinnerItemSieteMenorMenor(getString(R.string.lamenor), R.drawable.ic_nota_dp));
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.SPImagenUnoMenorMenor:
                MPunoMenorMenor.start();
                break;
            case R.id.SPImagenDosMenorMenor:
                MPdosMenorMenor.start();
                break;
            case R.id.SPImagenTresMenorMenor:
                MPtresMenorMenor.start();
                break;
            case R.id.SPImagenCuatroMenorMenor:
                MPcuatroMenorMenor.start();
                break;
            case R.id.SPImagenCincoMenorMenor:
                MPcincoMenorMenor.start();
                break;
            case R.id.SPImagenSeisMenorMenor:
                MPseisMenorMenor.start();
                break;
            case R.id.SPImagenSieteMenorMenor:
                MPsieteMenorMenor.start();
                break;
        }
    }

    private void InCorrecto(){

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.incorrecto));
        imageView.setImageResource(R.drawable.ic_marca_de_cancelar);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
        contadorAdivinaNotasMenorMenor++;
    }

    /* Método "Correto" para mostrar que está correcto, es llama cada vez que se cumplen las condiciones en los botones*/
    private void Correcto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.correcto));
        imageView.setImageResource(R.drawable.ic_correcto_simbolo);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
        contadorPruebaAdivinaNotasMenor++;
    }

    private void GuardarRecordAdivinaMenor() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("RECORD_NOTAS_ADIVINA_MENOR", contadorAdivinaNotasMenorMenor);
        editor.apply();
    }

    private void GuardarAdivinaCorrectoMenor() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (contadorPruebaAdivinaNotasMenor == 7)
        {
            editor.putString("GUARDAR_RECORD_ADIVINA_MENOR", "SI");
        }else {
            editor.putString("GUARDAR_RECORD_ADIVINA_MENOR", "NO");
        }
        editor.apply();
    }

    /*Método para bloquear el botón BACK*/
    @Override
    public void onBackPressed(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View guardar = layoutInflater.inflate(R.layout.guardar_notas_menores, null);
        final CardView Guardar = (CardView)guardar.findViewById(R.id.CardGuardar);
        final CardView Cancelar = (CardView)guardar.findViewById(R.id.CardCancelar);

        Guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarAdivinaCorrectoMenor();
                GuardarRecordAdivinaMenor();

                LogrosAdivinaNotasMenores LAdivinaNotasMenores= new LogrosAdivinaNotasMenores();
                LAdivinaNotasMenores.setADoMenor(SPdomenorMenor);
                LAdivinaNotasMenores.setAReMenor(SPremenorMenor);
                        LAdivinaNotasMenores.setAMiMenor(SPmimenorMenor);
                LAdivinaNotasMenores.setAFaMenor(SPfamenorMenor);
                        LAdivinaNotasMenores.setASolMenor(SPsolmenorMenor);
                LAdivinaNotasMenores.setALaMenor(SPlamenorMenor);
                LAdivinaNotasMenores.setASiMenor(SPsimenorMenor);

                final String id = user.get(sesion.USER_ID);
                logros_Adivina_Notas_Menores.child(id).setValue(LAdivinaNotasMenores).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        startActivity(new Intent(AdivinaNotasMenor.this, AdivinaLaNota.class));
                        Toast.makeText(AdivinaNotasMenor.this, "Datos guardados con éxitos", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                    }
                });

            }
        });

        Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        builder.setView(guardar);
        builder.show();

    }

    //metodo de tipo booleano para comprobar la conexion a internet
    public Boolean isOnlineNet() {
        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");
            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
}
