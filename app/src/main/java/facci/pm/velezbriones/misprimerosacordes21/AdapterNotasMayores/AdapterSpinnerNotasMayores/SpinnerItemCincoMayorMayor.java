package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores;

public class SpinnerItemCincoMayorMayor {
    private String NombreItemCincoMayor;
    private int ImagenItemCincoMayor;

    public SpinnerItemCincoMayorMayor (String nombreItemCincoMayor, int imagenItemCincoMayor){
        this.NombreItemCincoMayor = nombreItemCincoMayor;
        this.ImagenItemCincoMayor = imagenItemCincoMayor;
    }

    public String getNombreItemCincoMayor() {
        return NombreItemCincoMayor;
    }

    public int getImagenItemCincoMayor() {
        return ImagenItemCincoMayor;
    }
}
