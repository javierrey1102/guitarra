package facci.pm.velezbriones.misprimerosacordes21.Clases;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class Sesion {
    public static final String SHARED_PREF_NAME = "sesion";

    public static final String USER_NAME = "nombres";
    public static final String USER_ICONO = "user_icono";
    public static final String USER_ID = "user_id";
    public static Sesion mInstance;
    public static Context mCtx;

    public Sesion(Context context) {
        mCtx = context;
    }

    public static synchronized Sesion getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new Sesion(context);
        }
        return mInstance;
    }

    public void storeUserName(String nombres, String icono, String id) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_NAME, nombres);
        editor.putString(USER_ICONO, icono);
        editor.putString(USER_ID, id);
        editor.commit();
    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_NAME, null) != null;
    }

    public HashMap<String, String> LoggedInUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        HashMap<String, String> usuario = new HashMap<>();
        usuario.put(USER_NAME, sharedPreferences.getString(USER_NAME, null));
        usuario.put(USER_ICONO, sharedPreferences.getString(USER_ICONO, null));
        usuario.put(USER_ID, sharedPreferences.getString(USER_ID, null));
        return usuario;
    }
}
