package facci.pm.velezbriones.misprimerosacordes21.EscuchaNotasMusicales;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;

import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NotasMayor extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout domayor, remayor, mimayor, famayor, solmayor, lamayor, simayor;
    private ImageView imaMayores;
    private MediaPlayer MPdomayor, MPremayor, MPmimayor, MPfamayor, MPsolmayor, MPlamayor, MPsimayor;
    private int contadorNotasMayoresEscuchadas, escuchaNotasMayoresContador;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

  @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());

LeerRecordNotasAdivinaMayor();

        setContentView(R.layout.activity_notas_mayor);

        domayor = (LinearLayout)findViewById(R.id.BTNDomayor);
        remayor = (LinearLayout)findViewById(R.id.BTNRemayor);
        mimayor = (LinearLayout)findViewById(R.id.BTNMimayor);
        famayor = (LinearLayout)findViewById(R.id.BTNFamayor);
        solmayor = (LinearLayout)findViewById(R.id.BTNSolmayor);
        lamayor = (LinearLayout)findViewById(R.id.BTNLamayor);
        simayor = (LinearLayout)findViewById(R.id.BTNSimayor);

        domayor.setOnClickListener(this);
        remayor.setOnClickListener(this);
        mimayor.setOnClickListener(this);
        famayor.setOnClickListener(this);
        solmayor.setOnClickListener(this);
        lamayor.setOnClickListener(this);
        simayor.setOnClickListener(this);

        //  A cada referencia de MediaPlayer lo enlazamos con su respectivo sonido.
        MPdomayor = MediaPlayer.create(this, R.raw.domayor);
        MPremayor = MediaPlayer.create(this, R.raw.remayor);
        MPmimayor = MediaPlayer.create(this, R.raw.mimayor);
        MPfamayor = MediaPlayer.create(this, R.raw.famayor);
        MPsolmayor = MediaPlayer.create(this, R.raw.solmayor);
        MPlamayor = MediaPlayer.create(this, R.raw.lamayor);
        MPsimayor = MediaPlayer.create(this, R.raw.simayor);

        imaMayores = (ImageView)findViewById(R.id.IMGDoMayor);


    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.BTNDomayor:
                MPdomayor.start();
                contadorNotasMayoresEscuchadas++;
                imaMayores.setImageResource(R.drawable.domayor);

                domayor.setBackgroundResource(R.drawable.degradadoboton);
                remayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                mimayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                famayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                solmayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                lamayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                simayor.setBackgroundResource(R.drawable.degradadobotonblanco);

                // Si esta sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPremayor.isPlaying()){
                    // Si está sonando el acorde musical "re mayor" entonces se pausa.
                    MPremayor.pause();
                    try {
                        //El acorde musical "re mayor" se devuelve a su estado inical (segundo cero).
                        MPremayor.stop();
                        MPremayor.prepare();
                        MPremayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                    MPdomayor.start();
                }else if (MPmimayor.isPlaying()){
                    MPmimayor.pause();
                    try {

                        MPmimayor.stop();
                        MPmimayor.prepare();
                        MPmimayor.seekTo(0);

                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPfamayor.isPlaying()){
                    MPfamayor.pause();
                    try {
                        MPfamayor.stop();
                        MPfamayor.prepare();
                        MPfamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsolmayor.isPlaying()) {
                    MPsolmayor.pause();
                    try {
                        MPsolmayor.stop();
                        MPsolmayor.prepare();
                        MPsolmayor.seekTo(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else if (MPlamayor.isPlaying()){
                    MPlamayor.pause();
                    try {
                        MPlamayor.stop();
                        MPlamayor.prepare();
                        MPlamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimayor.isPlaying()){
                    MPsimayor.pause();
                    try {
                        MPsimayor.stop();
                        MPsimayor.prepare();
                        MPsimayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }




                break;
            case  R.id.BTNRemayor:
                contadorNotasMayoresEscuchadas++;
                // Se reproduce la nota musical
                MPremayor.start();
                imaMayores.setImageResource(R.drawable.remayor);

                domayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                remayor.setBackgroundResource(R.drawable.degradadoboton);
                mimayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                famayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                solmayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                lamayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                simayor.setBackgroundResource(R.drawable.degradadobotonblanco);


                MPdomayor.stop();
                try {
                    MPdomayor.stop();
                    MPdomayor.prepare();
                    MPdomayor.seekTo(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPmimayor.isPlaying()){
                    // Si está sonando el acorde musical "mi mayor" entonces se pausa.
                    MPmimayor.pause();
                    try {
                        //El acorde musical "mi mayor" se devuelve a su estado inical (segundo cero).
                        MPmimayor.stop();
                        MPmimayor.prepare();
                        MPmimayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPfamayor.isPlaying()){
                    MPfamayor.pause();
                    try {
                        MPfamayor.stop();
                        MPfamayor.prepare();
                        MPfamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPsolmayor.isPlaying()){
                    MPsolmayor.pause();
                    try {
                        MPsolmayor.stop();
                        MPsolmayor.prepare();
                        MPsolmayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPlamayor.isPlaying()){
                    MPlamayor.pause();
                    try {
                        MPlamayor.stop();
                        MPlamayor.prepare();
                        MPlamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimayor.isPlaying()){
                    MPsimayor.pause();
                    try {
                        MPsimayor.stop();
                        MPsimayor.prepare();
                        MPsimayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;


            case R.id.BTNMimayor:
                contadorNotasMayoresEscuchadas++;
                // Se reproduce la nota musical
                MPmimayor.start();
                imaMayores.setImageResource(R.drawable.mimayor);

                domayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                remayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                mimayor.setBackgroundResource(R.drawable.degradadoboton);
                famayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                solmayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                lamayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                simayor.setBackgroundResource(R.drawable.degradadobotonblanco);



                /* Cuanado suena por primera vez el acorde "mi mayor", entonces se detiene cualquier nota que esté sonando */
                MPdomayor.stop();
                try {
                    MPdomayor.stop();
                    MPdomayor.prepare();
                    MPdomayor.seekTo(0);
                }catch (IOException r){
                    r.printStackTrace();

                }

                MPremayor.stop();
                try {
                    MPremayor.stop();
                    MPremayor.prepare();
                    MPremayor.seekTo(0);
                }catch (IOException r){
                    r.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPfamayor.isPlaying()){
                    MPfamayor.pause();
                    // Si está sonando el acorde musical "fa mayor" entonces se pausa.
                    try {
                        //El acorde musical "fa mayor" se devuelve a su estado inical (segundo cero).
                        MPfamayor.stop();
                        MPfamayor.prepare();
                        MPfamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPsolmayor.isPlaying()){
                    MPsolmayor.pause();
                    try {
                        MPsolmayor.stop();
                        MPsolmayor.prepare();
                        MPsolmayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPlamayor.isPlaying()){
                    MPlamayor.pause();
                    try {
                        MPlamayor.stop();
                        MPlamayor.prepare();
                        MPlamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimayor.isPlaying()){
                    MPsimayor.pause();
                    try {
                        MPsimayor.stop();
                        MPsimayor.prepare();
                        MPsimayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;


            case R.id.BTNFamayor:
                contadorNotasMayoresEscuchadas++;
                // Se reproduce la nota musical
                MPfamayor.start();
                imaMayores.setImageResource(R.drawable.famayor);

                domayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                remayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                mimayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                famayor.setBackgroundResource(R.drawable.degradadoboton);
                solmayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                lamayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                simayor.setBackgroundResource(R.drawable.degradadobotonblanco);

                /* Cuanado suena por primera vez el acorde "fa mayor", entonces se detiene cualquier nota que esté sonando */
                MPdomayor.stop();
                try {
                    MPdomayor.stop();
                    MPdomayor.prepare();
                    MPdomayor.seekTo(0);
                }catch (IOException q){
                    q.printStackTrace();

                }

                MPremayor.stop();
                try {
                    MPremayor.stop();
                    MPremayor.prepare();
                    MPremayor.seekTo(0);
                }catch (IOException rq){
                    rq.printStackTrace();
                }

                MPmimayor.stop();
                try {
                    MPmimayor.stop();
                    MPmimayor.prepare();
                    MPmimayor.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPsolmayor.isPlaying()) {
                    MPsolmayor.pause();
                    // Si está sonando el acorde musical "sol mayor" entonces se pausa.
                    try {
                        //El acorde musical "sol mayor" se devuelve a su estado inical (segundo cero).
                        MPsolmayor.stop();
                        MPsolmayor.prepare();
                        MPsolmayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPlamayor.isPlaying()){
                    MPlamayor.pause();
                    try {
                        MPlamayor.stop();
                        MPlamayor.prepare();
                        MPlamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPsimayor.isPlaying()){
                    MPsimayor.pause();
                    try {
                        MPsimayor.stop();
                        MPsimayor.prepare();
                        MPsimayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;



            case  R.id.BTNSolmayor:
                contadorNotasMayoresEscuchadas++;
                // Se reproduce la nota musical
                MPsolmayor.start();
                imaMayores.setImageResource(R.drawable.solmayor);

                domayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                remayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                mimayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                famayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                solmayor.setBackgroundResource(R.drawable.degradadoboton);
                lamayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                simayor.setBackgroundResource(R.drawable.degradadobotonblanco);



                /* Cuanado suena por primera vez el acorde "sol mayor", entonces se detiene cualquier nota que esté sonando */
                MPdomayor.stop();
                try {
                    MPdomayor.stop();
                    MPdomayor.prepare();
                    MPdomayor.seekTo(0);
                }catch (IOException w){
                    w.printStackTrace();

                }

                MPremayor.stop();
                try {
                    MPremayor.stop();
                    MPremayor.prepare();
                    MPremayor.seekTo(0);
                }catch (IOException wq){
                    wq.printStackTrace();
                }


                MPmimayor.stop();
                try {
                    MPmimayor.stop();
                    MPmimayor.prepare();
                    MPmimayor.seekTo(0);
                }catch (IOException wp){
                    wp.printStackTrace();
                }

                MPfamayor.stop();
                try {
                    MPfamayor.stop();
                    MPfamayor.prepare();
                    MPfamayor.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPlamayor.isPlaying()){
                    // Si está sonando el acorde musical "la mayor" entonces se pausa.
                    MPlamayor.stop();
                    try {
                        //El acorde musical "la mayor" se devuelve a su estado inical (segundo cero).
                        MPlamayor.stop();
                        MPlamayor.prepare();
                        MPlamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPsimayor.isPlaying()){
                    MPsimayor.stop();
                    try {
                        MPsimayor.stop();
                        MPsimayor.prepare();
                        MPsimayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }



                break;
            case R.id.BTNLamayor:
                contadorNotasMayoresEscuchadas++;
                // Se reproduce la nota musical
                MPlamayor.start();
                imaMayores.setImageResource(R.drawable.lamayor);

                domayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                remayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                mimayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                famayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                solmayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                lamayor.setBackgroundResource(R.drawable.degradadoboton);
                simayor.setBackgroundResource(R.drawable.degradadobotonblanco);

                /* Cuanado suena por primera vez el acorde "la mayor", entonces se detiene cualquier nota que esté sonando */
                MPdomayor.stop();
                try {
                    MPdomayor.stop();
                    MPdomayor.prepare();
                    MPdomayor.seekTo(0);
                }catch (IOException ttt){
                    ttt.printStackTrace();

                }
                MPremayor.stop();
                try {
                    MPremayor.stop();
                    MPremayor.prepare();
                    MPremayor.seekTo(0);
                }catch (IOException tqq){
                    tqq.printStackTrace();
                }

                MPmimayor.stop();
                try {
                    MPmimayor.stop();
                    MPmimayor.prepare();
                    MPmimayor.seekTo(0);
                }catch (IOException rww){
                    rww.printStackTrace();
                }


                MPfamayor.stop();
                try {
                    MPfamayor.stop();
                    MPfamayor.prepare();
                    MPfamayor.seekTo(0);
                }catch (IOException bhh){
                    bhh.printStackTrace();
                }

                MPsolmayor.stop();
                try {
                    MPsolmayor.stop();
                    MPsolmayor.prepare();
                    MPsolmayor.seekTo(0);
                }catch (IOException fff){
                    fff.printStackTrace();
                }

                // Si esta sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                if (MPsimayor.isPlaying()){
                    MPsimayor.stop();
                    try {
                        MPsimayor.stop();
                        MPsimayor.prepare();
                        MPsimayor.seekTo(0);
                    }catch (IOException xrr){
                        xrr.printStackTrace();
                    }


                }
                break;


            case R.id.BTNSimayor:
                contadorNotasMayoresEscuchadas++;
                // Se reproduce la nota musical
                MPsimayor.start();
                imaMayores.setImageResource(R.drawable.simayor);

                domayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                remayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                mimayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                famayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                solmayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                lamayor.setBackgroundResource(R.drawable.degradadobotonblanco);
                simayor.setBackgroundResource(R.drawable.degradadoboton);

                /* Cuanado suena por primera vez si mayor, entonces se detiene cualquier nota que esté sonando */
                MPdomayor.stop();
                try {
                    MPdomayor.stop();
                    MPdomayor.prepare();
                    MPdomayor.seekTo(0);
                }catch (IOException t){
                    t.printStackTrace();

                }

                MPremayor.stop();
                try {
                    MPremayor.stop();
                    MPremayor.prepare();
                    MPremayor.seekTo(0);
                }catch (IOException tq){
                    tq.printStackTrace();
                }


                MPmimayor.stop();
                try {
                    MPmimayor.stop();
                    MPmimayor.prepare();
                    MPmimayor.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }

                MPfamayor.stop();
                try {
                    MPfamayor.stop();
                    MPfamayor.prepare();
                    MPfamayor.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                MPsolmayor.stop();
                try {
                    MPsolmayor.stop();
                    MPsolmayor.prepare();
                    MPsolmayor.seekTo(0);
                }catch (IOException pr){
                    pr.printStackTrace();
                }

                MPlamayor.stop();
                try {
                    MPlamayor.stop();
                    MPlamayor.prepare();
                    MPlamayor.seekTo(0);
                }catch (IOException ju){
                    ju.printStackTrace();
                }
                break;
        }
    }

    private void LeerRecordNotasAdivinaMayor() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        escuchaNotasMayoresContador = sharedPreferences.getInt("ESCUCHA_NOTAS_MAYOR", 0);

    }

    private void GuardarRecordAdivinaMenor() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("ESCUCHA_NOTAS_MAYOR", escuchaNotasMayoresContador + contadorNotasMayoresEscuchadas);
        editor.apply();
    }

    @Override
    public void onBackPressed(){
        GuardarRecordAdivinaMenor();
        finish();
    }
}
