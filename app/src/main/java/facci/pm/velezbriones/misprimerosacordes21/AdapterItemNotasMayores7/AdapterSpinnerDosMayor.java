package facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.sql.Array;
import java.util.ArrayList;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItemDosMayor7;
import facci.pm.velezbriones.misprimerosacordes21.R;

public class AdapterSpinnerDosMayor extends ArrayAdapter<SpinnerItemDosMayor7> {
    public  AdapterSpinnerDosMayor (Context context, ArrayList<SpinnerItemDosMayor7> NumberList){
        super(context, 0, NumberList);
    }

    @NonNull
    //se crea por defecto, no se modifica el código
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    //se crea por defecto, no se modifica el código
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    // también se crea por defecto pero le hacemos algunos ajustes
    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            // como la vista no tiene nada entonces le pasamos el XML
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_uno, parent, false
            );
        }
        // Inicializamos todos los campos que tiene el layout-XML  y a esos campos hay que convertirlos en (convertView)
        ImageView imageView = (ImageView)convertView.findViewById(R.id.IMGSPUnoMenor);
        TextView textView = (TextView)convertView.findViewById(R.id.LBLSPUnomenor);

        SpinnerItemDosMayor7 spinnerItemDosMayor7 = getItem(position);

        if (spinnerItemDosMayor7 != null){
            imageView.setImageResource(spinnerItemDosMayor7.getImagenItemMayor7());
            textView.setText(spinnerItemDosMayor7.getNombreItemMayor7());
        }
        return convertView;
    }

    }
