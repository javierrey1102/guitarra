package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.SpinnerItemNotasMenores;

public class SpinnerItemTresMenorMenor {
    private String NombreItemTresMenor;
    private int ImagenItemTresMenor;

    public SpinnerItemTresMenorMenor (String nombreItemTresMenor, int imagenItemTresMenor){
        this.NombreItemTresMenor = nombreItemTresMenor;
        this.ImagenItemTresMenor = imagenItemTresMenor;

    }

    public String getNombreItemTresMenor() {
        return NombreItemTresMenor;
    }

    public int getImagenItemTresMenor() {
        return ImagenItemTresMenor;
    }
}
