package facci.pm.velezbriones.misprimerosacordes21.Modelo;

public class LogrosAdivinaNotasMayores7 {
    private String ADoMayor7, AReMayor7, AMiMayor7, AFaMayor7, ASolMayor7, ALaMayor7, ASiMayor7, ANombreMayor7;

    public LogrosAdivinaNotasMayores7() {

    }

    public String getADoMayor7() {
        return ADoMayor7;
    }

    public void setADoMayor7(String ADoMayor7) {
        this.ADoMayor7 = ADoMayor7;
    }

    public String getAReMayor7() {
        return AReMayor7;
    }

    public void setAReMayor7(String AReMayor7) {
        this.AReMayor7 = AReMayor7;
    }

    public String getAMiMayor7() {
        return AMiMayor7;
    }

    public void setAMiMayor7(String AMiMayor7) {
        this.AMiMayor7 = AMiMayor7;
    }

    public String getAFaMayor7() {
        return AFaMayor7;
    }

    public void setAFaMayor7(String AFaMayor7) {
        this.AFaMayor7 = AFaMayor7;
    }

    public String getASolMayor7() {
        return ASolMayor7;
    }

    public void setASolMayor7(String ASolMayor7) {
        this.ASolMayor7 = ASolMayor7;
    }

    public String getALaMayor7() {
        return ALaMayor7;
    }

    public void setALaMayor7(String ALaMayor7) {
        this.ALaMayor7 = ALaMayor7;
    }

    public String getASiMayor7() {
        return ASiMayor7;
    }

    public void setASiMayor7(String ASiMayor7) {
        this.ASiMayor7 = ASiMayor7;
    }

    public String getANombreMayor7() {
        return ANombreMayor7;
    }

    public void setANombreMayor7(String ANombreMayor7) {
        this.ANombreMayor7 = ANombreMayor7;
    }
}
