package facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7;

public class SpinnerItemCincoMayor7 {
    private String NombreItemMayor7;
    private int ImagenItemMayor7;

    public SpinnerItemCincoMayor7 (String NombreItemMayor7, int ImagenItemMayor7){
        this.NombreItemMayor7 = NombreItemMayor7;
        this.ImagenItemMayor7 = ImagenItemMayor7;
    }

    public String getNombreItemMayor7() {
        return NombreItemMayor7;
    }

    public int getImagenItemMayor7() {
        return ImagenItemMayor7;
    }
}
