package facci.pm.velezbriones.misprimerosacordes21.EscuchaNotasMusicales;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;

import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NotasMenor extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout domenor, remenor, mimenor, famenor, solmenor, lamenor, simenor;
    private ImageView imagennotamenor;
    private MediaPlayer MPdomenor, MPremenor, MPmimenor, MPfamenor, MPsolmenor, MPlamenor, MPsimenor;
    private int contadorNotasMenoresEscuchadas, escuchaNotasMenoresContador;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());


        setContentView(R.layout.activity_notas_menor);

        LeerRecordNotasAdivinaMenor();

        // A cada referencia de los botones lo enlazamos con su id.
        domenor = (LinearLayout) findViewById(R.id.BTNDomenor);
        remenor = (LinearLayout) findViewById(R.id.BTNRemenor);
        mimenor = (LinearLayout) findViewById(R.id.BTNMimenor);
        famenor = (LinearLayout) findViewById(R.id.BTNFamenor);
        solmenor = (LinearLayout) findViewById(R.id.BTNSolmenor);
        lamenor = (LinearLayout) findViewById(R.id.BTNLamenor);
        simenor = (LinearLayout) findViewById(R.id.BTNSimenor);
        imagennotamenor = (ImageView)findViewById(R.id.IMGDoMenor);

        domenor.setOnClickListener(this);
        remenor.setOnClickListener(this);
        mimenor.setOnClickListener(this);
        famenor.setOnClickListener(this);
        solmenor.setOnClickListener(this);
        lamenor.setOnClickListener(this);
        simenor.setOnClickListener(this);

        //  A cada referencia de MediaPlayer lo enlazamos con su respectivo sonido.
        MPdomenor = MediaPlayer.create(this, R.raw.domenor);
        MPremenor = MediaPlayer.create(this, R.raw.remenor);
        MPmimenor = MediaPlayer.create(this, R.raw.mimenor);
        MPfamenor = MediaPlayer.create(this, R.raw.famenor);
        MPsolmenor = MediaPlayer.create(this, R.raw.solmenor);
        MPlamenor = MediaPlayer.create(this, R.raw.lamenor);
        MPsimenor = MediaPlayer.create(this, R.raw.simenor);


    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.BTNDomenor:
                contadorNotasMenoresEscuchadas++;
                MPdomenor.start();
                imagennotamenor.setImageResource(R.drawable.domenor);
                domenor.setBackgroundResource(R.drawable.degradadoboton);
                remenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                mimenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                famenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                solmenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                lamenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                simenor.setBackgroundResource(R.drawable.degradadobotonblanco);


            // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
            // Aplica para todas las condiciones de este botón.

                if (MPremenor.isPlaying()){
                // Si está sonando el acorde musical "re menor" entonces se pausa.
                MPremenor.pause();
                try {
                    //El acorde musical "re menor" se devuelve a su estado inical (se inicia desde cero).
                    MPremenor.stop();
                    MPremenor.prepare();
                    MPremenor.seekTo(0);
                }catch (IOException e){
                    e.printStackTrace();
                }
                MPdomenor.start();
            }else if (MPmimenor.isPlaying()){
                MPmimenor.pause();

                try {

                    MPmimenor.stop();
                    MPmimenor.prepare();
                    MPmimenor.seekTo(0);

                }catch (IOException e){
                    e.printStackTrace();
                }

            }else if (MPfamenor.isPlaying()){
                MPfamenor.pause();
                try {
                    MPfamenor.stop();
                    MPfamenor.prepare();
                    MPfamenor.seekTo(0);
                }catch (IOException e){
                    e.printStackTrace();
                }
            }else if (MPsolmenor.isPlaying()) {
                MPsolmenor.pause();
                try {
                    MPsolmenor.stop();
                    MPsolmenor.prepare();
                    MPsolmenor.seekTo(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else if (MPlamenor.isPlaying()){
                MPlamenor.pause();
                try {
                    MPlamenor.stop();
                    MPlamenor.prepare();
                    MPlamenor.seekTo(0);
                }catch (IOException e){
                    e.printStackTrace();
                }

            }else if (MPsimenor.isPlaying()){
                MPsimenor.pause();
                try {
                    MPsimenor.stop();
                    MPsimenor.prepare();
                    MPsimenor.seekTo(0);
                }catch (IOException e){
                    e.printStackTrace();
                }
                MPdomenor.start();
            }



            break;

            case  R.id.BTNRemenor:
                contadorNotasMenoresEscuchadas++;
                // Se reproduce la nota musical y se muestra la imagen de la nota
                MPremenor.start();
                imagennotamenor.setImageResource(R.drawable.remenor);

                domenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                remenor.setBackgroundResource(R.drawable.degradadoboton);
                mimenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                famenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                solmenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                lamenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                simenor.setBackgroundResource(R.drawable.degradadobotonblanco);


                // Si está sonando cualquier nota musical, entonces se detiene  y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomenor.stop();
                try {
                    MPdomenor.stop();
                    MPdomenor.prepare();
                    MPdomenor.seekTo(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPmimenor.isPlaying()){
                    // Si está sonando el acorde musical "mi menor" entonces se pausa.
                    MPmimenor.pause();
                    try {
                        //El acorde musical "mi menor" se devuelve a su estado inical (se inicia desde cero).
                        MPmimenor.stop();
                        MPmimenor.prepare();
                        MPmimenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPfamenor.isPlaying()){
                    MPfamenor.pause();
                    try {
                        MPfamenor.stop();
                        MPfamenor.prepare();
                        MPfamenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPsolmenor.isPlaying()){
                    MPsolmenor.pause();
                    try {
                        MPsolmenor.stop();
                        MPsolmenor.prepare();
                        MPsolmenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPlamenor.isPlaying()){
                    MPlamenor.pause();
                    try {
                        MPlamenor.stop();
                        MPlamenor.prepare();
                        MPlamenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimenor.isPlaying()){
                    MPsimenor.pause();
                    try {
                        MPsimenor.stop();
                        MPsimenor.prepare();
                        MPsimenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;

            case R.id.BTNMimenor:
                contadorNotasMenoresEscuchadas++;
                // Se reproduce la nota musical y se muestra la imagen de la nota
                MPmimenor.start();
                imagennotamenor.setImageResource(R.drawable.mimenor);

                domenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                remenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                mimenor.setBackgroundResource(R.drawable.degradadoboton);
                famenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                solmenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                lamenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                simenor.setBackgroundResource(R.drawable.degradadobotonblanco);


                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical  se devuelve a su estado inical (se inicia desde cero).
                MPdomenor.stop();
                try {
                    MPdomenor.stop();
                    MPdomenor.prepare();
                    MPdomenor.seekTo(0);
                }catch (IOException r){
                    r.printStackTrace();
                }

                MPremenor.stop();
                try {
                    MPremenor.stop();
                    MPremenor.prepare();
                    MPremenor.seekTo(0);
                }catch (IOException r){
                    r.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPfamenor.isPlaying()){
                    // Si está sonando el acorde musical "fa menor" entonces se pausa.
                    MPfamenor.pause();
                    try {
                        //El acorde musical "fa menor" se devuelve a su estado inical (se inicia desde cero).
                        MPfamenor.stop();
                        MPfamenor.prepare();
                        MPfamenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPsolmenor.isPlaying()){
                    MPsolmenor.pause();
                    try {
                        MPsolmenor.stop();
                        MPsolmenor.prepare();
                        MPsolmenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPlamenor.isPlaying()){
                    MPlamenor.pause();
                    try {
                        MPlamenor.stop();
                        MPlamenor.prepare();
                        MPlamenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimenor.isPlaying()){
                    MPsimenor.pause();
                    try {
                        MPsimenor.stop();
                        MPsimenor.prepare();
                        MPsimenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;

            case R.id.BTNFamenor:
                contadorNotasMenoresEscuchadas++;
                // Se reproduce la nota musical y se muestra la imagen de la nota
                MPfamenor.start();
                imagennotamenor.setImageResource(R.drawable.famenor);

                domenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                remenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                mimenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                famenor.setBackgroundResource(R.drawable.degradadoboton);
                solmenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                lamenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                simenor.setBackgroundResource(R.drawable.degradadobotonblanco);

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical  se devuelve a su estado inical (se inicia desde cero).
                MPdomenor.stop();
                try {
                    MPdomenor.stop();
                    MPdomenor.prepare();
                    MPdomenor.seekTo(0);
                }catch (IOException q){
                    q.printStackTrace();
                }

                MPremenor.stop();
                try {
                    MPremenor.stop();
                    MPremenor.prepare();
                    MPremenor.seekTo(0);
                }catch (IOException rq){
                    rq.printStackTrace();
                }

                MPmimenor.stop();
                try {
                    MPmimenor.stop();
                    MPmimenor.prepare();
                    MPmimenor.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPsolmenor.isPlaying()) {
                    // Si está sonando el acorde musical "sol menor" entonces se pausa.
                    MPsolmenor.pause();
                    try {
                        //El acorde musical "sol menor" se devuelve a su estado inical (se inicia desde cero).
                        MPsolmenor.stop();
                        MPsolmenor.prepare();
                        MPsolmenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPlamenor.isPlaying()){
                    MPlamenor.pause();
                    try {
                        MPlamenor.stop();
                        MPlamenor.prepare();
                        MPlamenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPsimenor.isPlaying()){
                    MPsimenor.pause();
                    try {
                        MPsimenor.stop();
                        MPsimenor.prepare();
                        MPsimenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;

            case  R.id.BTNSolmenor:
                contadorNotasMenoresEscuchadas++;
                // Se reproduce la nota musical y se muestra la imagen de la nota
                MPsolmenor.start();
                imagennotamenor.setImageResource(R.drawable.solmenor);

                domenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                remenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                mimenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                famenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                solmenor.setBackgroundResource(R.drawable.degradadoboton);
                lamenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                simenor.setBackgroundResource(R.drawable.degradadobotonblanco);

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical  se devuelve a su estado inical (se inicia desde cero).
                MPdomenor.stop();
                try {
                    MPdomenor.stop();
                    MPdomenor.prepare();
                    MPdomenor.seekTo(0);
                }catch (IOException w){
                    w.printStackTrace();
                }

                MPremenor.stop();
                try {
                    MPremenor.stop();
                    MPremenor.prepare();
                    MPremenor.seekTo(0);
                }catch (IOException wq){
                    wq.printStackTrace();
                }


                MPmimenor.stop();
                try {
                    MPmimenor.stop();
                    MPmimenor.prepare();
                    MPmimenor.seekTo(0);
                }catch (IOException wp){
                    wp.printStackTrace();
                }

                MPfamenor.stop();
                try {
                    MPfamenor.stop();
                    MPfamenor.prepare();
                    MPfamenor.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPlamenor.isPlaying()){
                    // Si está sonando el acorde musical "la menor" entonces se pausa.
                    MPlamenor.stop();
                    try {
                        //El acorde musical "la menor" se devuelve a su estado inical (se inicia desde cero).
                        MPlamenor.stop();
                        MPlamenor.prepare();
                        MPlamenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPsimenor.isPlaying()){
                    MPsimenor.stop();
                    try {
                        MPsimenor.stop();
                        MPsimenor.prepare();
                        MPsimenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;

            case R.id.BTNLamenor:
                contadorNotasMenoresEscuchadas++;
                // Se reproduce la nota musical y se muestra la imagen de la nota
                MPlamenor.start();
                imagennotamenor.setImageResource(R.drawable.lamenor);

                domenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                remenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                mimenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                famenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                solmenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                lamenor.setBackgroundResource(R.drawable.degradadoboton);
                simenor.setBackgroundResource(R.drawable.degradadobotonblanco);

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical  se devuelve a su estado inical (se inicia desde cero).
                MPdomenor.stop();
                try {
                    MPdomenor.stop();
                    MPdomenor.prepare();
                    MPdomenor.seekTo(0);
                }catch (IOException t){
                    t.printStackTrace();

                }
                MPremenor.stop();
                try {
                    MPremenor.stop();
                    MPremenor.prepare();
                    MPremenor.seekTo(0);
                }catch (IOException tq){
                    tq.printStackTrace();
                }

                MPmimenor.stop();
                try {
                    MPmimenor.stop();
                    MPmimenor.prepare();
                    MPmimenor.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }


                MPfamenor.stop();
                try {
                    MPfamenor.stop();
                    MPfamenor.prepare();
                    MPfamenor.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                MPsolmenor.stop();
                try {
                    MPsolmenor.stop();
                    MPsolmenor.prepare();
                    MPsolmenor.seekTo(0);
                }catch (IOException ff){
                    ff.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPsimenor.isPlaying()){
                    MPsimenor.stop();
                    // Si está sonando el acorde musical "si menor" entonces se pausa.
                    try {
                        //El acorde musical "si menor" se devuelve a su estado inical (se inicia desde cero).
                        MPsimenor.stop();
                        MPsimenor.prepare();
                        MPsimenor.seekTo(0);
                    }catch (IOException xr){
                        xr.printStackTrace();
                    }
                }
                break;


            case R.id.BTNSimenor:
                contadorNotasMenoresEscuchadas++;
                // Se reproduce la nota musical y se muestra la imagen de la nota
                MPsimenor.start();
                imagennotamenor.setImageResource(R.drawable.simenor);

                domenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                remenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                mimenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                famenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                solmenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                lamenor.setBackgroundResource(R.drawable.degradadobotonblanco);
                simenor.setBackgroundResource(R.drawable.degradadoboton);

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical se devuelve a su estado inical (se inicia desde cero).
                MPdomenor.stop();
                try {
                    MPdomenor.stop();
                    MPdomenor.prepare();
                    MPdomenor.seekTo(0);
                }catch (IOException t){
                    t.printStackTrace();

                }

                MPremenor.stop();
                try {
                    MPremenor.stop();
                    MPremenor.prepare();
                    MPremenor.seekTo(0);
                }catch (IOException tq){
                    tq.printStackTrace();
                }


                MPmimenor.stop();
                try {
                    MPmimenor.stop();
                    MPmimenor.prepare();
                    MPmimenor.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }

                MPfamenor.stop();
                try {
                    MPfamenor.stop();
                    MPfamenor.prepare();
                    MPfamenor.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                MPsolmenor.stop();
                try {
                    MPsolmenor.stop();
                    MPsolmenor.prepare();
                    MPsolmenor.seekTo(0);
                }catch (IOException pr){
                    pr.printStackTrace();
                }

                MPlamenor.stop();
                try {
                    MPlamenor.stop();
                    MPlamenor.prepare();
                    MPlamenor.seekTo(0);
                }catch (IOException ju){
                    ju.printStackTrace();
                }
                break;

        }
    }

    private void LeerRecordNotasAdivinaMenor() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        escuchaNotasMenoresContador = sharedPreferences.getInt("ESCUCHA_NOTAS_MENOR", 0);
    }


    private void GuardarRecordMenor() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("ESCUCHA_NOTAS_MENOR", escuchaNotasMenoresContador + contadorNotasMenoresEscuchadas);
        editor.apply();
    }
    public void onBackPressed(){
        GuardarRecordMenor();
        finish();
    }
}
