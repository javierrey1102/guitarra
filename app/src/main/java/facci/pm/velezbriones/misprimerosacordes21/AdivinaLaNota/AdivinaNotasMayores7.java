package facci.pm.velezbriones.misprimerosacordes21.AdivinaLaNota;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;

import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.AdapterSpinnerCincoMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.AdapterSpinnerCuatroMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.AdapterSpinnerDosMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.AdapterSpinnerSeisMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.AdapterSpinnerTresMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.AdapterSpinnerUnoMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.AdaterSpinnerSieteMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItemCincoMayor7;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItemCuatroMayor7;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItemDosMayor7;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItemSieteMayor7;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItemTresMayor7;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItemUnoMayor7;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItenSeisMayor7;
import facci.pm.velezbriones.misprimerosacordes21.Clases.Sesion;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosAdivinaNotasMayores7;
import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AdivinaNotasMayores7 extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout LLunomayor7, LLdosmayor7, LLtresmayor7, LLcuatromayor7, LLcincomayor7, LLseismayor7, LLsietemayor7;
    /*Creamos instancias de tipo String y las inicializamos en blanco*/
    private String SPdomayor7 = "";
    private String SPremayor7 = "";
    private String SPmimayor7 = "";
    private String SPsolmayor7 = "";
    private String SPlamayor7 = "";
    private String SPsimayor7 = "";
    private String SPfamayor7 = "";
    private int contadorAdivinaNotasMayores7, contadorPruebaAdivinaNotasMayores7;

    //Todo lo que tenga que ver con firebase :v
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference logros_Adivina_Notas_Mayores_7, users;
    private FirebaseAuth.AuthStateListener authStateListener;
    private String userId;

    //local data
    private Sesion sesion;
    private HashMap<String, String> user;



    /* Necesitamos una lista que deseamos que muestre el spinner. Agarramos los métodos de la clase creada (SpinnerItenUnoMayor)
también llamamos a esta clase los métodos de la clase AdapterSpinnerUnoMayor para poder implementarlos.
Asi mismo con los demás...
*/
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItemUnoMayor7> spinnerItenUnoMayor;
    private AdapterSpinnerUnoMayor adapterSpinnerUnoMayor;
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItemDosMayor7> spinnerItenDosMayor;
    private AdapterSpinnerDosMayor adapterSpinnerDosMayor;
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItemTresMayor7> spinnerItenTresMayor;
    private AdapterSpinnerTresMayor adapterSpinnerTresMayor;
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItemCuatroMayor7> spinnerItenCuatroMayor;
    private AdapterSpinnerCuatroMayor adapterSpinnerCuatroMayor;
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItemCincoMayor7> spinnerItenCincoMayor;
    private AdapterSpinnerCincoMayor adapterSpinnerCincoMayor;
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItenSeisMayor7> spinnerItenSeisMayor;
    private AdapterSpinnerSeisMayor adapterSpinnerSeisMayor;
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItemSieteMayor7> spinnerItenSieteMayor;
    private AdaterSpinnerSieteMayor adapterSpinnerSieteMayor;


    /* Instanciamos cada objeto de la clase privada Spinner */
    private Spinner spinnerUnoMayor7, spinnerDosMayor7, spinnerTresMayor7, spinnerCuatroMayor7,
            spinnerCincoMayor7, spinnerSeisMayor7, spinnerSieteMayor7;

    /* Instanciamos cada objeto de la clase privada Imageview y MediaPlayer */
    private ImageView unoMayor7, dosMayor7, tresMayor7, cuatroMayor7, cincoMayor7, seisMayor7, sieteMayor7;
    private MediaPlayer MPunoMayor7, MPdosMayor7, MPtresMayor7, MPcuatroMayor7, MPcincoMayor7, MPseisMayor7, MPsieteMayor7;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        setContentView(R.layout.activity_adivina_notas_mayores7);

        sesion = new Sesion(this);
        user = sesion.LoggedInUser();


        // A cada referencia de LinearLayout le asociamos con su id
        LLunomayor7 = (LinearLayout) findViewById(R.id.LLUnoMayor7);
        LLdosmayor7 = (LinearLayout) findViewById(R.id.LLDosMayor7);
        LLtresmayor7 = (LinearLayout) findViewById(R.id.LLTresMayor7);
        LLcuatromayor7 = (LinearLayout) findViewById(R.id.LLCuatroMayor7);
        LLcincomayor7 = (LinearLayout) findViewById(R.id.LLCincoMayor7);
        LLseismayor7 = (LinearLayout) findViewById(R.id.LLSeisMayor7);
        LLsietemayor7 = (LinearLayout) findViewById(R.id.LLSieteMayor7);



        /* A cada referencia de MediaPlayer la enlazamos con su id */
        MPunoMayor7 = MediaPlayer.create(this, R.raw.do7);
        MPdosMayor7 = MediaPlayer.create(this, R.raw.re7);
        MPtresMayor7 = MediaPlayer.create(this, R.raw.mi7);
        MPcuatroMayor7 = MediaPlayer.create(this, R.raw.fa7);
        MPcincoMayor7 = MediaPlayer.create(this, R.raw.sol7);
        MPseisMayor7 = MediaPlayer.create(this, R.raw.la7);
        MPsieteMayor7 = MediaPlayer.create(this, R.raw.si7);

        /* A cada referencia de las ImageView las enlazamos con su id */
        unoMayor7 = (ImageView) findViewById(R.id.SPImagenUnoMayor7);
        dosMayor7 = (ImageView) findViewById(R.id.SPImagenDosMayor7);
        tresMayor7 = (ImageView) findViewById(R.id.SPImagenTresMayor7);
        cuatroMayor7 = (ImageView) findViewById(R.id.SPImagenCuatroMayor7);
        cincoMayor7 = (ImageView) findViewById(R.id.SPImagenCincoMayor7);
        seisMayor7 = (ImageView) findViewById(R.id.SPImagenSeisMayor7);
        sieteMayor7 = (ImageView) findViewById(R.id.SPImagenSieteMayor7);

        //Asociamos el lístener a los botones.
        unoMayor7.setOnClickListener(this);
        dosMayor7.setOnClickListener(this);
        tresMayor7.setOnClickListener(this);
        cuatroMayor7.setOnClickListener(this);
        cincoMayor7.setOnClickListener(this);
        seisMayor7.setOnClickListener(this);
        sieteMayor7.setOnClickListener(this);

        //Asociamos el lístener a los LinearLyuts
        LLunomayor7.setOnClickListener(this);
        LLdosmayor7.setOnClickListener(this);
        LLtresmayor7.setOnClickListener(this);
        LLcuatromayor7.setOnClickListener(this);
        LLcincomayor7.setOnClickListener(this);
        LLseismayor7.setOnClickListener(this);
        LLsietemayor7.setOnClickListener(this);

        // Inicializamos los métodos para cada Spinner
        SpinnerUnoMayorPersonalizado();
        SpinnerDosMayorPersonalizado();
        SpinnerTresMayorPersonalizado();
        SpinnerCuatroMayorPersonalizado();
        SpinnerCincoMayorPersonalizado();
        SpinnerSeisMayorPersonalizado();
        SpinnerSieteMayorPersonalizado();


        /* A cada refrencia de los spinner lo enlazamos con su id */
        spinnerUnoMayor7 = (Spinner) findViewById(R.id.SPUnoMayor7);
        spinnerDosMayor7 = (Spinner) findViewById(R.id.SPDosMayor7);
        spinnerTresMayor7 = (Spinner) findViewById(R.id.SPTresMayor7);
        spinnerCuatroMayor7 = (Spinner) findViewById(R.id.SPCuatroMayor7);
        spinnerCincoMayor7 = (Spinner) findViewById(R.id.SPCincoMayor7);
        spinnerSeisMayor7 = (Spinner) findViewById(R.id.SPSeisMayor7);
        spinnerSieteMayor7 = (Spinner) findViewById(R.id.SPSieteMayor7);


    /* Con el método "etOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerUnoMayor7.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected (AdapterView < ? > parent, View view,int position, long id){
        SpinnerItemUnoMayor7 clickedItem = (SpinnerItemUnoMayor7) parent.getItemAtPosition(position);
        String clickedCountryName = clickedItem.getNombreItemMayor7();
        if (clickedCountryName.equals(getString(R.string.simayor7))) {

            if (SPsimayor7 == "") {
                SPsimayor7 = "CORRECTO: " + clickedCountryName;
            } else {
                SPsimayor7 += " - " + "CORRECTO: " + clickedCountryName;
            }

            LLunomayor7.setBackgroundResource(R.color.botoncolor);
            spinnerUnoMayor7.setEnabled(false);
            Correcto();
        } else if (clickedCountryName.equals(getString(R.string.opcion))) {
        } else {

            if (SPsimayor7 == "") {
                SPsimayor7 = "INCORRECTO: " + clickedCountryName;
            } else {
                SPsimayor7 += " - " + "INCORRECTO: " + clickedCountryName;
            }

            LLunomayor7.setBackgroundResource(R.color.backgroundcolor);
            InCorrecto();
        }
    }

        @Override
        public void onNothingSelected (AdapterView < ? > parent){

    }
    });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerDosMayor7.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemDosMayor7 clickedItem = (SpinnerItemDosMayor7) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemMayor7();
                if (clickedCountryName.equals(getString(R.string.lamayor7))) {

                    if (SPlamayor7 == ""){
                        SPlamayor7 = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPlamayor7 += " - " + "CORRECTO: " + clickedCountryName;
                    }
                    LLdosmayor7.setBackgroundResource(R.color.botoncolor);
                    spinnerDosMayor7.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {


                    if (SPlamayor7 == ""){
                        SPlamayor7 = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPlamayor7 += " - " + "INCORRECTO: " + clickedCountryName;
                    }

                    LLdosmayor7.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerTresMayor7.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemTresMayor7 clickedItem = (SpinnerItemTresMayor7) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemMayor7();
                if (clickedCountryName.equals(getString(R.string.solmayor7))) {

                    if (SPfamayor7 == ""){
                        SPfamayor7 = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPfamayor7 += " - "+ "CORRECTO: " +clickedCountryName;
                    }
                    LLtresmayor7.setBackgroundResource(R.color.botoncolor);
                    spinnerTresMayor7.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {

                    if (SPfamayor7 == ""){
                        SPfamayor7 = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPfamayor7 += " - "+ "INCORRECTO: " +clickedCountryName;
                    }
                    LLtresmayor7.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerCuatroMayor7.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemCuatroMayor7 clickedItem = (SpinnerItemCuatroMayor7) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemMayor7();
                if (clickedCountryName.equals(getString(R.string.famayor7))) {
                    if (SPsolmayor7 == ""){
                        SPsolmayor7 = "CORRECTO: " + clickedCountryName;
                    }else{
                        SPsolmayor7 += " - " + "CORRECTO: " + clickedCountryName;
                    }
                    LLcuatromayor7.setBackgroundResource(R.color.botoncolor);
                    spinnerCuatroMayor7.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {

                    if (SPsolmayor7 == ""){
                        SPsolmayor7 = "INCORRECTO: " + clickedCountryName;
                    }else{
                        SPsolmayor7 += " - " + "INCORRECTO: " + clickedCountryName;
                    }

                    LLcuatromayor7.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerCincoMayor7.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemCincoMayor7 clickedItem = (SpinnerItemCincoMayor7) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemMayor7();
                if (clickedCountryName.equals(getString(R.string.mimayor7))) {

                    if (SPmimayor7 == ""){
                        SPmimayor7 = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPmimayor7 += " - " + "CORRECTO: " +clickedCountryName;
                    }
                    LLcincomayor7.setBackgroundResource(R.color.botoncolor);
                    spinnerCincoMayor7.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {


                    if (SPmimayor7 == ""){
                        SPmimayor7 = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPmimayor7 += " - " + "INCORRECTO: " +clickedCountryName;
                    }
                    LLcincomayor7.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerSeisMayor7.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenSeisMayor7 clickedItem = (SpinnerItenSeisMayor7) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemMayor7();
                if (clickedCountryName.equals(getString(R.string.remayor7))) {

                    if (SPremayor7 == ""){
                        SPremayor7 = "CORRECTO: " + clickedCountryName;
                    }else{
                        SPremayor7 += " - " + "CORRECTO: " + clickedCountryName;
                    }
                    LLseismayor7.setBackgroundResource(R.color.botoncolor);
                    spinnerSeisMayor7.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {


                    if (SPremayor7 == ""){
                        SPremayor7 = "INCORRECTO: " + clickedCountryName;
                    }else{
                        SPremayor7 += " - " + "INCORRECTO: " + clickedCountryName;
                    }
                    LLseismayor7.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerSieteMayor7.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemSieteMayor7 clickedItem = (SpinnerItemSieteMayor7) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemMayor7();
                if (clickedCountryName.equals(getString(R.string.domayor7))) {

                    if (SPdomayor7 == ""){
                        SPdomayor7 = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPdomayor7 += " - " + "CORRECTO: " +clickedCountryName ;
                    }
                    LLsietemayor7.setBackgroundResource(R.color.botoncolor);
                    spinnerSieteMayor7.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {

                    if (SPdomayor7 == ""){
                        SPdomayor7 = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPdomayor7 += " - " + "INCORRECTO: " +clickedCountryName ;
                    }

                    LLsietemayor7.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        /* Asignados el origen de los datos desde los recursos. */
        adapterSpinnerUnoMayor = new AdapterSpinnerUnoMayor(this, spinnerItenUnoMayor);
        adapterSpinnerDosMayor = new AdapterSpinnerDosMayor(this, spinnerItenDosMayor);
        adapterSpinnerTresMayor = new AdapterSpinnerTresMayor(this, spinnerItenTresMayor);
        adapterSpinnerCuatroMayor = new AdapterSpinnerCuatroMayor(this, spinnerItenCuatroMayor);
        adapterSpinnerCincoMayor = new AdapterSpinnerCincoMayor(this, spinnerItenCincoMayor);
        adapterSpinnerSeisMayor = new AdapterSpinnerSeisMayor(this, spinnerItenSeisMayor);
        adapterSpinnerSieteMayor = new AdaterSpinnerSieteMayor(this, spinnerItenSieteMayor);


        /* Seteamos el adaptador */
        spinnerUnoMayor7.setAdapter(adapterSpinnerUnoMayor);
        spinnerDosMayor7.setAdapter(adapterSpinnerDosMayor);
        spinnerTresMayor7.setAdapter(adapterSpinnerTresMayor);
        spinnerCuatroMayor7.setAdapter(adapterSpinnerCuatroMayor);
        spinnerCincoMayor7.setAdapter(adapterSpinnerCincoMayor);
        spinnerSeisMayor7.setAdapter(adapterSpinnerSeisMayor);
        spinnerSieteMayor7.setAdapter(adapterSpinnerSieteMayor);

        // firebase

        /* Obtenemos la instancia para acceder a la base de datos "firebase"*/
        firebaseDatabase = FirebaseDatabase.getInstance();

        /* Obtenemos instancia para autenticar el usuario*/
        firebaseAuth = FirebaseAuth.getInstance();

        /* Obtenemos instancia y referencias del almacenamiento de firebase */
        /* Obetenemos de la base de datos el usuario actual. */

        /* Le damos el nombre a la tabla que guardará los datos en Firebase*/
        logros_Adivina_Notas_Mayores_7 = firebaseDatabase.getReference("ADIVINA_LOGROS_MAYORES_7");


    }

    /* Creamos un arreglo para el segundo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerUnoMayorPersonalizado () {
        spinnerItenUnoMayor = new ArrayList<>();
        spinnerItenUnoMayor.add(new SpinnerItemUnoMayor7(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItemUnoMayor7(getString(R.string.domayor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItemUnoMayor7(getString(R.string.lamayor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItemUnoMayor7(getString(R.string.famayor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItemUnoMayor7(getString(R.string.remayor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItemUnoMayor7(getString(R.string.simayor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItemUnoMayor7(getString(R.string.mimayor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItemUnoMayor7(getString(R.string.solmayor7), R.drawable.ic_nota_dp));

    }

    /* Creamos un arreglo para el segundo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerDosMayorPersonalizado () {
        spinnerItenDosMayor = new ArrayList<>();
        spinnerItenDosMayor.add(new SpinnerItemDosMayor7(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItemDosMayor7(getString(R.string.remayor7), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItemDosMayor7(getString(R.string.famayor7), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItemDosMayor7(getString(R.string.domayor7), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItemDosMayor7(getString(R.string.mimayor7), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItemDosMayor7(getString(R.string.solmayor7), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItemDosMayor7(getString(R.string.simayor7), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItemDosMayor7(getString(R.string.lamayor7), R.drawable.ic_nota_dp));


    }


    /* Creamos un arreglo para el tercer spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerTresMayorPersonalizado () {
        spinnerItenTresMayor = new ArrayList<>();
        spinnerItenTresMayor.add(new SpinnerItemTresMayor7(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItemTresMayor7(getString(R.string.solmayor7), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItemTresMayor7(getString(R.string.famayor7), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItemTresMayor7(getString(R.string.mimayor7), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItemTresMayor7(getString(R.string.remayor7), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItemTresMayor7(getString(R.string.domayor7), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItemTresMayor7(getString(R.string.simayor7), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItemTresMayor7(getString(R.string.lamayor7), R.drawable.ic_nota_dp));


    }

    /* Creamos un arreglo para el cuarto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerCuatroMayorPersonalizado () {
        spinnerItenCuatroMayor = new ArrayList<>();
        spinnerItenCuatroMayor.add(new SpinnerItemCuatroMayor7(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItemCuatroMayor7(getString(R.string.solmayor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItemCuatroMayor7(getString(R.string.domayor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItemCuatroMayor7(getString(R.string.lamayor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItemCuatroMayor7(getString(R.string.famayor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItemCuatroMayor7(getString(R.string.simayor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItemCuatroMayor7(getString(R.string.mimayor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItemCuatroMayor7(getString(R.string.remayor7), R.drawable.ic_nota_dp));


    }

    /* Creamos un arreglo para el quinto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerCincoMayorPersonalizado () {
        spinnerItenCincoMayor = new ArrayList<>();
        spinnerItenCincoMayor.add(new SpinnerItemCincoMayor7(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItemCincoMayor7(getString(R.string.simayor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItemCincoMayor7(getString(R.string.lamayor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItemCincoMayor7(getString(R.string.solmayor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItemCincoMayor7(getString(R.string.famayor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItemCincoMayor7(getString(R.string.mimayor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItemCincoMayor7(getString(R.string.remayor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItemCincoMayor7(getString(R.string.domayor7), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el sexto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerSeisMayorPersonalizado () {
        spinnerItenSeisMayor = new ArrayList<>();
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor7(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor7(getString(R.string.domayor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor7(getString(R.string.remayor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor7(getString(R.string.mimayor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor7(getString(R.string.famayor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor7(getString(R.string.solmayor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor7(getString(R.string.lamayor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor7(getString(R.string.simayor7), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el septimo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerSieteMayorPersonalizado () {
        spinnerItenSieteMayor = new ArrayList<>();
        spinnerItenSieteMayor.add(new SpinnerItemSieteMayor7(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItemSieteMayor7(getString(R.string.domayor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItemSieteMayor7(getString(R.string.mimayor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItemSieteMayor7(getString(R.string.remayor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItemSieteMayor7(getString(R.string.famayor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItemSieteMayor7(getString(R.string.lamayor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItemSieteMayor7(getString(R.string.solmayor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItemSieteMayor7(getString(R.string.simayor7), R.drawable.ic_nota_dp));


    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /* Se reproduce el sonido del primer botón */
            case R.id.SPImagenUnoMayor7:
                MPsieteMayor7.start();
                break;

            /* Se reproduce el sonido del segundo botón */
            case R.id.SPImagenDosMayor7:
                MPseisMayor7.start();
                break;

            /* Se reproduce el sonido del tercer botón */
            case R.id.SPImagenTresMayor7:
                MPcincoMayor7.start();
                break;

            /* Se reproduce el sonido del cuarto botón */
            case R.id.SPImagenCuatroMayor7:
                MPcuatroMayor7.start();
                break;

            /* Se reproduce el sonido del quinto botón */
            case R.id.SPImagenCincoMayor7:
                MPtresMayor7.start();
                break;

            /* Se reproduce el sonido del sexto botón */
            case R.id.SPImagenSeisMayor7:
                MPdosMayor7.start();
                break;

            /* Se reproduce el sonido del septimo botón */
            case R.id.SPImagenSieteMayor7:
                MPunoMayor7.start();
                break;

        }


    }
    /* Método ""InCorreto para mostrar que está incorrecto, es llama cada vez que no se cumplen las condiciones en los botones*/
    private void InCorrecto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.incorrecto));
        imageView.setImageResource(R.drawable.ic_marca_de_cancelar);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
        contadorAdivinaNotasMayores7++;
    }

    /* Método "Correto" para mostrar que está correcto, es llama cada vez que se cumplen las condiciones en los botones*/
    private void Correcto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.correcto));
        imageView.setImageResource(R.drawable.ic_correcto_simbolo);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
        contadorPruebaAdivinaNotasMayores7++;
    }

    private void GuardarRecordAdivinaMayor7() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("RECORD_NOTAS_ADIVINA_MAYORES_7", contadorAdivinaNotasMayores7);
        editor.apply();
    }

    private void GuardarAdivinaCorrectoMayor7() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (contadorPruebaAdivinaNotasMayores7 ==7){
            editor.putString("GUARDAR_RECORD_ADIVINA_MAYOR_7", "SI");
        }else {
            editor.putString("GUARDAR_RECORD_ADIVINA_MAYOR_7", "NO");
        }
        editor.apply();
    }
    /*Método para bloquear el botón BACK*/
    @Override
    public void onBackPressed(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View guardar = layoutInflater.inflate(R.layout.guardar_notas_menores, null);
        final CardView Guardar = (CardView)guardar.findViewById(R.id.CardGuardar);
        final CardView Cancelar = (CardView)guardar.findViewById(R.id.CardCancelar);

        Guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
          GuardarRecordAdivinaMayor7();
          GuardarAdivinaCorrectoMayor7();

                LogrosAdivinaNotasMayores7 LAdivinaNotasMayores7 = new LogrosAdivinaNotasMayores7();
                LAdivinaNotasMayores7.setADoMayor7(SPdomayor7);
                LAdivinaNotasMayores7.setAReMayor7(SPremayor7);
                LAdivinaNotasMayores7.setAMiMayor7(SPmimayor7);
                LAdivinaNotasMayores7.setAFaMayor7(SPmimayor7);
                LAdivinaNotasMayores7.setASolMayor7(SPfamayor7);
                LAdivinaNotasMayores7.setALaMayor7(SPlamayor7);
                LAdivinaNotasMayores7.setASiMayor7(SPsimayor7);

                final String id = user.get(sesion.USER_ID);
                logros_Adivina_Notas_Mayores_7.child(id).setValue(LAdivinaNotasMayores7).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        startActivity(new Intent(AdivinaNotasMayores7.this, AdivinaLaNota.class));
                        Toast.makeText(AdivinaNotasMayores7.this, "Datos guardados de forma correcta", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                    }
                });


            }
        });

        Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        builder.setView(guardar);
        builder.show();

    }

    //metodo de tipo booleano para comprobar la conexion a internet
    public Boolean isOnlineNet() {
        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");
            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

}

