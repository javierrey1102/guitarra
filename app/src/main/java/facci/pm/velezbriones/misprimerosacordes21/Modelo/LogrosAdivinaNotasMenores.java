package facci.pm.velezbriones.misprimerosacordes21.Modelo;

public class LogrosAdivinaNotasMenores {
    private String ADoMenor, AReMenor, AMiMenor, AFaMenor, ASolMenor, ALaMenor, ASiMenor, ANombreMenor;
    public  LogrosAdivinaNotasMenores(){

         }

    public String getADoMenor() {
        return ADoMenor;
    }

    public void setADoMenor(String ADoMenor) {
        this.ADoMenor = ADoMenor;
    }

    public String getAReMenor() {
        return AReMenor;
    }

    public void setAReMenor(String AReMenor) {
        this.AReMenor = AReMenor;
    }

    public String getAMiMenor() {
        return AMiMenor;
    }

    public void setAMiMenor(String AMiMenor) {
        this.AMiMenor = AMiMenor;
    }

    public String getAFaMenor() {
        return AFaMenor;
    }

    public void setAFaMenor(String AFaMenor) {
        this.AFaMenor = AFaMenor;
    }

    public String getASolMenor() {
        return ASolMenor;
    }

    public void setASolMenor(String ASolMenor) {
        this.ASolMenor = ASolMenor;
    }

    public String getALaMenor() {
        return ALaMenor;
    }

    public void setALaMenor(String ALaMenor) {
        this.ALaMenor = ALaMenor;
    }

    public String getASiMenor() {
        return ASiMenor;
    }

    public void setASiMenor(String ASiMenor) {
        this.ASiMenor = ASiMenor;
    }

    public String getANombreMenor() {
        return ANombreMenor;
    }

    public void setANombreMenor(String ANombreMenor) {
        this.ANombreMenor = ANombreMenor;
    }
}
