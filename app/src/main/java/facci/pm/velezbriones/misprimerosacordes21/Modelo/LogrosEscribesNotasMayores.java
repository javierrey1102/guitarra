package facci.pm.velezbriones.misprimerosacordes21.Modelo;

public class LogrosEscribesNotasMayores {
    private String EDoMayor, EReMayor, EMiMayor, ESolMayor, ELaMayor, ESiMayor, ENombreMayor;



    public  LogrosEscribesNotasMayores(){

    }

    public String getEDoMayor() {
        return EDoMayor;
    }

    public void setEDoMayor(String EDoMayor) {
        this.EDoMayor = EDoMayor;
    }

    public String getEReMayor() {
        return EReMayor;
    }

    public void setEReMayor(String EReMayor) {
        this.EReMayor = EReMayor;
    }

    public String getEMiMayor() {
        return EMiMayor;
    }

    public void setEMiMayor(String EMiMayor) {
        this.EMiMayor = EMiMayor;
    }

    public String getESolMayor() {
        return ESolMayor;
    }

    public void setESolMayor(String ESolMayor) {
        this.ESolMayor = ESolMayor;
    }

    public String getELaMayor() {
        return ELaMayor;
    }

    public void setELaMayor(String ELaMayor) {
        this.ELaMayor = ELaMayor;
    }

    public String getESiMayor() {
        return ESiMayor;
    }

    public void setESiMayor(String ESiMayor) {
        this.ESiMayor = ESiMayor;
    }
    public String getENombreMayor() {
        return ENombreMayor;
    }

    public void setENombreMayor(String ENombreMayor) {
        this.ENombreMayor = ENombreMayor;
    }
}
