package facci.pm.velezbriones.misprimerosacordes21.Fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosEscribesNotasMayores;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosEscribesNotasMayores7;
import facci.pm.velezbriones.misprimerosacordes21.R;



public class EscribesLogrosMayoresSiete extends Fragment {
    //Instanciamos cada objeto de la clase privada ListView y FirebaseListAdapter
    private ListView listView;
    private FirebaseListAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_escribes_logros_mayores_siete, container, false);
        // a la referencia de listView lo enlazamos con su id
        listView = (ListView)view.findViewById(R.id.LVMostrarNotasMayorSieteFragmento);
        //obtenemos el nombre de la tabla de donde queremos extraer los datos
        final DatabaseReference query = FirebaseDatabase.getInstance().getReference().child("ESCRIBE_LOGROS_MAYORES_7");
        // creamos una instancia "FirebaseListOptions" que se implementa en una lista "logrosMayoresFirebaseListOptions"
        // a esa lista le pasamos una clase que son todos los items que contiene la tabla "LogrosMayores"
        // todos esos datos se guardará en un XML "logrosmayores" después le pasamos la referencia de la base de datos mas la clase "logrosmayores"
        final FirebaseListOptions<LogrosEscribesNotasMayores7> logrosMayoresSieteFirebaseListOptions =
                new FirebaseListOptions.Builder<LogrosEscribesNotasMayores7>()
                        .setLayout(R.layout.logros_mayores_siete)
                        .setQuery(query, LogrosEscribesNotasMayores7.class).build();
        // inicializamos el "FirebaseListAdapter" y le pasamos la instancia "FirebaseListOptions"
        adapter = new FirebaseListAdapter(logrosMayoresSieteFirebaseListOptions) {
            //Inicializamos todos los campos que estén en el XML y le pasamos la vista "v" y por último se setea los campos en el layout.
            @Override
            protected void populateView(View v, Object model, int position) {

                TextInputEditText EdoMayor7 = (TextInputEditText)v.findViewById(R.id.EDoMayor7);
                TextInputEditText EreMayor7 = (TextInputEditText)v.findViewById(R.id.EReMayor7);
                TextInputEditText EmiMayor7 = (TextInputEditText)v.findViewById(R.id.EMiMayor7);
                TextInputEditText EsolMayor7 = (TextInputEditText)v.findViewById(R.id.ESolMayor7);
                TextInputEditText ElaMayor7 = (TextInputEditText)v.findViewById(R.id.ELaMayor7);
                TextInputEditText EsiMayor7 = (TextInputEditText)v.findViewById(R.id.ESiMayor7);
                TextInputEditText EscribenombresUsuario7 = (TextInputEditText)v.findViewById(R.id.NombreUserEscribeMayorSiete);

                LogrosEscribesNotasMayores7 logrosMayores7 = (LogrosEscribesNotasMayores7) model;
                EdoMayor7.setText(logrosMayores7.getEDoMayor7());
                EreMayor7.setText(logrosMayores7.getEReMayor7());
                EmiMayor7.setText(logrosMayores7.getEMiMayor7());
                EsolMayor7.setText(logrosMayores7.getESolMayor7());
                ElaMayor7.setText(logrosMayores7.getELaMayor7());
                EsiMayor7.setText(logrosMayores7.getESiMayor7());
                EscribenombresUsuario7.setText(logrosMayores7.getENombreMayor7());

            }
        };
        listView.setAdapter(adapter);
        return view;
    }
    //ciclo de vida de la aplicación, cuando se inicia la aplicación entonces se inicia el adapter
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }
    //ciclo de vida de la aplicación, cuando se finaliza la aplicación entonces se finaliza  el adapter
    // para que si la aplicación está en segundo plano no consuma recursos
    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
