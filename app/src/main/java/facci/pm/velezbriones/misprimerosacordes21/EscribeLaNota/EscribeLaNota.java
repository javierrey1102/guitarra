package facci.pm.velezbriones.misprimerosacordes21.EscribeLaNota;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EscribeLaNota extends AppCompatActivity implements View.OnClickListener{
    private LinearLayout escribenotasMenor, escribeNotasMayor, escribeNotasMenor7, escribesNotasMayor7;
    private TextView erroresMenores, erroresMayores, erroresMenores7, erroresMayores7, notasMenorCompletado, notasMayorCompletado, notasMenorCompletado7, notasMayorCompletado7;
    private int RecordMenores, RecordMayores, RecorddMenores7, RecordMayores7;
    private String RecordMenoresCompletado, RecordMayoresCompletado, RecordMenoresCompletado7, RecordMayorCompletado7;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());


        setContentView(R.layout.activity_escribe_la_nota);

        escribenotasMenor = (LinearLayout)findViewById(R.id.LLEscribeNotasMenor);
        escribeNotasMayor = (LinearLayout)findViewById(R.id.LLEscribeNotasMayor);
        escribeNotasMenor7 = (LinearLayout)findViewById(R.id.LLEscribeNotasMenores7);
        escribesNotasMayor7 = (LinearLayout)findViewById(R.id.LLEscribeNotasMayores7);
        escribenotasMenor.setOnClickListener(this);
        escribeNotasMayor.setOnClickListener(this);
        escribeNotasMenor7.setOnClickListener(this);
        escribesNotasMayor7.setOnClickListener(this);


    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.LLEscribeNotasMenor:
                startActivity(new Intent(EscribeLaNota.this, EscribesNotasMenores.class));
                break;
            case  R.id.LLEscribeNotasMayor:
                startActivity(new Intent(EscribeLaNota.this, EscribeNotasMayores.class));
                break;
            case R.id.LLEscribeNotasMenores7:
                startActivity(new Intent(EscribeLaNota.this, EscribeNotasMenores7.class));
                break;
            case R.id.LLEscribeNotasMayores7:
                startActivity(new Intent(EscribeLaNota.this, EscribesNotasMayores7.class));
                break;

        }
    }

    private void LeerRecord() {
        erroresMenores = (TextView) findViewById(R.id.TVNotasMenorErrores);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        RecordMenores = sharedPreferences.getInt("RECORD", 0);
        erroresMenores.setText(String.valueOf(RecordMenores));
    }


    private void LeerRecordNotasMayor() {
        erroresMayores = (TextView) findViewById(R.id.TVNotasMayorErrores);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        RecordMayores = sharedPreferences.getInt("RECORD_NOTAS_MAYORES", 0);
        erroresMayores.setText(String.valueOf(RecordMayores));
    }

    private void LeerRecordNotasMenores7() {
        erroresMenores7 = (TextView) findViewById(R.id.TVNotasMenores7Errores);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        RecorddMenores7 = sharedPreferences.getInt("RECORD_NOTAS_MENORES_7", 0);
        erroresMenores7.setText(String.valueOf(RecorddMenores7));
    }

    private void LeerRecordNotasMayores7() {
        erroresMayores7 = (TextView) findViewById(R.id.TVNotasMayores7Errores);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        RecordMayores7 = sharedPreferences.getInt("RECORD_NOTAS_MAYORES_7", 0);
        erroresMayores7.setText(String.valueOf(RecordMayores7));
    }

    private void LeerRecordNotasMenoresCompletado() {
        notasMenorCompletado = (TextView) findViewById(R.id.TVNotasMenorCompletado);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        RecordMenoresCompletado = sharedPreferences.getString("RECORD_MENOR", "NO");
            notasMenorCompletado.setText(RecordMenoresCompletado);
        }

    private void LeerRecordNotasMayoresCompletado() {
        notasMayorCompletado = (TextView) findViewById(R.id.TVNotasMayorCompletado);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        RecordMayoresCompletado = sharedPreferences.getString("RECORD_MAYOR", "NO");
        notasMayorCompletado.setText(RecordMayoresCompletado);
    }

    private void LeerRecordNotasMenoresCompletado7() {
        notasMenorCompletado7 = (TextView) findViewById(R.id.TVNotasMenores7Completado);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        RecordMenoresCompletado7 = sharedPreferences.getString("RECORD_MENOR_7", "NO");
        notasMenorCompletado7.setText(RecordMenoresCompletado7);
    }

    private void LeerRecordNotasMayoresCompletado7() {
        notasMayorCompletado7 = (TextView) findViewById(R.id.TVNotasMayores7Completado);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        RecordMayorCompletado7 = sharedPreferences.getString("RECORD_MAYOR_7", "NO");
        notasMayorCompletado7.setText(RecordMayorCompletado7);
    }




    @Override
    protected void onResume() {
        super.onResume();
        LeerRecord();
        LeerRecordNotasMayor();
        LeerRecordNotasMenores7();
        LeerRecordNotasMayores7();
        LeerRecordNotasMenoresCompletado();
        LeerRecordNotasMayoresCompletado();
        LeerRecordNotasMenoresCompletado7();
        LeerRecordNotasMayoresCompletado7();
    }

}

