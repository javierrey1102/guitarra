package facci.pm.velezbriones.misprimerosacordes21.Modelo;

public class LogrosAdivinaNotasMayores {
    private String ADoMayor, AReMayor, AMiMayor, AFaMayor, ASolMayor, ALaMayor, ASiMayor, ANombreMayor;
    public LogrosAdivinaNotasMayores(){

    }

    public String getADoMayor() {
        return ADoMayor;
    }

    public void setADoMayor(String ADoMayor) {
        this.ADoMayor = ADoMayor;
    }

    public String getAReMayor() {
        return AReMayor;
    }

    public void setAReMayor(String AReMayor) {
        this.AReMayor = AReMayor;
    }

    public String getAMiMayor() {
        return AMiMayor;
    }

    public void setAMiMayor(String AMiMayor) {
        this.AMiMayor = AMiMayor;
    }

    public String getAFaMayor() {
        return AFaMayor;
    }

    public void setAFaMayor(String AFaMayor) {
        this.AFaMayor = AFaMayor;
    }

    public String getASolMayor() {
        return ASolMayor;
    }

    public void setASolMayor(String ASolMayor) {
        this.ASolMayor = ASolMayor;
    }

    public String getALaMayor() {
        return ALaMayor;
    }

    public void setALaMayor(String ALaMayor) {
        this.ALaMayor = ALaMayor;
    }

    public String getASiMayor() {
        return ASiMayor;
    }

    public void setASiMayor(String ASiMayor) {
        this.ASiMayor = ASiMayor;
    }

    public String getANombreMayor() {
        return ANombreMayor;
    }

    public void setANombreMayor(String ANombreMayor) {
        this.ANombreMayor = ANombreMayor;
    }
}
