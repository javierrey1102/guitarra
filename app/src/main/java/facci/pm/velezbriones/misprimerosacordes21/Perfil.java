package facci.pm.velezbriones.misprimerosacordes21;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;

import facci.pm.velezbriones.misprimerosacordes21.Clases.Sesion;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Perfil extends AppCompatActivity {
    private TextView namePerfil, contadorTodasLasNotasEscuchadas;
    private int todasLasNotasEscuchadasContador;
    private Sesion sesion;
    private ImageView userfoto;
    private HashMap<String, String> user;



    //cambiar
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //de aqui
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        //hasta aqui
        setContentView(R.layout.activity_perfil);

        LeerTodasLasNotasEscuchadas();



        sesion = new Sesion(this);
        user = sesion.LoggedInUser();
        namePerfil = (TextView)findViewById(R.id.TVNombreUsuario);
        namePerfil.setText(user.get(sesion.USER_NAME));
        userfoto = (ImageView)findViewById(R.id.IMGUsuario);


        int numero = Integer.valueOf(user.get(sesion.USER_ICONO));
        if (numero == 1){
            userfoto.setImageResource(R.drawable.fantasma1);
        }if (numero == 2){
            userfoto.setImageResource(R.drawable.fantasma2);
        }if (numero == 3){
            userfoto.setImageResource(R.drawable.fantasma3);
        }if (numero == 4){
            userfoto.setImageResource(R.drawable.fantasma4);
        }if (numero == 5){
            userfoto.setImageResource(R.drawable.fantasma5);
        }if (numero == 6){
            userfoto.setImageResource(R.drawable.fantasma6);
        }if (numero == 7){
            userfoto.setImageResource(R.drawable.fantasma7);
        }if (numero == 8){
            userfoto.setImageResource(R.drawable.fantasma8);
        }if (numero == 9){
            userfoto.setImageResource(R.drawable.fantasma9);
        }if (numero == 10){
            userfoto.setImageResource(R.drawable.fantasma10);
        }if (numero == 11){
            userfoto.setImageResource(R.drawable.fantasma11);
        }if (numero == 12){
            userfoto.setImageResource(R.drawable.fantasma12);
        }if (numero == 13){
            userfoto.setImageResource(R.drawable.fantasma13);
        }if (numero == 14){
            userfoto.setImageResource(R.drawable.fantasma14);
        }if (numero == 15){
            userfoto.setImageResource(R.drawable.fantasma15);
        }
    }

    private void LeerTodasLasNotasEscuchadas(){
        contadorTodasLasNotasEscuchadas = (TextView) findViewById(R.id.TVTotalNotasEscuchadas);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        todasLasNotasEscuchadasContador = sharedPreferences.getInt("TOTAL_NOTAS_ESCUCHADAS", 0);
        contadorTodasLasNotasEscuchadas.setText(String.valueOf(todasLasNotasEscuchadasContador));
    }
}
