package facci.pm.velezbriones.misprimerosacordes21.Fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosAdivinaNotasMayores;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosAdivinaNotasMenores;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosEscribeNotasMenores;
import facci.pm.velezbriones.misprimerosacordes21.R;


public class AdivinaLogrosMenores extends Fragment {
    //Instanciamos cada objeto de la clase privada ListView y FirebaseListAdapter
    private ListView listView;
    private FirebaseListAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_adivina_logros_menores, container, false);
        // a la referencia de listView lo enlazamos con su id
        listView = (ListView) view.findViewById(R.id.LVMostrarNotasAdivinaMenoresFragmento);
        //obtenemos el nombre de la tabla de donde queremos extraer los datos
        final DatabaseReference query = FirebaseDatabase.getInstance().getReference().child("ADIVINA_LOGROS_MENORES");
        // creamos una instancia "FirebaseListOptions" que se implementa en una lista "logrosMayoresFirebaseListOptions"
        // a esa lista le pasamos una clase que son todos los items que contiene la tabla "LogrosAdivinaMayor"
        // todos esos datos se guardará en un XML "logros_adivina_7_mayor" después le pasamos la referencia
        // de la base de datos mas la clase "logros_adivina_7_mayor"
        final FirebaseListOptions<LogrosAdivinaNotasMenores> logrosAdivinaNotasMenoresFirebaseListOptions =
                new FirebaseListOptions.Builder<LogrosAdivinaNotasMenores>()
                .setLayout(R.layout.logros_adivina_menores)
                .setQuery(query,LogrosAdivinaNotasMenores.class).build();
        // inicializamos el "FirebaseListAdapter" y le pasamos la instancia "FirebaseListOptions"

        adapter = new FirebaseListAdapter(logrosAdivinaNotasMenoresFirebaseListOptions) {

            //Inicializamos todos los campos que estén en el XML y le pasamos la vista "v" y por último se setea los campos en el layout.
            @Override
            protected void populateView(View v, Object model, int position) {

                TextInputEditText AdoMenor = (TextInputEditText) v.findViewById(R.id.DoMenorAdivina);
                TextInputEditText AreMenor = (TextInputEditText) v.findViewById(R.id.ReMenorAdivina);
                TextInputEditText AmiMenor = (TextInputEditText) v.findViewById(R.id.MiMenorAdivina);
                TextInputEditText AsolMenor = (TextInputEditText) v.findViewById(R.id.SolMenorAdivina);
                TextInputEditText AlaMenor = (TextInputEditText) v.findViewById(R.id.LaMenorAdivina);
                TextInputEditText AsiMenor = (TextInputEditText) v.findViewById(R.id.SiMenorAdivina);
                TextInputEditText AnombreMenor = (TextInputEditText) v.findViewById(R.id.NombreUserAdivinaNotasMenor);

                LogrosAdivinaNotasMenores logrosEscribeNotasMenores = (LogrosAdivinaNotasMenores) model;
                AdoMenor.setText(logrosEscribeNotasMenores.getADoMenor());
                AreMenor.setText(logrosEscribeNotasMenores.getAReMenor());
                AmiMenor.setText(logrosEscribeNotasMenores.getAMiMenor());
                AsolMenor.setText(logrosEscribeNotasMenores.getASolMenor());
                AlaMenor.setText(logrosEscribeNotasMenores.getALaMenor());
                AsiMenor.setText(logrosEscribeNotasMenores.getASiMenor());
                AnombreMenor.setText(logrosEscribeNotasMenores.getANombreMenor());

            }
        };
        listView.setAdapter(adapter);
        return view;

    }
    //ciclo de vida de la aplicación, cuando se inicia la aplicación entonces se inicia el adapter
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }
    //ciclo de vida de la aplicación, cuando se finaliza la aplicación entonces se finaliza  el adapter
    // para que si la aplicación está en segundo plano no consuma recursos
    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }


}
