package facci.pm.velezbriones.misprimerosacordes21.Fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosAdivinaNotasMenores;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosAdivinaNotasMenores7;
import facci.pm.velezbriones.misprimerosacordes21.R;

public class AdivinaLogrosMenoresSiete extends Fragment {
    //Instanciamos cada objeto de la clase privada ListView y FirebaseListAdapter
    private ListView listView;
    private FirebaseListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_adivina_logros_menores_siete, container, false);
        listView = (ListView) view.findViewById(R.id.LVMostrarNotasAdivinaMenoresSieteFragmento);
        //obtenemos el nombre de la tabla de donde queremos extraer los datos
        final DatabaseReference query = FirebaseDatabase.getInstance().getReference().child("ADIVINA_LOGROS_MENORES_7");
        // creamos una instancia "FirebaseListOptions" que se implementa en una lista "logrosMayoresFirebaseListOptions"
        // a esa lista le pasamos una clase que son todos los items que contiene la tabla "LogrosAdivinaMayor"
        // todos esos datos se guardará en un XML "logros_adivina_7_mayor" después le pasamos la referencia
        // de la base de datos mas la clase "logros_adivina_7_mayor"
        final FirebaseListOptions<LogrosAdivinaNotasMenores7> logrosAdivinaNotasMenoresSieteFirebaseListOptions =
                new FirebaseListOptions.Builder<LogrosAdivinaNotasMenores7>()
                        .setLayout(R.layout.logros_adivina_menores_siete)
                        .setQuery(query,LogrosAdivinaNotasMenores7.class).build();
        // inicializamos el "FirebaseListAdapter" y le pasamos la instancia "FirebaseListOptions"

        adapter = new FirebaseListAdapter(logrosAdivinaNotasMenoresSieteFirebaseListOptions) {

            //Inicializamos todos los campos que estén en el XML y le pasamos la vista "v" y por último se setea los campos en el layout.
            @Override
            protected void populateView(View v, Object model, int position) {

                TextInputEditText AdoMenor7 = (TextInputEditText) v.findViewById(R.id.DoMenorAdivina7);
                TextInputEditText AreMenor7 = (TextInputEditText) v.findViewById(R.id.ReMenorAdivina7);
                TextInputEditText AmiMenor7 = (TextInputEditText) v.findViewById(R.id.MiMenorAdivina7);
                TextInputEditText AsolMenor7 = (TextInputEditText) v.findViewById(R.id.SolMenorAdivina7);
                TextInputEditText AlaMenor7 = (TextInputEditText) v.findViewById(R.id.LaMenorAdivina7);
                TextInputEditText AsiMenor7 = (TextInputEditText) v.findViewById(R.id.SiMenorAdivina7);
                TextInputEditText AnombreMenor7 = (TextInputEditText) v.findViewById(R.id.NombreUserAdivinaNotasMenor7);

                LogrosAdivinaNotasMenores7 logrosEscribeNotasMenores7 = (LogrosAdivinaNotasMenores7) model;
                AdoMenor7.setText(logrosEscribeNotasMenores7.getADoMenor7());
                AreMenor7.setText(logrosEscribeNotasMenores7.getAReMenor7());
                AmiMenor7.setText(logrosEscribeNotasMenores7.getAMiMenor7());
                AsolMenor7.setText(logrosEscribeNotasMenores7.getASolMenor7());
                AlaMenor7.setText(logrosEscribeNotasMenores7.getALaMenor7());
                AsiMenor7.setText(logrosEscribeNotasMenores7.getASiMenor7());
                AnombreMenor7.setText(logrosEscribeNotasMenores7.getANombreMenor7());

            }
        };
        listView.setAdapter(adapter);
        return view;

    }
    //ciclo de vida de la aplicación, cuando se inicia la aplicación entonces se inicia el adapter
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }
    //ciclo de vida de la aplicación, cuando se finaliza la aplicación entonces se finaliza  el adapter
    // para que si la aplicación está en segundo plano no consuma recursos
    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }


}
