package facci.pm.velezbriones.misprimerosacordes21.EscribeLaNota;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores.SpinnerItemUnoMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.Clases.Sesion;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosEscribesNotasMayores7;
import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EscribesNotasMayores7 extends AppCompatActivity implements View.OnClickListener {
    // Instanciamos cada objeto a la clase privada ImageView, EditText, Button y MediaPlayer.
    private ImageView sonidoUnoMayor7, sonidoDosMayor7, sonidoTresMayor7, sonidoCuatroMayor7, sonidoCincoMayor7, sonidoSeisMayor7;
    private EditText respuestaUnoMayor7, respuestaDosMayor7, respuestaTresMayor7, respuestaCuatroMayor7, respuestaCincoMayor7, respuestaSeisMayor7;
    private LinearLayout comprobarUnoMayor7, comprobarDosMayor7, comprobarTresMayor7, comprobarCuatroMayor7, comprobarCincoMayor7, comprobarSeisMayor7;
    private LinearLayout correctoUnoMayor7, correctoDosMayor7, correctoTresMayor7, correctoCuatroMayor7, correctoCincoMayor7, correctoSeisMayor7;
    private MediaPlayer MPsonidounoMayor7, MPsonidodosMayor7, MPsonidotresMayor7, MpsonidocuatroMayor7, MPsonidocincoMayor7, MPsonidoseisMayor7;

    /*Creamos instancias de tipo String y las inicializamos en blanco*/
    private String doMayor7 = "";
    private String reMayor7 = "";
    private String miMayor7 = "";
    private String solMayor7 = "";
    private String lamMayor7 = "";
    private String siMayor7 = "";
    int contadorMayor7;

    //Todo lo que tenga que ver con firebase :v
    private ProgressDialog progressDialog;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference logros_Notas_Mayores_7, users;
    private FirebaseAuth.AuthStateListener authStateListener;
    private String userId;

    //local data
    private Sesion sesion;
    private HashMap<String, String> user;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());


        setContentView(R.layout.activity_escribes_notas_mayores7);

        sesion = new Sesion(this);
        user = sesion.LoggedInUser();



        // A cada referecia de ImageView lo enlazamos con su respectivo id.
        sonidoUnoMayor7 = (ImageView) findViewById(R.id.SonidoMayorUno7);
        sonidoDosMayor7 = (ImageView) findViewById(R.id.SonidoMayorDos7);
        sonidoTresMayor7 = (ImageView) findViewById(R.id.SonidoMayorTres7);
        sonidoCuatroMayor7 = (ImageView) findViewById(R.id.SonidoMayorCuatro7);
        sonidoCincoMayor7 = (ImageView) findViewById(R.id.SonidoMayorCinco7);
        sonidoSeisMayor7 = (ImageView) findViewById(R.id.SonidoMayorSeis7);

        //  A cada referencia de MediaPlayer lo enlazamos con su respectivo sonido
        MPsonidounoMayor7 = MediaPlayer.create(this, R.raw.do7);
        MPsonidodosMayor7 = MediaPlayer.create(this, R.raw.re7);
        MPsonidotresMayor7 = MediaPlayer.create(this, R.raw.mi7);
        MpsonidocuatroMayor7 = MediaPlayer.create(this, R.raw.sol7);
        MPsonidocincoMayor7 = MediaPlayer.create(this, R.raw.la7);
        MPsonidoseisMayor7 = MediaPlayer.create(this, R.raw.si7);

        // A cada referencia de los EditText lo enlazamos con su id.
        respuestaUnoMayor7 = (EditText) findViewById(R.id.TXTRespuestaMayorUno7);
        respuestaDosMayor7 = (EditText) findViewById(R.id.TXTRespuestaMayorDos7);
        respuestaTresMayor7 = (EditText) findViewById(R.id.TXTRespuestaMayorTres7);
        respuestaCuatroMayor7 = (EditText) findViewById(R.id.TXTRespuestaMayorCuatro7);
        respuestaCincoMayor7 = (EditText) findViewById(R.id.TXTRespuestaMayorCinco7);
        respuestaSeisMayor7 = (EditText) findViewById(R.id.TXTRespuestaMayorSeis7);


        // A cada referencia de los botones lo enlazamos con su id.
        comprobarUnoMayor7 = (LinearLayout) findViewById(R.id.BTNComprobarMayorUno7);
        comprobarDosMayor7 = (LinearLayout) findViewById(R.id.BTNComprobarMayorDos7);
        comprobarTresMayor7 = (LinearLayout) findViewById(R.id.BTNComprobarMayorTres7);
        comprobarCuatroMayor7 = (LinearLayout) findViewById(R.id.BTNComprobarMayorCuatro7);
        comprobarCincoMayor7 = (LinearLayout) findViewById(R.id.BTNComprobarMayorCinco7);
        comprobarSeisMayor7 = (LinearLayout) findViewById(R.id.BTNComprobarMayorSeis7);

        correctoUnoMayor7 = (LinearLayout) findViewById(R.id.LLUnoVerdeMayor7);
        correctoDosMayor7 = (LinearLayout) findViewById(R.id.LLDosVerdeMayor7);
        correctoTresMayor7 = (LinearLayout) findViewById(R.id.LLTresVerdeMayor7);
        correctoCuatroMayor7 = (LinearLayout) findViewById(R.id.LLCuatroVerdeMayor7);
        correctoCincoMayor7 = (LinearLayout) findViewById(R.id.LLCincoVerdeMayor7);
        correctoSeisMayor7 = (LinearLayout) findViewById(R.id.LLSeisVerdeMayor7);


        //Dentro de este método OnCreate asociamos el lístener a los botones.
        sonidoUnoMayor7.setOnClickListener(this);
        sonidoDosMayor7.setOnClickListener(this);
        sonidoTresMayor7.setOnClickListener(this);
        sonidoCuatroMayor7.setOnClickListener(this);
        sonidoCincoMayor7.setOnClickListener(this);
        sonidoSeisMayor7.setOnClickListener(this);

        //Al método OnCreate le asociamos el lístener a los botones.
        comprobarUnoMayor7.setOnClickListener(this);
        comprobarDosMayor7.setOnClickListener(this);
        comprobarTresMayor7.setOnClickListener(this);
        comprobarCuatroMayor7.setOnClickListener(this);
        comprobarCincoMayor7.setOnClickListener(this);
        comprobarSeisMayor7.setOnClickListener(this);

        // firebase

        /* Obtenemos la instancia para acceder a la base de datos "firebase"*/
        firebaseDatabase = FirebaseDatabase.getInstance();

        /* Obtenemos instancia para autenticar el usuario*/
        firebaseAuth = FirebaseAuth.getInstance();

        /* Obtenemos instancia y referencias del almacenamiento de firebase */
        /* Obetenemos de la base de datos el usuario actual. */

        /* Le damos el nombre a la tabla que guardará los datos en Firebase*/
        logros_Notas_Mayores_7 = firebaseDatabase.getReference("ESCRIBE_LOGROS_MAYORES_7");


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            /* Se reproduce el sonido del primer botón */
            case R.id.SonidoMayorUno7:
                MPsonidounoMayor7.start();
                break;

            case R.id.SonidoMayorDos7:
                /* Se reproduce el sonido del primer botón */
                MPsonidodosMayor7.start();
                break;

            case R.id.SonidoMayorTres7:
                /* Se reproduce el sonido del primer botón */
                MPsonidotresMayor7.start();
                break;

            case R.id.SonidoMayorCuatro7:
                /* Se reproduce el sonido del primer botón */
                MpsonidocuatroMayor7.start();
                break;

            case R.id.SonidoMayorCinco7:
                /* Se reproduce el sonido del primer botón */
                MPsonidocincoMayor7.start();
                break;

            case R.id.SonidoMayorSeis7:
                /* Se reproduce el sonido del primer botón */
                MPsonidoseisMayor7.start();
                break;


            case R.id.BTNComprobarMayorUno7:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaUnoMayor7.getText().toString().isEmpty()) {
                    respuestaUnoMayor7.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaUnoMayor7.getText().toString().equals(getString(R.string.domayor7)+" ") ||
                        respuestaUnoMayor7.getText().toString().equals(getString(R.string.domayor7)) ||
                        respuestaUnoMayor7.getText().toString().equals(getString(R.string.domayorLPMa7)+ " ") ||
                        respuestaUnoMayor7.getText().toString().equals(getString(R.string.domayorLPMa7))||
                        respuestaUnoMayor7.getText().toString().equals(getString(R.string.DdomayorLPMa7)+ " ") ||
                        respuestaUnoMayor7.getText().toString().equals(getString(R.string.DdomayorLPMa7))){


                    if (doMayor7==""){
                        doMayor7 = "CORRECTO: "+respuestaUnoMayor7.getText().toString();
                    }else {
                        doMayor7 +=  " - " + "CORRECTO: "+respuestaUnoMayor7.getText().toString();
                    }
                    comprobarUnoMayor7.setBackgroundResource(R.color.botoncolor);
                    correctoUnoMayor7.setBackgroundResource(R.color.botoncolor);
                    sonidoUnoMayor7.setImageResource(R.drawable.correct_note);
                    comprobarUnoMayor7.setEnabled(false);
                    respuestaUnoMayor7.setText("CORRECTO");
                    respuestaUnoMayor7.setEnabled(false);

                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */

                    if (doMayor7==""){
                        doMayor7 = "INCORRECTO: "+ respuestaUnoMayor7.getText().toString();
                    }else {
                        doMayor7 +=  " - " + "INCORRECTO: "+respuestaUnoMayor7.getText().toString();
                    }

                    InCorrecto();
                    respuestaUnoMayor7.setText("");
                }
                break;

            case R.id.BTNComprobarMayorDos7:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaDosMayor7.getText().toString().isEmpty()) {
                    respuestaDosMayor7.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaDosMayor7.getText().toString().equals(getString(R.string.remayor7)+" ") ||
                        respuestaDosMayor7.getText().toString().equals(getString(R.string.remayor7)) ||
                        respuestaDosMayor7.getText().toString().equals(getString(R.string.remayorLPMa7)+ " ") ||
                        respuestaDosMayor7.getText().toString().equals(getString(R.string.remayorLPMa7))||
                        respuestaDosMayor7.getText().toString().equals(getString(R.string.RremayorLPMa7)+ " ") ||
                        respuestaDosMayor7.getText().toString().equals(getString(R.string.RremayorLPMa7))){


                    if (reMayor7==""){
                        reMayor7 = "CORRECTO: "+respuestaDosMayor7.getText().toString();
                    }else {
                        reMayor7 +=  " - " + "CORRECTO: "+respuestaDosMayor7.getText().toString();
                    }
                    comprobarDosMayor7.setBackgroundResource(R.color.botoncolor);
                    correctoDosMayor7.setBackgroundResource(R.color.botoncolor);
                    sonidoDosMayor7.setImageResource(R.drawable.correct_note);
                    comprobarDosMayor7.setEnabled(false);
                    respuestaDosMayor7.setText("CORRECTO");
                    respuestaDosMayor7.setEnabled(false);



                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */

                    if (reMayor7==""){
                        reMayor7 = "INCORRECTO: "+ respuestaDosMayor7.getText().toString();
                    }else {
                        reMayor7 +=  " - " + "INCORRECTO: "+respuestaDosMayor7.getText().toString();
                    }


                    InCorrecto();
                    respuestaDosMayor7.setText("");
                }
                break;

            case R.id.BTNComprobarMayorTres7:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaTresMayor7.getText().toString().isEmpty()) {
                    respuestaTresMayor7.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaTresMayor7.getText().toString().equals(getString(R.string.mimayor7)+" ")  ||
                        respuestaTresMayor7.getText().toString().equals(getString(R.string.mimayorLPMa7)+ " ") ||
                        respuestaTresMayor7.getText().toString().equals(getString(R.string.mimayorLPMa7))||
                        respuestaTresMayor7.getText().toString().equals(getString(R.string.MmimayorLPMa7)+ " ") ||
                        respuestaTresMayor7.getText().toString().equals(getString(R.string.MmimayorLPMa7))){


                    if (miMayor7==""){
                        miMayor7 = "CORRECTO: "+respuestaTresMayor7.getText().toString();
                    }else {
                        miMayor7 +=  " - " + "CORRECTO: "+respuestaTresMayor7.getText().toString();
                    }
                    comprobarTresMayor7.setBackgroundResource(R.color.botoncolor);
                    correctoTresMayor7.setBackgroundResource(R.color.botoncolor);
                    sonidoTresMayor7.setImageResource(R.drawable.correct_note);
                    comprobarTresMayor7.setEnabled(false);
                    respuestaTresMayor7.setText("CORRECTO");
                    respuestaTresMayor7.setEnabled(false);



                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (miMayor7==""){
                        miMayor7 = "INCORRECTO: "+ respuestaTresMayor7.getText().toString();
                    }else {
                        miMayor7 +=  " - " + "INCORRECTO: "+respuestaTresMayor7.getText().toString();
                    }


                    InCorrecto();
                    respuestaTresMayor7.setText("");
                }
                break;

            case R.id.BTNComprobarMayorCuatro7:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaCuatroMayor7.getText().toString().isEmpty()) {
                    respuestaCuatroMayor7.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaCuatroMayor7.getText().toString().equals(getString(R.string.solmayor7)+" ")  ||
                        respuestaCuatroMayor7.getText().toString().equals(getString(R.string.solmayorLPMa7)+ " ") ||
                        respuestaCuatroMayor7.getText().toString().equals(getString(R.string.solmayorLPMa7))||
                        respuestaCuatroMayor7.getText().toString().equals(getString(R.string.SsolmayorLPMa7)+ " ") ||
                        respuestaCuatroMayor7.getText().toString().equals(getString(R.string.SsolmayorLPMa7))){

                    if (solMayor7==""){
                        solMayor7 = "CORRECTO: "+respuestaCuatroMayor7.getText().toString();
                    }else {
                        solMayor7 +=  " - " + "CORRECTO: "+respuestaCuatroMayor7.getText().toString();
                    }
                    comprobarCuatroMayor7.setBackgroundResource(R.color.botoncolor);
                    correctoCuatroMayor7.setBackgroundResource(R.color.botoncolor);
                    sonidoCuatroMayor7.setImageResource(R.drawable.correct_note);
                    comprobarCuatroMayor7.setEnabled(false);
                    respuestaCuatroMayor7.setText("CORRECTO");
                    respuestaCuatroMayor7.setEnabled(false);


                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */

                    if (solMayor7==""){
                        solMayor7 = "INCORRECTO: "+ respuestaCuatroMayor7.getText().toString();
                    }else {
                        solMayor7 +=  " - " + "INCORRECTO: "+respuestaCuatroMayor7.getText().toString();
                    }

                    InCorrecto();
                    respuestaCuatroMayor7.setText("");
                }
                break;

            case R.id.BTNComprobarMayorCinco7:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaCincoMayor7.getText().toString().isEmpty()) {
                    respuestaCincoMayor7.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaCincoMayor7.getText().toString().equals(getString(R.string.lamayor7)+" ")  ||
                        respuestaCincoMayor7.getText().toString().equals(getString(R.string.lamayorLPMa7)+ " ") ||
                        respuestaCincoMayor7.getText().toString().equals(getString(R.string.lamayorLPMa7))||
                        respuestaCincoMayor7.getText().toString().equals(getString(R.string.LlamayorLPMa7)+ " ") ||
                        respuestaCincoMayor7.getText().toString().equals(getString(R.string.LlamayorLPMa7))){


                    if (lamMayor7==""){
                        lamMayor7 = "CORRECTO: "+respuestaCincoMayor7.getText().toString();
                    }else {
                        lamMayor7 +=  " - " + "CORRECTO: "+respuestaCincoMayor7.getText().toString();
                    }
                    comprobarCincoMayor7.setBackgroundResource(R.color.botoncolor);
                    correctoCincoMayor7.setBackgroundResource(R.color.botoncolor);
                    sonidoCincoMayor7.setImageResource(R.drawable.correct_note);
                    comprobarCincoMayor7.setEnabled(false);
                    respuestaCincoMayor7.setText("CORRECTO");
                    respuestaCincoMayor7.setEnabled(false);


                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */

                    if (lamMayor7==""){
                        lamMayor7 = "INCORRECTO: "+ respuestaCincoMayor7.getText().toString();
                    }else {
                        lamMayor7 +=  " - " + "INCORRECTO: "+respuestaCincoMayor7.getText().toString();
                    }

                    InCorrecto();
                    respuestaCincoMayor7.setText("");
                }
                break;

            case R.id.BTNComprobarMayorSeis7:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaSeisMayor7.getText().toString().isEmpty()) {
                    respuestaSeisMayor7.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaSeisMayor7.getText().toString().equals(getString(R.string.simayor7)+" ")  ||
                        respuestaSeisMayor7.getText().toString().equals(getString(R.string.simayorLPMa7)+ " ") ||
                        respuestaSeisMayor7.getText().toString().equals(getString(R.string.simayorLPMa7))||
                        respuestaSeisMayor7.getText().toString().equals(getString(R.string.SsimayorLPMa7)+ " ") ||
                        respuestaSeisMayor7.getText().toString().equals(getString(R.string.SsimayorLPMa7))) {

                    if (siMayor7 == "") {
                        siMayor7= "CORRECTO: " + respuestaSeisMayor7.getText().toString();
                    } else {
                        siMayor7 += " - " + "CORRECTO: " + respuestaSeisMayor7.getText().toString();
                    }
                    comprobarSeisMayor7.setBackgroundResource(R.color.botoncolor);
                    correctoSeisMayor7.setBackgroundResource(R.color.botoncolor);
                    sonidoSeisMayor7.setImageResource(R.drawable.correct_note);
                    comprobarSeisMayor7.setEnabled(false);
                    respuestaSeisMayor7.setText("CORRECTO");
                    respuestaSeisMayor7.setEnabled(false);

                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (siMayor7==""){
                        siMayor7= "INCORECTO: "+respuestaSeisMayor7.getText().toString();
                    }else {
                        siMayor7+= " - "+ "INCORRECTO: " +respuestaSeisMayor7.getText().toString();
                    }
                    InCorrecto();
                    respuestaSeisMayor7.setText("");
                }
                break;

        }
    }

    private void GuardarRecordNotasMayores7() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("RECORD_NOTAS_MAYORES_7", contadorMayor7);
        editor.apply();
    }


    private void GuardarCorrectoMayor7() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if ((respuestaUnoMayor7.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaDosMayor7.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaTresMayor7.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaCuatroMayor7.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaCincoMayor7.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaSeisMayor7.getText().toString().equals(getString(R.string.correcto)))){
            editor.putString("GUARDAR_RECORD_ADIVINA_MAYOR_7", "SI");
        }else {
            editor.putString("GUARDAR_RECORD_ADIVINA_MAYOR_7", "NO");
        }
        editor.apply();
    }


    /*Método para bloquear el botón BACK*/
    @Override
    public void onBackPressed(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View guardar = layoutInflater.inflate(R.layout.guardar_notas_menores, null);
        final CardView Guardar = (CardView)guardar.findViewById(R.id.CardGuardar);
        final CardView Cancelar = (CardView)guardar.findViewById(R.id.CardCancelar);

        Guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarRecordNotasMayores7();
                GuardarCorrectoMayor7();

                LogrosEscribesNotasMayores7 LENMayores7 = new LogrosEscribesNotasMayores7();
                LENMayores7.setEDoMayor7(doMayor7);
                LENMayores7.setEReMayor7(reMayor7);
                LENMayores7.setEMiMayor7(miMayor7);
                LENMayores7.setESolMayor7(solMayor7);
                LENMayores7.setELaMayor7(lamMayor7);
                LENMayores7.setESiMayor7(siMayor7);

                if (isOnlineNet()==true) {
                    final String id = user.get(sesion.USER_ID);
                    logros_Notas_Mayores_7.child(id).setValue(LENMayores7).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            startActivity(new Intent(EscribesNotasMayores7.this, EscribeLaNota.class));
                            Toast.makeText(EscribesNotasMayores7.this, "Datos guardados con éxitos", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                        }
                    });

                }else
                    Toast.makeText(EscribesNotasMayores7.this, R.string.sinaccesoainternet, Toast.LENGTH_SHORT).show();

            }
        });

        Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        builder.setView(guardar);
        builder.show();

    }



    /* Método ""InCorreto para mostrar que está incorrecto, es llama cada vez que no se cumplen las condiciones en los botones*/
    private void InCorrecto(){

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.incorrecto));
        imageView.setImageResource(R.drawable.ic_marca_de_cancelar);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
        contadorMayor7++;
    }

    /* Método "Correto" para mostrar que está correcto, es llama cada vez que se cumplen las condiciones en los botones*/
    private void Correcto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.correcto));
        imageView.setImageResource(R.drawable.ic_correcto_simbolo);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }
    //metodo de tipo booleano para comprobar la conexion a internet
    public Boolean isOnlineNet() {
        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");
            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
}

