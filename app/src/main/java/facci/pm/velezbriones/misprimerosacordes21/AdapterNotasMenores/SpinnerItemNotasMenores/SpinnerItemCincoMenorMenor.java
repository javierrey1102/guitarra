package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.SpinnerItemNotasMenores;

public class SpinnerItemCincoMenorMenor {
    private String NombreitemCincoMenor;
    private int ImagenItemCincoMenor;

    public SpinnerItemCincoMenorMenor (String nombreitemCincoMenor, int imagenItemCincoMenor){
        this.NombreitemCincoMenor = nombreitemCincoMenor;
        this.ImagenItemCincoMenor = imagenItemCincoMenor;
    }

    public String getNombreitemCincoMenor() {
        return NombreitemCincoMenor;
    }

    public int getImagenItemCincoMenor() {
        return ImagenItemCincoMenor;
    }
}
