package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.SpinnerItemNotasMenores;

public class SpinnerItemDosMenorMenor {
    private String NombreItemDosMenor;
    private int ImagenItemDosMenor;

    public SpinnerItemDosMenorMenor (String nombreItemDosMenor, int imagenItemDosMenor){
        this.NombreItemDosMenor = nombreItemDosMenor;
        this.ImagenItemDosMenor = imagenItemDosMenor;
    }

    public String getNombreItemDosMenor() {
        return NombreItemDosMenor;
    }

    public int getImagenItemDosMenor() {
        return ImagenItemDosMenor;
    }
}
