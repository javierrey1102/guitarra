package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.SpinnerItemNotasMenores;

public class SpinnerItemCuatroMenorMenor {

    private String NombreItemCuatroMenor;
    private int ImagenItemCuatroMenor;

    public SpinnerItemCuatroMenorMenor (String nombreItemCuatroMenor, int imagenItemCuatroMenor){
        this.NombreItemCuatroMenor = nombreItemCuatroMenor;
        this.ImagenItemCuatroMenor = imagenItemCuatroMenor;
    }

    public String getNombreItemCuatroMenor() {
        return NombreItemCuatroMenor;
    }

    public int getImagenItemCuatroMenor() {
        return ImagenItemCuatroMenor;
    }
}
