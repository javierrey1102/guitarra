package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7;

public class SpinnerItenSeisMenor {
    //hacemos dos instancia de tipo string y entero
    private String NombreItemMenor;
    private int ImagenItemMenor;
    //Creamos el constructor y le pasamos dos parámetros (string e int) similar a los de arriba para no perder la referencia pero no son iguales.
    public SpinnerItenSeisMenor (String nombreItemMenor, int imagenItemMenor){
        this.NombreItemMenor = nombreItemMenor;
        this.ImagenItemMenor = imagenItemMenor;
    }
    //generamos el get de de las instancia del tipo "string" y me retorna el String
    public String getNombreItemSeisMenor() {
        return NombreItemMenor;
    }

    public int getImagenItemSeisMenor() {
        return ImagenItemMenor;
    }
}
