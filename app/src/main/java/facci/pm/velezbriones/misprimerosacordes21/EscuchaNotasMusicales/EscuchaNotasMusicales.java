package facci.pm.velezbriones.misprimerosacordes21.EscuchaNotasMusicales;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EscuchaNotasMusicales extends AppCompatActivity implements View.OnClickListener{
private LinearLayout notasMenor, notasMayor, notasMenores7, notasMayores7;
private TextView escuchaNotasMenores, escuchaNotasMayores, escuchaNotasMayores7, escuchaNotasMenores7;
private int escuchaNotasMenoresContador, escuchaNotasMayoresContador, escuhaNotasMayores7Contador, escuchaNotasMenore7Contador;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());


        setContentView(R.layout.activity_escucha_notas_musicales);
        notasMenor = (LinearLayout)findViewById(R.id.LLEscuchaNotasMenor);
        notasMayor = (LinearLayout)findViewById(R.id.LLEscuchaNotasMayor);
        notasMenores7 = (LinearLayout)findViewById(R.id.LLEscuchaNotasMenores7);
        notasMayores7 = (LinearLayout)findViewById(R.id.LLEscuchaNotasMayores7);

        notasMenor.setOnClickListener(this);
        notasMayor.setOnClickListener(this);
        notasMenores7.setOnClickListener(this);
        notasMayores7.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.LLEscuchaNotasMenor:
                startActivity(new Intent(EscuchaNotasMusicales.this, NotasMenor.class));
                break;
            case R.id.LLEscuchaNotasMayor:
                startActivity(new Intent(EscuchaNotasMusicales.this, NotasMayor.class));
                break;
                case R.id.LLEscuchaNotasMenores7:
                startActivity(new Intent(EscuchaNotasMusicales.this, NotasMenor7.class));
                break;
            case  R.id.LLEscuchaNotasMayores7:
                startActivity(new Intent(EscuchaNotasMusicales.this, NotasMayor7.class));
                break;
        }
    }

    private void LeerRecordNotasAdivinaMenor() {
        escuchaNotasMenores = (TextView) findViewById(R.id.TVNotasEscuchadasMenor);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        escuchaNotasMenoresContador = sharedPreferences.getInt("ESCUCHA_NOTAS_MENOR", 0);
        escuchaNotasMenores.setText(String.valueOf(escuchaNotasMenoresContador));
    }

    private void LeerRecordNotasAdivinaMayor() {
        escuchaNotasMayores = (TextView) findViewById(R.id.TVNotasEscuchadasMayor);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        escuchaNotasMayoresContador = sharedPreferences.getInt("ESCUCHA_NOTAS_MAYOR", 0);
        escuchaNotasMayores.setText(String.valueOf(escuchaNotasMayoresContador));
    }

    private void LeerRecordNotasAdivinaMayor7() {
        escuchaNotasMayores7 = (TextView) findViewById(R.id.TVNotasEscuchadasMayor7);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        escuhaNotasMayores7Contador = sharedPreferences.getInt("ESCUCHA_NOTAS_MAYOR_7", 0);
        escuchaNotasMayores7.setText(String.valueOf(escuhaNotasMayores7Contador));
    }


    private void LeerRecordNotasAdivinaMenor7() {
        escuchaNotasMenores7 = (TextView) findViewById(R.id.TVNotasEscuchadasMenor7);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        escuchaNotasMenore7Contador = sharedPreferences.getInt("ESCUCHA_NOTAS_MENOR_7", 0);
        escuchaNotasMenores7.setText(String.valueOf(escuchaNotasMenore7Contador));
    }

    private void GuardarRecordTodasLasNotasEscuchadas (){
     SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);
     SharedPreferences.Editor editor = sharedPreferences.edit();
     int total = escuchaNotasMenoresContador + escuchaNotasMayoresContador +
             escuchaNotasMenore7Contador + escuhaNotasMayores7Contador;
     editor.putInt("TOTAL_NOTAS_ESCUCHADAS", total);
     editor.apply();
    }


    @Override
    protected void onResume() {
        super.onResume();
        LeerRecordNotasAdivinaMayor();
        LeerRecordNotasAdivinaMayor7();
        LeerRecordNotasAdivinaMenor7();
        LeerRecordNotasAdivinaMenor();

        GuardarRecordTodasLasNotasEscuchadas();

    }
}
