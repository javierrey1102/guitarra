package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores;

public class SpinnerItemSeisMayorMayor {
    private String NombreItemSeisMayor;
    private int ImagenItemSeisMayor;

    public SpinnerItemSeisMayorMayor (String nombreItemSeisMayor, int imagenItemSeisMayor){
        this.NombreItemSeisMayor = nombreItemSeisMayor;
        this.ImagenItemSeisMayor = imagenItemSeisMayor;
    }

    public String getNombreItemSeisMayor() {
        return NombreItemSeisMayor;
    }

    public int getImagenItemSeisMayor() {
        return ImagenItemSeisMayor;
    }
}
