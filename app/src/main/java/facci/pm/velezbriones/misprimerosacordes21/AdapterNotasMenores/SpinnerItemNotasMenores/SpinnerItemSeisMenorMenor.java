package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.SpinnerItemNotasMenores;

public class SpinnerItemSeisMenorMenor {
    private String NombreItemSeisMenor;
    private int ImagenItemSeisMenor;

    public SpinnerItemSeisMenorMenor (String nombreItemSeisMenor, int imagenItemSeisMenor){
        this.NombreItemSeisMenor = nombreItemSeisMenor;
        this.ImagenItemSeisMenor = imagenItemSeisMenor;
    }

    public String getNombreItemSeisMenor() {
        return NombreItemSeisMenor;
    }

    public int getImagenItemSeisMenor() {
        return ImagenItemSeisMenor;
    }
}
