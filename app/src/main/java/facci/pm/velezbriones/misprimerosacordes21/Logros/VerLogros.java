package facci.pm.velezbriones.misprimerosacordes21.Logros;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class VerLogros extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout escribeLaNota, adivinaLaNota;


    //cambiar
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //de aqui
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        //hasta aqui
        setContentView(R.layout.activity_ver_logros);

    escribeLaNota = (LinearLayout)findViewById(R.id.LLLEscribeLaNota);
    adivinaLaNota = (LinearLayout)findViewById(R.id.LLAdivinaLaNota);


    escribeLaNota.setOnClickListener(this);
    adivinaLaNota.setOnClickListener(this);

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.LLLEscribeLaNota:
                startActivity(new Intent(this, LogrosEscribeLaNota.class));
                break;
            case R.id.LLAdivinaLaNota:
                startActivity(new Intent(this, LogrosAdivinaLaNota.class));
                break;
        }
    }
}
