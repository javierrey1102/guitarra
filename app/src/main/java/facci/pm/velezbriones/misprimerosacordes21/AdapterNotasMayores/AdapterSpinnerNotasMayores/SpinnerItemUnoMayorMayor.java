package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores;

public class SpinnerItemUnoMayorMayor {
    private String NombreItemUnoMayor;
    private int ImagenItemUnoMayor;

    public SpinnerItemUnoMayorMayor (String nombreItemUnoMayor, int imagenItemUnoMayor){
        this.NombreItemUnoMayor = nombreItemUnoMayor;
        this.ImagenItemUnoMayor = imagenItemUnoMayor;
    }

    public String getNombreItemUnoMayor() {
        return NombreItemUnoMayor;
    }

    public int getImagenItemUnoMayor() {
        return ImagenItemUnoMayor;
    }
}
