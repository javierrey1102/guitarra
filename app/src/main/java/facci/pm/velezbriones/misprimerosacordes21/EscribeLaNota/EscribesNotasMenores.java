package facci.pm.velezbriones.misprimerosacordes21.EscribeLaNota;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.UUID;

import facci.pm.velezbriones.misprimerosacordes21.Clases.Sesion;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosEscribeNotasMenores;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.Usuario;
import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EscribesNotasMenores extends AppCompatActivity implements View.OnClickListener {
    // Instanciamos cada objeto a la clase privada ImageView, EditText, Button y MediaPlayer.
    private ImageView sonidoUno, sonidoDos, sonidoTres, sonidoCuatro, sonidoCinco, sonidoSeis;
    private EditText respuestaUno, respuestaDos, respuestaTres, respuestaCuatro, respuestaCinco, respuestaSeis;
    private LinearLayout comprobarUno, comprobarDos, comprobarTres, comprobarCuatro, comprobarCinco, comprobarSeis;
    private LinearLayout correctoUnoMenor, correctoDosMenor, correctoTresMenor, correctoCuatroMenor, correctoCincoMenor, correctoSeisMenor;
    private MediaPlayer MPsonidouno, MPsonidodos, MPsonidotres, Mpsonidocuatro, MPsonidocinco, MPsonidoseis;


    /*Creamos instancias de tipo String y las inicializamos en blanco*/
    private String domenor = "";
    private String remenor = "";
    private String mimenor = "";
    private String solmenor = "";
    private String lamenor = "";
    private String simenor = "";
    private String nombreMenor = "";
    int contador;



    //Todo lo que tenga que ver con firebase :v
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference logros_Notas_Menores, logros_Notas_MenoresP;
    private FirebaseAuth.AuthStateListener authStateListener;
    private String userId;

    //local data
    private Sesion sesion;
    private HashMap<String, String> user;




    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());


        setContentView(R.layout.activity_escribes_notas_menores);

        sesion = new Sesion(this);
        user = sesion.LoggedInUser();



        // A cada referecia de ImageView lo enlazamos con su respectivo id.
        sonidoUno = (ImageView)findViewById(R.id.Sonido);
        sonidoDos = (ImageView)findViewById(R.id.SonidoDos);
        sonidoTres = (ImageView)findViewById(R.id.SonidoTres);
        sonidoCuatro = (ImageView)findViewById(R.id.SonidoCuatro);
        sonidoCinco = (ImageView)findViewById(R.id.SonidoCinco);
        sonidoSeis = (ImageView)findViewById(R.id.SonidoSeis);

        //  A cada referencia de MediaPlayer lo enlazamos con su respectivo sonido
        MPsonidouno = MediaPlayer.create(this, R.raw.domenor);
        MPsonidodos = MediaPlayer.create(this, R.raw.remenor);
        MPsonidotres = MediaPlayer.create(this, R.raw.mimenor);
        Mpsonidocuatro = MediaPlayer.create(this, R.raw.solmenor);
        MPsonidocinco = MediaPlayer.create(this, R.raw.lamenor);
        MPsonidoseis = MediaPlayer.create(this, R.raw.simenor);

        // A cada referencia de los EditText lo enlazamos con su id.
        respuestaUno = (EditText)findViewById(R.id.TXTRespuesta);
        respuestaDos = (EditText)findViewById(R.id.TXTRespuestaDos);
        respuestaTres = (EditText)findViewById(R.id.TXTRespuestaTres);
        respuestaCuatro = (EditText)findViewById(R.id.TXTRespuestaCuatro);
        respuestaCinco = (EditText)findViewById(R.id.TXTRespuestaCinco);
        respuestaSeis = (EditText)findViewById(R.id.TXTRespuestaSeis);



        // A cada referencia de los botones lo enlazamos con su id.
        comprobarUno = (LinearLayout) findViewById(R.id.BTNComprobarMenorUno);
        comprobarDos = (LinearLayout) findViewById(R.id.BTNComprobarMenorDos);
        comprobarTres = (LinearLayout) findViewById(R.id.BTNComprobarMenorTres);
        comprobarCuatro = (LinearLayout) findViewById(R.id.BTNComprobarMenorCuatro);
        comprobarCinco = (LinearLayout) findViewById(R.id.BTNComprobarMenorCinco);
        comprobarSeis = (LinearLayout) findViewById(R.id.BTNComprobarMenorSeis);

        correctoUnoMenor = (LinearLayout)findViewById(R.id.LLUnoVerde);
        correctoDosMenor = (LinearLayout)findViewById(R.id.LLDosVerde);
        correctoTresMenor = (LinearLayout)findViewById(R.id.LLTresVerde);
        correctoCuatroMenor = (LinearLayout)findViewById(R.id.LLCuatroVerde);
        correctoCincoMenor = (LinearLayout)findViewById(R.id.LLCincoVerde);
        correctoSeisMenor = (LinearLayout)findViewById(R.id.LLSeisVerde);

        //Dentro de este método OnCreate asociamos el lístener a los botones.
        sonidoUno.setOnClickListener(this);
        sonidoDos.setOnClickListener(this);
        sonidoTres.setOnClickListener(this);
        sonidoCuatro.setOnClickListener(this);
        sonidoCinco.setOnClickListener(this);
        sonidoSeis.setOnClickListener(this);

        //Al método OnCreate le asociamos el lístener a los botones.
        comprobarUno.setOnClickListener(this);
        comprobarDos.setOnClickListener(this);
        comprobarTres.setOnClickListener(this);
        comprobarCuatro.setOnClickListener(this);
        comprobarCinco.setOnClickListener(this);
        comprobarSeis.setOnClickListener(this);

            // firebase

            /* Obtenemos la instancia para acceder a la base de datos "firebase"*/
            firebaseDatabase = FirebaseDatabase.getInstance();

            /* Obtenemos instancia para autenticar el usuario*/
            firebaseAuth = FirebaseAuth.getInstance();

            /* Obtenemos instancia y referencias del almacenamiento de firebase */
            /* Obetenemos de la base de datos el usuario actual. */

            /* Le damos el nombre a la tabla que guardará los datos en Firebase*/
            logros_Notas_Menores = firebaseDatabase.getReference("ESCRIBE_LOGROS_MENORES");


            logros_Notas_MenoresP = firebaseDatabase.getReference("ESCRIBE_LOGROS_MENORES_P");




    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){


            /* Se reproduce el sonido del primer botón */
            case R.id.Sonido:
                MPsonidouno.start();
                break;

            case R.id.SonidoDos:
                /* Se reproduce el sonido del segundo botón */
                MPsonidodos.start();
                break;

            case R.id.SonidoTres:
                /* Se reproduce el sonido del tercer botón */
                MPsonidotres.start();
                break;

            case R.id.SonidoCuatro:
                /* Se reproduce el sonido del cuarto botón */
                Mpsonidocuatro.start();
                break;

            case R.id.SonidoCinco:
                /* Se reproduce el sonido del quinto botón */
                MPsonidocinco.start();
                break;

            case R.id.SonidoSeis:
                /* Se reproduce el sonido del sexto botón */
                MPsonidoseis.start();
                break;

            case R.id.BTNComprobarMenorUno:
                /* Cuando comprobamos y lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaUno.getText().toString().isEmpty()) {
                    respuestaUno.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaUno.getText().toString().equals(getString(R.string.domenor)+" ") ||
                        respuestaUno.getText().toString().equals(getString(R.string.domenor)) ||
                        respuestaUno.getText().toString().equals(getString(R.string.domenorLPMe)+ " ") ||
                        respuestaUno.getText().toString().equals(getString(R.string.domenorLPMe))||
                        respuestaUno.getText().toString().equals(getString(R.string.DdomenorLPMe)+ " ") ||
                        respuestaUno.getText().toString().equals(getString(R.string.DdomenorLPMe))){


                    if (domenor==""){
                        domenor = "CORRECTO: "+respuestaUno.getText().toString();
                    }else {
                        domenor +=  " - " + "CORRECTO: "+respuestaUno.getText().toString();
                    }
                    comprobarUno.setEnabled(false);
                    comprobarUno.setBackgroundResource(R.color.botoncolor);
                    correctoUnoMenor.setBackgroundResource(R.color.botoncolor);
                    sonidoUno.setImageResource(R.drawable.correct_note);
                    respuestaUno.setText("CORRECTO");
                    respuestaUno.setEnabled(false);
                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (domenor==""){
                        domenor = "INCORRECTO: "+ respuestaUno.getText().toString();
                    }else {
                        domenor +=  " - " + "INCORRECTO: "+respuestaUno.getText().toString();
                    }
                    InCorrecto();
                    respuestaUno.setText("");
                }
                break;


            case R.id.BTNComprobarMenorDos:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaDos.getText().toString().isEmpty()) {
                    respuestaDos.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaDos.getText().toString().equals(getString(R.string.remenor)+" ")  ||
                        respuestaDos.getText().toString().equals(getString(R.string.remenor)) ||
                        respuestaDos.getText().toString().equals(getString(R.string.remenorLPMe)+ " ") ||
                        respuestaDos.getText().toString().equals(getString(R.string.remenorLPMe))||
                        respuestaDos.getText().toString().equals(getString(R.string.RremenorLPMe)+ " ") ||
                        respuestaDos.getText().toString().equals(getString(R.string.RremenorLPMe))){

                    if (remenor==""){
                        remenor= "CORECTO: "+respuestaDos.getText().toString();
                    }else {
                        remenor+= " - "+ "CORRECTO: " +respuestaDos.getText().toString();
                    }

                    correctoDosMenor.setBackgroundResource(R.color.botoncolor);
                    respuestaDos.setText("CORRECTO");
                    respuestaDos.setEnabled(false);
                    comprobarDos.setBackgroundResource(R.color.botoncolor);
                    sonidoDos.setImageResource(R.drawable.correct_note);
                    comprobarDos.setEnabled(false);
                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (remenor==""){
                        remenor= "INCORECTO: "+respuestaDos.getText().toString();
                    }else {
                        remenor+= " - "+ "INCORRECTO: " +respuestaDos.getText().toString();
                    }

                    InCorrecto();
                    respuestaDos.setText("");
                }
                break;

            case R.id.BTNComprobarMenorTres:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaTres.getText().toString().isEmpty()) {
                    respuestaTres.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaTres.getText().toString().equals(getString(R.string.mimenor)+" ")||
                        respuestaTres.getText().toString().equals(getString(R.string.mimenor)) ||
                        respuestaTres.getText().toString().equals(getString(R.string.mimenorLPMe)+ " ") ||
                        respuestaTres.getText().toString().equals(getString(R.string.mimenorLPMe))||
                        respuestaTres.getText().toString().equals(getString(R.string.MmimenorLPMe)+ " ") ||
                        respuestaTres.getText().toString().equals(getString(R.string.MmimenorLPMe))){

                    if (mimenor==""){
                        mimenor= "CORECTO: "+respuestaTres.getText().toString();
                    }else {
                        mimenor+= " - "+ "CORRECTO: " +respuestaTres.getText().toString();
                    }

                    correctoTresMenor.setBackgroundResource(R.color.botoncolor);
                    sonidoTres.setImageResource(R.drawable.correct_note);
                    respuestaTres.setText("CORRECTO");
                    respuestaTres.setEnabled(false);
                    comprobarTres.setEnabled(false);
                    comprobarTres.setBackgroundResource(R.color.botoncolor);
                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */

                    if (mimenor==""){
                        mimenor= "INCORECTO: "+respuestaTres.getText().toString();
                    }else {
                        mimenor+= " - "+ "INCORRECTO: " +respuestaTres.getText().toString();
                    }

                    InCorrecto();
                    respuestaTres.setText("");
                }
                break;

            case R.id.BTNComprobarMenorCuatro:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaCuatro.getText().toString().isEmpty()) {
                    respuestaCuatro.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaCuatro.getText().toString().equals(getString(R.string.solmenor)+" ")||
                        respuestaCuatro.getText().toString().equals(getString(R.string.solmenor)) ||
                        respuestaCuatro.getText().toString().equals(getString(R.string.solmenorLPMe)+ " ") ||
                        respuestaCuatro.getText().toString().equals(getString(R.string.solmenorLPMe))||
                        respuestaCuatro.getText().toString().equals(getString(R.string.SsolmenorLPMe)+ " ") ||
                        respuestaCuatro.getText().toString().equals(getString(R.string.SsolmenorLPMe))){


             if (solmenor==""){
                        solmenor= "CORECTO: "+respuestaCuatro.getText().toString();
                    }else {
                        solmenor+= " - "+ "CORRECTO: " +respuestaCuatro.getText().toString();
                    }

                    correctoCuatroMenor.setBackgroundResource(R.color.botoncolor);
                    sonidoCuatro.setImageResource(R.drawable.correct_note);
                    respuestaCuatro.setText("CORRECTO");
                    respuestaCuatro.setEnabled(false);
                    comprobarCuatro.setEnabled(false);
                    comprobarCuatro.setBackgroundResource(R.color.botoncolor);
                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (solmenor==""){
                        solmenor= "INCORECTO: "+respuestaCuatro.getText().toString();
                    }else {
                        solmenor+= " - "+ "INCORRECTO: " +respuestaCuatro.getText().toString();
                    }


                    InCorrecto();
                    respuestaCuatro.setText("");
                }
                break;

            case R.id.BTNComprobarMenorCinco:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaCinco.getText().toString().isEmpty()) {
                    respuestaCinco.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaCinco.getText().toString().equals(getString(R.string.lamenor)+" ") ||
                        respuestaCinco.getText().toString().equals(getString(R.string.lamenor)) ||
                        respuestaCinco.getText().toString().equals(getString(R.string.lamenorLPMe)+ " ") ||
                        respuestaCinco.getText().toString().equals(getString(R.string.lamenorLPMe))||
                        respuestaCinco.getText().toString().equals(getString(R.string.LlamenorLPMe)+ " ") ||
                        respuestaCinco.getText().toString().equals(getString(R.string.LlamenorLPMe))){


                    if (lamenor==""){
                        lamenor= "CORECTO: "+respuestaCinco.getText().toString();
                    }else {
                        lamenor+= " - "+ "CORRECTO: " +respuestaCinco.getText().toString();
                    }

                    correctoCincoMenor.setBackgroundResource(R.color.botoncolor);
                    sonidoCinco.setImageResource(R.drawable.correct_note);
                    respuestaCinco.setText("CORRECTO");
                    respuestaCinco.setEnabled(false);
                    comprobarCinco.setEnabled(false);
                    comprobarCinco.setBackgroundResource(R.color.botoncolor);

                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (lamenor==""){
                        lamenor= "INCORECTO: "+respuestaCinco.getText().toString();
                    }else {
                        lamenor+= " - "+ "INCORRECTO: " +respuestaCinco.getText().toString();
                    }

                    InCorrecto();
                    respuestaCinco.setText("");
                }
                break;

            case R.id.BTNComprobarMenorSeis:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaSeis.getText().toString().isEmpty()) {
                    respuestaSeis.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaSeis.getText().toString().equals(getString(R.string.simenor)+" ") ||
                        respuestaSeis.getText().toString().equals(getString(R.string.simenor)) ||
                        respuestaSeis.getText().toString().equals(getString(R.string.simenorLPMe)+ " ") ||
                        respuestaSeis.getText().toString().equals(getString(R.string.simenorLPMe))||
                        respuestaSeis.getText().toString().equals(getString(R.string.SsimenorLPMe)+ " ") ||
                        respuestaSeis.getText().toString().equals(getString(R.string.SsimenorLPMe))){


                    if (simenor==""){
                        simenor= "CORECTO: "+respuestaSeis.getText().toString();
                    }else {
                        simenor+= " - "+ "CORRECTO: " +respuestaSeis.getText().toString();
                    }
                    correctoSeisMenor.setBackgroundResource(R.color.botoncolor);
                    sonidoSeis.setImageResource(R.drawable.correct_note);
                    respuestaSeis.setText("CORRECTO");
                    respuestaSeis.setEnabled(false);
                    comprobarSeis.setEnabled(false);
                    comprobarSeis.setBackgroundResource(R.color.botoncolor);

                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (simenor==""){
                        simenor= "INCORECTO: "+respuestaSeis.getText().toString();
                    }else {
                        simenor+= " - "+ "INCORRECTO: " +respuestaSeis.getText().toString();
                    }
                    InCorrecto();
                    respuestaSeis.setText("");
                }
                break;

        }

    }




    /* Método ""InCorreto para mostrar que está incorrecto, es llama cada vez que no se cumplen las condiciones en los botones*/
    private void InCorrecto(){

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.incorrecto));
        imageView.setImageResource(R.drawable.ic_marca_de_cancelar);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
        contador++;
    }

    /* Método "Correto" para mostrar que está correcto, es llama cada vez que se cumplen las condiciones en los botones*/
    private void Correcto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.correcto));
        imageView.setImageResource(R.drawable.ic_correcto_simbolo);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }



    private void GuardarRecord() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("RECORD", contador);
        editor.apply();
    }

    private void GuardarCorrectoMenor() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if ((respuestaUno.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaDos.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaTres.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaCuatro.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaCinco.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaSeis.getText().toString().equals(getString(R.string.correcto)))){
            editor.putString("RECORD_MENOR", "SI");
        }else {
            editor.putString("RECORD_MENOR", "NO");
        }
        editor.apply();
    }



    /*Método para bloquear el botón BACK*/
    @Override
    public void onBackPressed(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View guardar = layoutInflater.inflate(R.layout.guardar_notas_menores, null);
        final CardView Guardar = (CardView)guardar.findViewById(R.id.CardGuardar);
        final CardView Cancelar = (CardView)guardar.findViewById(R.id.CardCancelar);

        Guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarRecord();
                GuardarCorrectoMenor();

                LogrosEscribeNotasMenores LENMenores = new LogrosEscribeNotasMenores();
                LENMenores.setEDoMenor(domenor);
                LENMenores.setEReMenor(remenor);
                LENMenores.setEMiMenor(mimenor);
                LENMenores.setESolMenor(solmenor);
                LENMenores.setELaMenor(lamenor);
                LENMenores.setESiMenor(simenor);
                LENMenores.setENombreMenor(user.get(sesion.USER_NAME));
                LENMenores.setENombreMenorImg(user.get(sesion.USER_ICONO));

                if (isOnlineNet()==true) {
                    GuardarPersonal(LENMenores);
                }else
                    Toast.makeText(EscribesNotasMenores.this, R.string.sinaccesoainternet, Toast.LENGTH_SHORT).show();

            }
        });

        Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        builder.setView(guardar);
        builder.show();

    }

    private void GuardarPersonal(final LogrosEscribeNotasMenores logrosEscribeNotasMenores){

        final String id = user.get(sesion.USER_ID);
        logros_Notas_Menores.child(id).child("ESCRIBE_LOGROS_MENORES - " + id).child(UUID.randomUUID().toString()).setValue(logrosEscribeNotasMenores).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                GuardarPublico(logrosEscribeNotasMenores);
            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        });

    }

    private void GuardarPublico(LogrosEscribeNotasMenores l){
        final String id = user.get(sesion.USER_ID);
        logros_Notas_MenoresP.child(id).setValue(l).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                startActivity(new Intent(EscribesNotasMenores.this, EscribeLaNota.class));
                Toast.makeText(EscribesNotasMenores.this, "Datos guardados con éxito", Toast.LENGTH_SHORT).show();
                finish();
            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        });
    }

    //metodo de tipo booleano para comprobar la conexion a internet
    public Boolean isOnlineNet() {
        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");
            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
}
