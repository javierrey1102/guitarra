package facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItemTresMayor7;
import facci.pm.velezbriones.misprimerosacordes21.R;

public class AdapterSpinnerTresMayor extends ArrayAdapter<SpinnerItemTresMayor7> {
    public AdapterSpinnerTresMayor (Context context, ArrayList<SpinnerItemTresMayor7> spinnerItemTresMayor7s){
        super(context, 0, spinnerItemTresMayor7s);
    }

    @NonNull
    //se crea por defecto, no se modifica el código
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    //se crea por defecto, no se modifica el código
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    // también se crea por defecto pero le hacemos algunos ajustes
    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            // como la vista no tiene nada entonces le pasamos el XML
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_uno, parent, false
            );
        }
        // Inicializamos todos los campos que tiene el layout-XML  y a esos campos hay que convertirlos en (convertView)
        ImageView imageView = (ImageView)convertView.findViewById(R.id.IMGSPUnoMenor);
        TextView textView = (TextView)convertView.findViewById(R.id.LBLSPUnomenor);

        SpinnerItemTresMayor7 spinnerItemTresMayor7 = getItem(position);

        if (spinnerItemTresMayor7 != null){
        imageView.setImageResource(spinnerItemTresMayor7.getImagenItemMayor7());
        textView.setText(spinnerItemTresMayor7.getNombreItemMayor7());
        }
    return convertView;
    }

}
