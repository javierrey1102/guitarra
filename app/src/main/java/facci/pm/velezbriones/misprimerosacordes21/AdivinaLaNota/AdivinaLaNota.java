package facci.pm.velezbriones.misprimerosacordes21.AdivinaLaNota;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import facci.pm.velezbriones.misprimerosacordes21.EscribeLaNota.EscribeLaNota;
import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AdivinaLaNota extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout adivinaNotasMayor, adivinaNotasMenor, adivinaNotasMayor7, adivinaNotasMenor7;
    private TextView erroresMenores7, erroresMayores7, erroresMenores, erroresMayores;
    private int recordAdivinaMenores7, recordAdivinaMayores7, recordAdivinaMenores, recordAdivinaMayores;

    private TextView erroresNotasMenores7, erroresNotasMayores7, erroresNotasMenores, erroresNotasMayores;
    private String recordCorrectoNotasMenoresCompletadoAdivina7, recordCorrectoNotasMayoresCompletadoAdivina7,
            recordCorrectoNotasMenoresCompletadoAdivina, recordCorrectoNotasMayoresCompletadoAdivina;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());


        setContentView(R.layout.activity_adivina_la_nota);

        adivinaNotasMayor = (LinearLayout)findViewById(R.id.LLAdivinaNotasMayor);
        adivinaNotasMenor = (LinearLayout)findViewById(R.id.LLAdivinaNotasMenor);
        adivinaNotasMenor7 = (LinearLayout)findViewById(R.id.LLAdivinaNotasMenores7);
        adivinaNotasMayor7 =  (LinearLayout)findViewById(R.id.LLAdivinaNotasMayores7);


        adivinaNotasMenor.setOnClickListener(this);
        adivinaNotasMayor.setOnClickListener(this);
        adivinaNotasMenor7.setOnClickListener(this);
        adivinaNotasMayor7.setOnClickListener(this);


    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.LLAdivinaNotasMayor:
                startActivity(new Intent(AdivinaLaNota.this, AdivinaNotasMayor.class));
                break;

            case R.id.LLAdivinaNotasMenor:
                startActivity(new Intent(AdivinaLaNota.this, AdivinaNotasMenor.class));
                break;

            case R.id.LLAdivinaNotasMenores7:
                startActivity(new Intent(AdivinaLaNota.this, AdivinaNotasMenores7.class));
                break;

            case R.id.LLAdivinaNotasMayores7:
                startActivity(new Intent(AdivinaLaNota.this, AdivinaNotasMayores7.class));
                break;
        }
    }

    private void GuardarRecordNotasAdivinaMenor7() {
        erroresMenores7 = (TextView) findViewById(R.id.TVNotasMenor7ErroresAdivina);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        recordAdivinaMenores7 = sharedPreferences.getInt("RECORD_NOTAS_ADIVINA_MENORES_7", 0);
        erroresMenores7.setText(String.valueOf(recordAdivinaMenores7));
    }
    private void GuardarRecordNotasAdivinaMayor7() {
        erroresMayores7 = (TextView) findViewById(R.id.TVNotasMenores7ErroresAdivina);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        recordAdivinaMayores7 = sharedPreferences.getInt("RECORD_NOTAS_ADIVINA_MAYORES_7", 0);
        erroresMayores7.setText(String.valueOf(recordAdivinaMayores7));
    }
    private void GuardarRecordNotasAdivinaMenor() {
        erroresMenores = (TextView) findViewById(R.id.TVNotasMenorErroresAdivina);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        recordAdivinaMenores = sharedPreferences.getInt("RECORD_NOTAS_ADIVINA_MENOR", 0);
        erroresMenores.setText(String.valueOf(recordAdivinaMenores));
    }
    private void GuardarRecordNotasAdivinaMayor() {
        erroresMayores = (TextView) findViewById(R.id.TVNotasMayoresErroresAdivina);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        recordAdivinaMayores = sharedPreferences.getInt("RECORD_NOTAS_ADIVINA_MAYORES_MAYORES", 0);
        erroresMayores.setText(String.valueOf(recordAdivinaMayores));
    }



    private void GuardarRecordNotasAdivinaMenor7Completado() {
        erroresNotasMenores7 = (TextView) findViewById(R.id.TVNotasMenor7CompletadoAdivina);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        recordCorrectoNotasMenoresCompletadoAdivina7 = sharedPreferences.getString("GUARDAR_RECORD_ADIVINA_MENOR_7", "NO");
        erroresNotasMenores7.setText(String.valueOf(recordCorrectoNotasMenoresCompletadoAdivina7));
    }

    private void GuardarRecordNotasAdivinaMayor7Completado() {
        erroresNotasMayores7 = (TextView) findViewById(R.id.TVNotasMayores7CompletadoAdivina);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        recordCorrectoNotasMayoresCompletadoAdivina7 = sharedPreferences.getString("GUARDAR_RECORD_ADIVINA_MAYOR_7", "NO");
        erroresNotasMayores7.setText(String.valueOf(recordCorrectoNotasMayoresCompletadoAdivina7));
    }

    private void GuardarRecordNotasAdivinaMenorCompletado() {
        erroresNotasMenores = (TextView) findViewById(R.id.TVNotasMenorCompletadoAdivina);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        recordCorrectoNotasMenoresCompletadoAdivina = sharedPreferences.getString("GUARDAR_RECORD_ADIVINA_MENOR", "NO");
        erroresNotasMenores.setText(String.valueOf(recordCorrectoNotasMenoresCompletadoAdivina));
    }

    private void GuardarRecordNotasAdivinaMayorCompletado() {
        erroresNotasMayores = (TextView) findViewById(R.id.TVNotasMayorCompletadoAdivina);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        recordCorrectoNotasMayoresCompletadoAdivina = sharedPreferences.getString("GUARDAR_RECORD_ADIVINA_MAYOR", "NO");
        erroresNotasMayores.setText(String.valueOf(recordCorrectoNotasMayoresCompletadoAdivina));
    }



    @Override
    protected void onResume() {
        super.onResume();
        GuardarRecordNotasAdivinaMenor7();
        GuardarRecordNotasAdivinaMayor7();
        GuardarRecordNotasAdivinaMenor();
        GuardarRecordNotasAdivinaMayor();
        GuardarRecordNotasAdivinaMenor7Completado();
        GuardarRecordNotasAdivinaMayor7Completado();
        GuardarRecordNotasAdivinaMenorCompletado();
        GuardarRecordNotasAdivinaMayorCompletado();
    }

}
