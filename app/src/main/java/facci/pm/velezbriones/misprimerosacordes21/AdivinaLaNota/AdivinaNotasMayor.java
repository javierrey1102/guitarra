package facci.pm.velezbriones.misprimerosacordes21.AdivinaLaNota;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;

import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.AdapterSpinnerCincoMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.AdapterSpinnerCuatroMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.AdapterSpinnerDosMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.AdapterSpinnerSeisMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.AdapterSpinnerTresMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.AdapterSpinnerUnoMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.AdaterSpinnerSieteMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItemCincoMayor7;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItemCuatroMayor7;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItemDosMayor7;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItemSieteMayor7;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItemTresMayor7;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItemUnoMayor7;
import facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7.SpinnerItenSeisMayor7;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSPinnerDosMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerCincoMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerCuatroMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores.SpinnerItemCincoMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores.SpinnerItemCuatroMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores.SpinnerItemDosMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores.SpinnerItemSeisMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores.SpinnerItemSieteMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores.SpinnerItemTresMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores.SpinnerItemUnoMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerSeisMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerSieteMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerTresMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerUnoMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.Clases.Sesion;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosAdivinaNotasMayores;
import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AdivinaNotasMayor extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout LLunomayorMayor, LLdosmayorMayor, LLtresmayorMayor, LLcuatromayorMayor, LLcincomayorMayor, LLseismayorMayor, LLsietemayorMayor;
    /*Creamos instancias de tipo String y las inicializamos en blanco*/
    private String SPdomayorMayor = "";
    private String SPremayorMayor = "";
    private String SPmimayorMayor = "";
    private String SPsolmayorMayor = "";
    private String SPlamayorMayor = "";
    private String SPsimayorMayor = "";
    private String SPfamayorMayor = "";
    private int contadorAdivinaNotasMayoresM, contadorPruebaAdivinaNotasMayores;


    //Todo lo que tenga que ver con firebase :v
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference logros_Adivina_Notas_Mayores, users;
    private FirebaseAuth.AuthStateListener authStateListener;
    private String userId;

    //local data
    private Sesion sesion;
    private HashMap<String, String> user;

    /* Necesitamos una lista que deseamos que muestre el spinner. Agarramos los métodos de la clase creada (SpinnerItenUnoMayor)
también llamamos a esta clase los métodos de la clase AdapterSpinnerUnoMayor para poder implementarlos.
Asi mismo con los demás...
*/
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItemUnoMayorMayor> spinnerItemUnoMayorMayor;
    private AdapterSpinnerUnoMayorMayor adapterSpinnerUnoMayorMayor;

    private ArrayList<SpinnerItemDosMayorMayor> spinnerItemDosMayorMayor;
    private AdapterSPinnerDosMayorMayor adapterSPinnerDosMayorMayor;

    private ArrayList<SpinnerItemTresMayorMayor> spinnerItemTresMayorMayor;
    private AdapterSpinnerTresMayorMayor adapterSpinnerTresMayorMayor;

    private ArrayList<SpinnerItemCuatroMayorMayor> spinnerItemCuatroMayorMayor;
    private AdapterSpinnerCuatroMayorMayor adapterSpinnerCuatroMayorMayor;

    private ArrayList<SpinnerItemCincoMayorMayor> spinnerItemCincoMayorMayor;
    private AdapterSpinnerCincoMayorMayor adapterSpinnerCincoMayorMayor;

    private ArrayList<SpinnerItemSeisMayorMayor> spinnerItemSeisMayorMayor;
    private AdapterSpinnerSeisMayorMayor adapterSpinnerSeisMayorMayor;

    private ArrayList<SpinnerItemSieteMayorMayor> spinnerItemSieteMayorMayor;
    private AdapterSpinnerSieteMayorMayor adapterSpinnerSieteMayorMayor;

    /* Instanciamos cada objeto de la clase privada Spinner */
    private Spinner spinnerUnoMayorMayor, spinnerDosMayorMayor, spinnerTresMayorMayor, spinnerCuatroMayorMayor,
            spinnerCincoMayorMayor, spinnerSeisMayorMayor, spinnerSieteMayorMayor;

    /* Instanciamos cada objeto de la clase privada Imageview y MediaPlayer */
    private ImageView unoMayorMayor, dosMayorMayor, tresMayorMayor, cuatroMayorMayor, cincoMayorMayor, seisMayorMayor, sieteMayorMayor;
    private MediaPlayer MPunoMayorMayor, MPdosMayorMayor, MPtresMayorMayor, MPcuatroMayorMayor, MPcincoMayorMayor, MPseisMayorMayor, MPsieteMayorMayor;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        setContentView(R.layout.activity_adivina_notas_mayor);


        sesion = new Sesion(this);
        user = sesion.LoggedInUser();


        // A cada referencia de LinearLayout le asociamos con su id
        LLunomayorMayor = (LinearLayout) findViewById(R.id.LLUnoMayorMayor);
        LLdosmayorMayor = (LinearLayout) findViewById(R.id.LLDosMayorMayor);
        LLtresmayorMayor = (LinearLayout) findViewById(R.id.LLTresMayorMayor);
        LLcuatromayorMayor = (LinearLayout) findViewById(R.id.LLCuatroMayorMayor);
        LLcincomayorMayor = (LinearLayout) findViewById(R.id.LLCincoMayorMayor);
        LLseismayorMayor = (LinearLayout) findViewById(R.id.LLSeisMayorMayor);
        LLsietemayorMayor = (LinearLayout) findViewById(R.id.LLSieteMayorMayor);



        /* A cada referencia de MediaPlayer la enlazamos con su id */
        MPunoMayorMayor = MediaPlayer.create(this, R.raw.domayor);
        MPdosMayorMayor = MediaPlayer.create(this, R.raw.remayor);
        MPtresMayorMayor = MediaPlayer.create(this, R.raw.mimayor);
        MPcuatroMayorMayor = MediaPlayer.create(this, R.raw.famayor);
        MPcincoMayorMayor = MediaPlayer.create(this, R.raw.solmayor);
        MPseisMayorMayor = MediaPlayer.create(this, R.raw.lamayor);
        MPsieteMayorMayor = MediaPlayer.create(this, R.raw.simayor);

        /* A cada referencia de las ImageView las enlazamos con su id */
        unoMayorMayor = (ImageView) findViewById(R.id.SPImagenUnoMayorMayor);
        dosMayorMayor = (ImageView) findViewById(R.id.SPImagenDosMayorMayor);
        tresMayorMayor = (ImageView) findViewById(R.id.SPImagenTresMayorMayor);
        cuatroMayorMayor = (ImageView) findViewById(R.id.SPImagenCuatroMayorMayor);
        cincoMayorMayor = (ImageView) findViewById(R.id.SPImagenCincoMayorMayor);
        seisMayorMayor = (ImageView) findViewById(R.id.SPImagenSeisMayorMayor);
        sieteMayorMayor = (ImageView) findViewById(R.id.SPImagenSieteMayorMayor);

        //Asociamos el lístener a los botones.
        unoMayorMayor.setOnClickListener(this);
        dosMayorMayor.setOnClickListener(this);
        tresMayorMayor.setOnClickListener(this);
        cuatroMayorMayor.setOnClickListener(this);
        cincoMayorMayor.setOnClickListener(this);
        seisMayorMayor.setOnClickListener(this);
        sieteMayorMayor.setOnClickListener(this);

        //Asociamos el lístener a los LinearLyuts
        LLunomayorMayor.setOnClickListener(this);
        LLdosmayorMayor.setOnClickListener(this);
        LLtresmayorMayor.setOnClickListener(this);
        LLcuatromayorMayor.setOnClickListener(this);
        LLcincomayorMayor.setOnClickListener(this);
        LLseismayorMayor.setOnClickListener(this);
        LLsietemayorMayor.setOnClickListener(this);

        // Inicializamos los métodos para cada Spinner
        SpinnerUnoMayorPersonalizado();
        SpinnerDosMayorPersonalizado();
        SpinnerTresMayorPersonalizado();
        SpinnerCuatroMayorPersonalizado();
        SpinnerCincoMayorPersonalizado();
        SpinnerSeisMayorPersonalizado();
        SpinnerSieteMayorPersonalizado();


        /* A cada refrencia de los spinner lo enlazamos con su id */
        spinnerUnoMayorMayor = (Spinner) findViewById(R.id.SPUnoMayorMayor);
        spinnerDosMayorMayor = (Spinner) findViewById(R.id.SPDosMayorMayor);
        spinnerTresMayorMayor = (Spinner) findViewById(R.id.SPTresMayorMayor);
        spinnerCuatroMayorMayor = (Spinner) findViewById(R.id.SPCuatroMayorMayor);
        spinnerCincoMayorMayor = (Spinner) findViewById(R.id.SPCincoMayorMayor);
        spinnerSeisMayorMayor = (Spinner) findViewById(R.id.SPSeisMayorMayor);
        spinnerSieteMayorMayor = (Spinner) findViewById(R.id.SPSieteMayorMayor);


        /* Con el método "etOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerUnoMayorMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected (AdapterView < ? > parent, View view,int position, long id){
                SpinnerItemUnoMayorMayor clickedItem = (SpinnerItemUnoMayorMayor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemUnoMayor();
                if (clickedCountryName.equals(getString(R.string.simayor))) {

                    if (SPsimayorMayor == "") {
                        SPsimayorMayor = "CORRECTO: " + clickedCountryName;
                    } else {
                        SPsimayorMayor += " - " + "CORRECTO: " + clickedCountryName;
                    }

                    LLunomayorMayor.setBackgroundResource(R.color.botoncolor);
                    spinnerUnoMayorMayor.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {
                } else {

                    if (SPsimayorMayor == "") {
                        SPsimayorMayor = "INCORRECTO: " + clickedCountryName;
                    } else {
                        SPsimayorMayor += " - " + "INCORRECTO: " + clickedCountryName;
                    }

                    LLunomayorMayor.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected (AdapterView < ? > parent){

            }
        });
        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerDosMayorMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemDosMayorMayor clickedItem = (SpinnerItemDosMayorMayor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemDosMayor();
                if (clickedCountryName.equals(getString(R.string.lamayor))) {

                    if (SPlamayorMayor == ""){
                        SPlamayorMayor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPlamayorMayor += " - " + "CORRECTO: " + clickedCountryName;
                    }
                    LLdosmayorMayor.setBackgroundResource(R.color.botoncolor);
                    spinnerDosMayorMayor.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {


                    if (SPlamayorMayor == ""){
                        SPlamayorMayor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPlamayorMayor += " - " + "INCORRECTO: " + clickedCountryName;
                    }

                    LLdosmayorMayor.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerTresMayorMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemTresMayorMayor clickedItem = (SpinnerItemTresMayorMayor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemTresMayor();
                if (clickedCountryName.equals(getString(R.string.famayor))) {

                    if (SPfamayorMayor == ""){
                        SPfamayorMayor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPfamayorMayor += " - "+ "CORRECTO: " +clickedCountryName;
                    }
                    LLtresmayorMayor.setBackgroundResource(R.color.botoncolor);
                    spinnerTresMayorMayor.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {

                    if (SPfamayorMayor == ""){
                        SPfamayorMayor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPfamayorMayor += " - "+ "INCORRECTO: " +clickedCountryName;
                    }
                    LLtresmayorMayor.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerCuatroMayorMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemCuatroMayorMayor clickedItem = (SpinnerItemCuatroMayorMayor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemCuatroMayor();
                if (clickedCountryName.equals(getString(R.string.solmayor))) {
                    if (SPsolmayorMayor == ""){
                        SPsolmayorMayor = "CORRECTO: " + clickedCountryName;
                    }else{
                        SPsolmayorMayor += " - " + "CORRECTO: " + clickedCountryName;
                    }
                    LLcuatromayorMayor.setBackgroundResource(R.color.botoncolor);
                    spinnerCuatroMayorMayor.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {

                    if (SPsolmayorMayor == ""){
                        SPsolmayorMayor = "INCORRECTO: " + clickedCountryName;
                    }else{
                        SPsolmayorMayor += " - " + "INCORRECTO: " + clickedCountryName;
                    }

                    LLcuatromayorMayor.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerCincoMayorMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemCincoMayorMayor clickedItem = (SpinnerItemCincoMayorMayor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemCincoMayor();
                if (clickedCountryName.equals(getString(R.string.mimayor))) {

                    if (SPmimayorMayor == ""){
                        SPmimayorMayor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPmimayorMayor += " - " + "CORRECTO: " +clickedCountryName;
                    }
                    LLcincomayorMayor.setBackgroundResource(R.color.botoncolor);
                    spinnerCincoMayorMayor.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {


                    if (SPmimayorMayor == ""){
                        SPmimayorMayor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPmimayorMayor += " - " + "INCORRECTO: " +clickedCountryName;
                    }
                    LLcincomayorMayor.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerSeisMayorMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemSeisMayorMayor clickedItem = (SpinnerItemSeisMayorMayor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemSeisMayor();
                if (clickedCountryName.equals(getString(R.string.remayor))) {

                    if (SPremayorMayor == ""){
                        SPremayorMayor = "CORRECTO: " + clickedCountryName;
                    }else{
                        SPremayorMayor += " - " + "CORRECTO: " + clickedCountryName;
                    }
                    LLseismayorMayor.setBackgroundResource(R.color.botoncolor);
                    spinnerSeisMayorMayor.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {


                    if (SPremayorMayor == ""){
                        SPremayorMayor = "INCORRECTO: " + clickedCountryName;
                    }else{
                        SPremayorMayor += " - " + "INCORRECTO: " + clickedCountryName;
                    }
                    LLseismayorMayor.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerSieteMayorMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItemSieteMayorMayor clickedItem = (SpinnerItemSieteMayorMayor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemSieteMayor();
                if (clickedCountryName.equals(getString(R.string.domayor))) {

                    if (SPdomayorMayor == ""){
                        SPdomayorMayor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPdomayorMayor += " - " + "CORRECTO: " +clickedCountryName ;
                    }
                    LLsietemayorMayor.setBackgroundResource(R.color.botoncolor);
                    spinnerSieteMayorMayor.setEnabled(false);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {

                    if (SPdomayorMayor == ""){
                        SPdomayorMayor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPdomayorMayor += " - " + "INCORRECTO: " +clickedCountryName ;
                    }

                    LLsietemayorMayor.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        /* Asignados el origen de los datos desde los recursos. */
        adapterSpinnerUnoMayorMayor = new AdapterSpinnerUnoMayorMayor(this, spinnerItemUnoMayorMayor);
        adapterSPinnerDosMayorMayor = new AdapterSPinnerDosMayorMayor(this, spinnerItemDosMayorMayor);
        adapterSpinnerTresMayorMayor = new AdapterSpinnerTresMayorMayor(this, spinnerItemTresMayorMayor);
        adapterSpinnerCuatroMayorMayor = new AdapterSpinnerCuatroMayorMayor(this, spinnerItemCuatroMayorMayor);
        adapterSpinnerCincoMayorMayor = new AdapterSpinnerCincoMayorMayor(this, spinnerItemCincoMayorMayor);
        adapterSpinnerSeisMayorMayor = new AdapterSpinnerSeisMayorMayor(this, spinnerItemSeisMayorMayor);
        adapterSpinnerSieteMayorMayor = new AdapterSpinnerSieteMayorMayor(this, spinnerItemSieteMayorMayor);


        /* Seteamos el adaptador */
        spinnerUnoMayorMayor.setAdapter(adapterSpinnerUnoMayorMayor);
        spinnerDosMayorMayor.setAdapter(adapterSPinnerDosMayorMayor);
        spinnerTresMayorMayor.setAdapter(adapterSpinnerTresMayorMayor);
        spinnerCuatroMayorMayor.setAdapter(adapterSpinnerCuatroMayorMayor);
        spinnerCincoMayorMayor.setAdapter(adapterSpinnerCincoMayorMayor);
        spinnerSeisMayorMayor.setAdapter(adapterSpinnerSeisMayorMayor);
        spinnerSieteMayorMayor.setAdapter(adapterSpinnerSieteMayorMayor);

        // firebase

        /* Obtenemos la instancia para acceder a la base de datos "firebase"*/
        firebaseDatabase = FirebaseDatabase.getInstance();

        /* Obtenemos instancia para autenticar el usuario*/
        firebaseAuth = FirebaseAuth.getInstance();

        /* Obtenemos instancia y referencias del almacenamiento de firebase */
        /* Obetenemos de la base de datos el usuario actual. */

        /* Le damos el nombre a la tabla que guardará los datos en Firebase*/
        logros_Adivina_Notas_Mayores = firebaseDatabase.getReference("ADIVINA_LOGROS_MAYORES");


    }


    /* Creamos un arreglo para el segundo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerUnoMayorPersonalizado () {
        spinnerItemUnoMayorMayor = new ArrayList<>();
        spinnerItemUnoMayorMayor.add(new SpinnerItemUnoMayorMayor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItemUnoMayorMayor.add(new SpinnerItemUnoMayorMayor(getString(R.string.domayor), R.drawable.ic_nota_dp));
        spinnerItemUnoMayorMayor.add(new SpinnerItemUnoMayorMayor(getString(R.string.lamayor), R.drawable.ic_nota_dp));
        spinnerItemUnoMayorMayor.add(new SpinnerItemUnoMayorMayor(getString(R.string.famayor), R.drawable.ic_nota_dp));
        spinnerItemUnoMayorMayor.add(new SpinnerItemUnoMayorMayor(getString(R.string.remayor), R.drawable.ic_nota_dp));
        spinnerItemUnoMayorMayor.add(new SpinnerItemUnoMayorMayor(getString(R.string.simayor), R.drawable.ic_nota_dp));
        spinnerItemUnoMayorMayor.add(new SpinnerItemUnoMayorMayor(getString(R.string.mimayor), R.drawable.ic_nota_dp));
        spinnerItemUnoMayorMayor.add(new SpinnerItemUnoMayorMayor(getString(R.string.solmayor), R.drawable.ic_nota_dp));
    }
    /* Creamos un arreglo para el segundo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerDosMayorPersonalizado () {
        spinnerItemDosMayorMayor = new ArrayList<>();
        spinnerItemDosMayorMayor.add(new SpinnerItemDosMayorMayor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItemDosMayorMayor.add(new SpinnerItemDosMayorMayor(getString(R.string.remayor), R.drawable.ic_nota_dp));
        spinnerItemDosMayorMayor.add(new SpinnerItemDosMayorMayor(getString(R.string.famayor), R.drawable.ic_nota_dp));
        spinnerItemDosMayorMayor.add(new SpinnerItemDosMayorMayor(getString(R.string.domayor), R.drawable.ic_nota_dp));
        spinnerItemDosMayorMayor.add(new SpinnerItemDosMayorMayor(getString(R.string.mimayor), R.drawable.ic_nota_dp));
        spinnerItemDosMayorMayor.add(new SpinnerItemDosMayorMayor(getString(R.string.solmayor), R.drawable.ic_nota_dp));
        spinnerItemDosMayorMayor.add(new SpinnerItemDosMayorMayor(getString(R.string.simayor), R.drawable.ic_nota_dp));
        spinnerItemDosMayorMayor.add(new SpinnerItemDosMayorMayor(getString(R.string.lamayor), R.drawable.ic_nota_dp));
    }


    /* Creamos un arreglo para el tercer spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerTresMayorPersonalizado () {
        spinnerItemTresMayorMayor = new ArrayList<>();
        spinnerItemTresMayorMayor.add(new SpinnerItemTresMayorMayor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItemTresMayorMayor.add(new SpinnerItemTresMayorMayor(getString(R.string.solmayor), R.drawable.ic_nota_dp));
        spinnerItemTresMayorMayor.add(new SpinnerItemTresMayorMayor(getString(R.string.famayor), R.drawable.ic_nota_dp));
        spinnerItemTresMayorMayor.add(new SpinnerItemTresMayorMayor(getString(R.string.mimayor), R.drawable.ic_nota_dp));
        spinnerItemTresMayorMayor.add(new SpinnerItemTresMayorMayor(getString(R.string.remayor), R.drawable.ic_nota_dp));
        spinnerItemTresMayorMayor.add(new SpinnerItemTresMayorMayor(getString(R.string.domayor), R.drawable.ic_nota_dp));
        spinnerItemTresMayorMayor.add(new SpinnerItemTresMayorMayor(getString(R.string.simayor), R.drawable.ic_nota_dp));
        spinnerItemTresMayorMayor.add(new SpinnerItemTresMayorMayor(getString(R.string.lamayor), R.drawable.ic_nota_dp));


    }

    /* Creamos un arreglo para el cuarto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerCuatroMayorPersonalizado () {
        spinnerItemCuatroMayorMayor = new ArrayList<>();
        spinnerItemCuatroMayorMayor.add(new SpinnerItemCuatroMayorMayor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItemCuatroMayorMayor.add(new SpinnerItemCuatroMayorMayor(getString(R.string.solmayor), R.drawable.ic_nota_dp));
        spinnerItemCuatroMayorMayor.add(new SpinnerItemCuatroMayorMayor(getString(R.string.domayor), R.drawable.ic_nota_dp));
        spinnerItemCuatroMayorMayor.add(new SpinnerItemCuatroMayorMayor(getString(R.string.lamayor), R.drawable.ic_nota_dp));
        spinnerItemCuatroMayorMayor.add(new SpinnerItemCuatroMayorMayor(getString(R.string.famayor), R.drawable.ic_nota_dp));
        spinnerItemCuatroMayorMayor.add(new SpinnerItemCuatroMayorMayor(getString(R.string.simayor), R.drawable.ic_nota_dp));
        spinnerItemCuatroMayorMayor.add(new SpinnerItemCuatroMayorMayor(getString(R.string.mimayor), R.drawable.ic_nota_dp));
        spinnerItemCuatroMayorMayor.add(new SpinnerItemCuatroMayorMayor(getString(R.string.remayor), R.drawable.ic_nota_dp));


    }

    /* Creamos un arreglo para el quinto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerCincoMayorPersonalizado () {
        spinnerItemCincoMayorMayor = new ArrayList<>();
        spinnerItemCincoMayorMayor.add(new SpinnerItemCincoMayorMayor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItemCincoMayorMayor.add(new SpinnerItemCincoMayorMayor(getString(R.string.simayor), R.drawable.ic_nota_dp));
        spinnerItemCincoMayorMayor.add(new SpinnerItemCincoMayorMayor(getString(R.string.lamayor), R.drawable.ic_nota_dp));
        spinnerItemCincoMayorMayor.add(new SpinnerItemCincoMayorMayor(getString(R.string.solmayor), R.drawable.ic_nota_dp));
        spinnerItemCincoMayorMayor.add(new SpinnerItemCincoMayorMayor(getString(R.string.famayor), R.drawable.ic_nota_dp));
        spinnerItemCincoMayorMayor.add(new SpinnerItemCincoMayorMayor(getString(R.string.mimayor), R.drawable.ic_nota_dp));
        spinnerItemCincoMayorMayor.add(new SpinnerItemCincoMayorMayor(getString(R.string.remayor), R.drawable.ic_nota_dp));
        spinnerItemCincoMayorMayor.add(new SpinnerItemCincoMayorMayor(getString(R.string.domayor), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el sexto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerSeisMayorPersonalizado () {
        spinnerItemSeisMayorMayor = new ArrayList<>();
        spinnerItemSeisMayorMayor.add(new SpinnerItemSeisMayorMayor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItemSeisMayorMayor.add(new SpinnerItemSeisMayorMayor(getString(R.string.domayor), R.drawable.ic_nota_dp));
        spinnerItemSeisMayorMayor.add(new SpinnerItemSeisMayorMayor(getString(R.string.remayor), R.drawable.ic_nota_dp));
        spinnerItemSeisMayorMayor.add(new SpinnerItemSeisMayorMayor(getString(R.string.mimayor), R.drawable.ic_nota_dp));
        spinnerItemSeisMayorMayor.add(new SpinnerItemSeisMayorMayor(getString(R.string.famayor), R.drawable.ic_nota_dp));
        spinnerItemSeisMayorMayor.add(new SpinnerItemSeisMayorMayor(getString(R.string.solmayor), R.drawable.ic_nota_dp));
        spinnerItemSeisMayorMayor.add(new SpinnerItemSeisMayorMayor(getString(R.string.lamayor), R.drawable.ic_nota_dp));
        spinnerItemSeisMayorMayor.add(new SpinnerItemSeisMayorMayor(getString(R.string.simayor), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el septimo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerSieteMayorPersonalizado () {
        spinnerItemSieteMayorMayor = new ArrayList<>();
        spinnerItemSieteMayorMayor.add(new SpinnerItemSieteMayorMayor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItemSieteMayorMayor.add(new SpinnerItemSieteMayorMayor(getString(R.string.domayor), R.drawable.ic_nota_dp));
        spinnerItemSieteMayorMayor.add(new SpinnerItemSieteMayorMayor(getString(R.string.mimayor), R.drawable.ic_nota_dp));
        spinnerItemSieteMayorMayor.add(new SpinnerItemSieteMayorMayor(getString(R.string.remayor), R.drawable.ic_nota_dp));
        spinnerItemSieteMayorMayor.add(new SpinnerItemSieteMayorMayor(getString(R.string.famayor), R.drawable.ic_nota_dp));
        spinnerItemSieteMayorMayor.add(new SpinnerItemSieteMayorMayor(getString(R.string.lamayor), R.drawable.ic_nota_dp));
        spinnerItemSieteMayorMayor.add(new SpinnerItemSieteMayorMayor(getString(R.string.solmayor), R.drawable.ic_nota_dp));
        spinnerItemSieteMayorMayor.add(new SpinnerItemSieteMayorMayor(getString(R.string.simayor), R.drawable.ic_nota_dp));


    }






    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            /* Se reproduce el sonido del primer botón */
            case R.id.SPImagenUnoMayorMayor:
                MPsieteMayorMayor.start();
                break;

            /* Se reproduce el sonido del segundo botón */
            case R.id.SPImagenDosMayorMayor:
                MPseisMayorMayor.start();
                break;

            /* Se reproduce el sonido del tercer botón */
            case R.id.SPImagenTresMayorMayor:
                MPcincoMayorMayor.start();
                break;

            /* Se reproduce el sonido del cuarto botón */
            case R.id.SPImagenCuatroMayorMayor:
                MPcuatroMayorMayor.start();
                break;

            /* Se reproduce el sonido del quinto botón */
            case R.id.SPImagenCincoMayorMayor:
                MPtresMayorMayor.start();
                break;

            /* Se reproduce el sonido del sexto botón */
            case R.id.SPImagenSeisMayorMayor:
                MPdosMayorMayor.start();
                break;

            /* Se reproduce el sonido del septimo botón */
            case R.id.SPImagenSieteMayorMayor:
                MPunoMayorMayor.start();
                break;


        }
    }

    /* Método ""InCorreto para mostrar que está incorrecto, es llama cada vez que no se cumplen las condiciones en los botones*/
    private void InCorrecto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.incorrecto));
        imageView.setImageResource(R.drawable.ic_marca_de_cancelar);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
        contadorAdivinaNotasMayoresM++;
    }

    /* Método "Correto" para mostrar que está correcto, es llama cada vez que se cumplen las condiciones en los botones*/
    private void Correcto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.correcto));
        imageView.setImageResource(R.drawable.ic_correcto_simbolo);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        contadorPruebaAdivinaNotasMayores++;
        toast.show();
    }

    private void GuardarRecordAdivinaMayorM() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("RECORD_NOTAS_ADIVINA_MAYORES_MAYORES", contadorAdivinaNotasMayoresM);
        editor.apply();
    }
    private void GuardarAdivinaCorrectoMayor() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (contadorPruebaAdivinaNotasMayores ==7){
            editor.putString("GUARDAR_RECORD_ADIVINA_MAYOR", "SI");
        }else {
            editor.putString("GUARDAR_RECORD_ADIVINA_MAYOR", "NO");
        }
        editor.apply();
    }



    /*Método para bloquear el botón BACK*/
    @Override
    public void onBackPressed(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View guardar = layoutInflater.inflate(R.layout.guardar_notas_menores, null);
        final CardView Guardar = (CardView)guardar.findViewById(R.id.CardGuardar);
        final CardView Cancelar = (CardView)guardar.findViewById(R.id.CardCancelar);

        Guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarRecordAdivinaMayorM();
                GuardarAdivinaCorrectoMayor();

                LogrosAdivinaNotasMayores LAdivinaNotasMayores = new LogrosAdivinaNotasMayores();
                LAdivinaNotasMayores.setADoMayor(SPdomayorMayor);
                LAdivinaNotasMayores.setAReMayor(SPremayorMayor);
                LAdivinaNotasMayores.setAMiMayor(SPmimayorMayor);
                LAdivinaNotasMayores.setAFaMayor(SPfamayorMayor);
                LAdivinaNotasMayores.setALaMayor(SPlamayorMayor);
                LAdivinaNotasMayores.setASolMayor(SPsolmayorMayor);
                LAdivinaNotasMayores.setASiMayor(SPsimayorMayor);

                final String id = user.get(sesion.USER_ID);
                logros_Adivina_Notas_Mayores.child(id).setValue(LAdivinaNotasMayores).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        startActivity(new Intent(AdivinaNotasMayor.this, AdivinaLaNota.class));
                        Toast.makeText(AdivinaNotasMayor.this, "Datos guardados de forma correcta", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                    }
                });

            }
        });

        Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        builder.setView(guardar);
        builder.show();

    }
    //metodo de tipo booleano para comprobar la conexion a internet
    public Boolean isOnlineNet() {
        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");
            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

}