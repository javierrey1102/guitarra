package facci.pm.velezbriones.misprimerosacordes21.EscuchaNotasMusicales;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;

import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NotasMenor7 extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout DoMenorSiete, ReMenorSiete, MiMenorSiete, FaMenorSiete, SolMenorSiete, LaMenorSiete, SiMenorSiete;
    private ImageView TodasMenorSiete;
    private MediaPlayer MPdomenor7, MPremenor7, MPmimenor7, MPfamenor7, MPsolmenor7, MPlamenor7, MPsimenor7;
    private int contadorNotasMenoresEscuchadas7, escuhaNotasMayores7Contador;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        LeerRecordNotasAdivinaMayor7();

        setContentView(R.layout.activity_notas_menor7);
    DoMenorSiete = (LinearLayout)findViewById(R.id.BTNDomenorS);
    ReMenorSiete = (LinearLayout)findViewById(R.id.BTNRemenorS);
    MiMenorSiete = (LinearLayout)findViewById(R.id.BTNMimenorS);
    FaMenorSiete = (LinearLayout)findViewById(R.id.BTNFamenorS);
    SolMenorSiete = (LinearLayout)findViewById(R.id.BTNSolmenorS);
    LaMenorSiete = (LinearLayout)findViewById(R.id.BTNLamenorS);
    SiMenorSiete = (LinearLayout)findViewById(R.id.BTNSimenorS);
    TodasMenorSiete = (ImageView)findViewById(R.id.IMGDoMenorSiete);

        //Dentro de este método OnCreate asociamos el lístener a los botones.
        DoMenorSiete.setOnClickListener(this);
        ReMenorSiete.setOnClickListener(this);
        MiMenorSiete.setOnClickListener(this);
        FaMenorSiete.setOnClickListener(this);
        SolMenorSiete.setOnClickListener(this);
        LaMenorSiete.setOnClickListener(this);
        SiMenorSiete.setOnClickListener(this);

        //  A cada referencia de MediaPlayer lo enlazamos con su respectivo sonido.
        MPdomenor7 = MediaPlayer.create(this, R.raw.domenor7);
        MPremenor7 = MediaPlayer.create(this, R.raw.remenor7);
        MPmimenor7 = MediaPlayer.create(this, R.raw.mimenor7);
        MPfamenor7 = MediaPlayer.create(this, R.raw.famenor7);
        MPsolmenor7 = MediaPlayer.create(this, R.raw.solmenor7);
        MPlamenor7 = MediaPlayer.create(this, R.raw.lamenor7);
        MPsimenor7 = MediaPlayer.create(this, R.raw.simenor7);
    }




    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.BTNDomenorS:
                // Se reproduce la nota musical y se muestra la imagen de la nota.
                MPdomenor7.start();
                contadorNotasMenoresEscuchadas7++;
                TodasMenorSiete.setImageResource(R.drawable.domenor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                DoMenorSiete.setBackgroundResource(R.drawable.degradadoboton);
                ReMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                MiMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                FaMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                SolMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                LaMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                SiMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPremenor7.isPlaying()){
                    MPremenor7.pause();
                    try {
                        MPremenor7.stop();
                        MPremenor7.prepare();
                        MPremenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                    MPdomenor7.start();
                }else if (MPmimenor7.isPlaying()){
                    MPmimenor7.pause();
                    try {
                        MPmimenor7.stop();
                        MPmimenor7.prepare();
                        MPmimenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPfamenor7.isPlaying()){
                    MPfamenor7.pause();
                    try {
                        MPfamenor7.stop();
                        MPfamenor7.prepare();
                        MPfamenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsolmenor7.isPlaying()) {
                    MPsolmenor7.pause();
                    try {
                        MPsolmenor7.stop();
                        MPsolmenor7.prepare();
                        MPsolmenor7.seekTo(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else if (MPlamenor7.isPlaying()){
                    MPlamenor7.pause();
                    try {
                        MPlamenor7.stop();
                        MPlamenor7.prepare();
                        MPlamenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimenor7.isPlaying()){
                    MPsimenor7.pause();
                    try {
                        MPsimenor7.stop();
                        MPsimenor7.prepare();
                        MPsimenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;

            // Se reproduce la nota musical y se muestra la imagen de la nota.
            case  R.id.BTNRemenorS:
                contadorNotasMenoresEscuchadas7++;
                MPremenor7.start();
                TodasMenorSiete.setImageResource(R.drawable.remenor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                DoMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                ReMenorSiete.setBackgroundResource(R.drawable.degradadoboton);
                MiMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                FaMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                SolMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                LaMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                SiMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);

                // Si está sonando la  nota musical "do menor 7", entonces se detiene  y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomenor7.stop();
                try {
                    MPdomenor7.stop();
                    MPdomenor7.prepare();
                    MPdomenor7.seekTo(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPmimenor7.isPlaying()){
                    MPmimenor7.pause();
                    try {
                        MPmimenor7.stop();
                        MPmimenor7.prepare();
                        MPmimenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPfamenor7.isPlaying()){
                    MPfamenor7.pause();
                    try {
                        MPfamenor7.stop();
                        MPfamenor7.prepare();
                        MPfamenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsolmenor7.isPlaying()){
                    MPsolmenor7.pause();
                    try {
                        MPsolmenor7.stop();
                        MPsolmenor7.prepare();
                        MPsolmenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPlamenor7.isPlaying()){
                    MPlamenor7.pause();
                    try {
                        MPlamenor7.stop();
                        MPlamenor7.prepare();
                        MPlamenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimenor7.isPlaying()){
                    MPsimenor7.pause();
                    try {
                        MPsimenor7.stop();
                        MPsimenor7.prepare();
                        MPsimenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;
            // Se reproduce la nota musical y se muestra la imagen de la nota.
            case R.id.BTNMimenorS:
                contadorNotasMenoresEscuchadas7++;
                MPmimenor7.start();
                TodasMenorSiete.setImageResource(R.drawable.mimenor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                DoMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                ReMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                MiMenorSiete.setBackgroundResource(R.drawable.degradadoboton);
                FaMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                SolMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                LaMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                SiMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);

                // Si están sonando las  notas musicales "do menor 7" o "re menor 7", entonces se detiene  y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomenor7.stop();
                try {
                    MPdomenor7.stop();
                    MPdomenor7.prepare();
                    MPdomenor7.seekTo(0);
                }catch (IOException r){
                    r.printStackTrace();
                }
                MPremenor7.stop();
                try {
                    MPremenor7.stop();
                    MPremenor7.prepare();
                    MPremenor7.seekTo(0);
                }catch (IOException r){
                    r.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPfamenor7.isPlaying()){
                    MPfamenor7.pause();
                    try {
                        MPfamenor7.stop();
                        MPfamenor7.prepare();
                        MPfamenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                } else if (MPsolmenor7.isPlaying()){
                    MPsolmenor7.pause();
                    try {
                        MPsolmenor7.stop();
                        MPsolmenor7.prepare();
                        MPsolmenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPlamenor7.isPlaying()){
                    MPlamenor7.pause();
                    try {
                        MPlamenor7.stop();
                        MPlamenor7.prepare();
                        MPlamenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimenor7.isPlaying()){
                    MPsimenor7.pause();
                    try {
                        MPsimenor7.stop();
                        MPsimenor7.prepare();
                        MPsimenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;

            // Se reproduce la nota musical y se muestra la imagen de la nota.
            case R.id.BTNFamenorS:
                contadorNotasMenoresEscuchadas7++;
                MPfamenor7.start();
                TodasMenorSiete.setImageResource(R.drawable.famenor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                DoMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                ReMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                MiMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                FaMenorSiete.setBackgroundResource(R.drawable.degradadoboton);
                SolMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                LaMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                SiMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);


                // Si están sonando una de estas notas musicales "do menor 7", "re menor 7" o "mi menor 7", entonces se detienen  y dan paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomenor7.stop();
                try {
                    MPdomenor7.stop();
                    MPdomenor7.prepare();
                    MPdomenor7.seekTo(0);
                }catch (IOException q){
                    q.printStackTrace();
                }
                MPremenor7.stop();
                try {
                    MPremenor7.stop();
                    MPremenor7.prepare();
                    MPremenor7.seekTo(0);
                }catch (IOException rq){
                    rq.printStackTrace();
                }
                MPmimenor7.stop();
                try {
                    MPmimenor7.stop();
                    MPmimenor7.prepare();
                    MPmimenor7.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }

                if (MPsolmenor7.isPlaying())
                {
                    MPsolmenor7.pause();
                    try {
                        MPsolmenor7.stop();
                        MPsolmenor7.prepare();
                        MPsolmenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                } else if (MPlamenor7.isPlaying()){
                    MPlamenor7.pause();
                    try {
                        MPlamenor7.stop();
                        MPlamenor7.prepare();
                        MPlamenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                } else if (MPsimenor7.isPlaying()){
                    MPsimenor7.pause();
                    try {
                        MPsimenor7.stop();
                        MPsimenor7.prepare();
                        MPsimenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;

            // Se reproduce la nota musical y se muestra la imagen de la nota.
            case  R.id.BTNSolmenorS:
                contadorNotasMenoresEscuchadas7++;
                MPsolmenor7.start();
                TodasMenorSiete.setImageResource(R.drawable.solmenor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                DoMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                ReMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                MiMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                FaMenorSiete.setBackgroundResource(R.drawable.ic_botonsonido);
                SolMenorSiete.setBackgroundResource(R.drawable.degradadoboton);
                LaMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                SiMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);

                // Si están sonando una de estas notas musicales "do menor 7", "re menor 7", "mi menor 7" o "fa menor 7", entonces se detienen  y dan paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomenor7.stop();
                try {
                    MPdomenor7.stop();
                    MPdomenor7.prepare();
                    MPdomenor7.seekTo(0);
                }catch (IOException w){
                    w.printStackTrace();
                }
                MPremenor7.stop();
                try {
                    MPremenor7.stop();
                    MPremenor7.prepare();
                    MPremenor7.seekTo(0);
                }catch (IOException wq){
                    wq.printStackTrace();
                }
                MPmimenor7.stop();
                try {
                    MPmimenor7.stop();
                    MPmimenor7.prepare();
                    MPmimenor7.seekTo(0);
                }catch (IOException wp){
                    wp.printStackTrace();
                }
                MPfamenor7.stop();
                try {
                    MPfamenor7.stop();
                    MPfamenor7.prepare();
                    MPfamenor7.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPlamenor7.isPlaying()){
                    MPlamenor7.stop();
                    try {
                        MPlamenor7.stop();
                        MPlamenor7.prepare();
                        MPlamenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                } else if (MPsimenor7.isPlaying()){
                    MPsimenor7.stop();
                    try {
                        MPsimenor7.stop();
                        MPsimenor7.prepare();
                        MPsimenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;

            // Se reproduce la nota musical y se muestra la imagen de la nota.
            case R.id.BTNLamenorS:
                contadorNotasMenoresEscuchadas7++;
                MPlamenor7.start();
                TodasMenorSiete.setImageResource(R.drawable.lamenor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                DoMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                ReMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                MiMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                FaMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                SolMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                LaMenorSiete.setBackgroundResource(R.drawable.degradadoboton);
                SiMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);

                // Si están sonando una de estas notas musicales "do menor 7", "re menor 7", "mi menor 7", "fa menor 7" o "sol menor 7", entonces se detienen  y dan paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomenor7.stop();
                try {
                    MPdomenor7.stop();
                    MPdomenor7.prepare();
                    MPdomenor7.seekTo(0);
                }catch (IOException t){
                    t.printStackTrace();
                }
                MPremenor7.stop();
                try {
                    MPremenor7.stop();
                    MPremenor7.prepare();
                    MPremenor7.seekTo(0);
                }catch (IOException tq){
                    tq.printStackTrace();
                }
                MPmimenor7.stop();
                try {
                    MPmimenor7.stop();
                    MPmimenor7.prepare();
                    MPmimenor7.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }
                MPfamenor7.stop();
                try {
                    MPfamenor7.stop();
                    MPfamenor7.prepare();
                    MPfamenor7.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }
                MPsolmenor7.stop();
                try {
                    MPsolmenor7.stop();
                    MPsolmenor7.prepare();
                    MPsolmenor7.seekTo(0);
                }catch (IOException ff){
                    ff.printStackTrace();
                }

                // Si está sonando la nota musical "si menor 7", entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                if (MPsimenor7.isPlaying()){
                    MPsimenor7.stop();
                    try {
                        MPsimenor7.stop();
                        MPsimenor7.prepare();
                        MPsimenor7.seekTo(0);
                    }catch (IOException xr){
                        xr.printStackTrace();
                    }
                }
                break;

            // Se reproduce la nota musical y se muestra la imagen de la nota.
            case R.id.BTNSimenorS:
                contadorNotasMenoresEscuchadas7++;
                MPsimenor7.start();
                TodasMenorSiete.setImageResource(R.drawable.simenor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                DoMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                ReMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                MiMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                FaMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                SolMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                LaMenorSiete.setBackgroundResource(R.drawable.degradadobotonblanco);
                SiMenorSiete.setBackgroundResource(R.drawable.degradadoboton);


                // Si están sonando una de estas notas musicales "do menor 7", "re menor 7", "mi menor 7", "fa menor 7", "sol menor 7" o "la menor 7",
                // entonces se detienen  y dan paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomenor7.stop();
                try {
                    MPdomenor7.stop();
                    MPdomenor7.prepare();
                    MPdomenor7.seekTo(0);
                }catch (IOException t){
                    t.printStackTrace();
                }

                MPremenor7.stop();
                try {
                    MPremenor7.stop();
                    MPremenor7.prepare();
                    MPremenor7.seekTo(0);
                }catch (IOException tq){
                    tq.printStackTrace();
                }

                MPmimenor7.stop();
                try {
                    MPmimenor7.stop();
                    MPmimenor7.prepare();
                    MPmimenor7.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }

                MPfamenor7.stop();
                try {
                    MPfamenor7.stop();
                    MPfamenor7.prepare();
                    MPfamenor7.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                MPsolmenor7.stop();
                try {
                    MPsolmenor7.stop();
                    MPsolmenor7.prepare();
                    MPsolmenor7.seekTo(0);
                }catch (IOException pr){
                    pr.printStackTrace();
                }

                MPlamenor7.stop();
                try {
                    MPlamenor7.stop();
                    MPlamenor7.prepare();
                    MPlamenor7.seekTo(0);
                }catch (IOException ju){
                    ju.printStackTrace();
                }
                break;



        }

    }

    private void LeerRecordNotasAdivinaMayor7() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        escuhaNotasMayores7Contador = sharedPreferences.getInt("ESCUCHA_NOTAS_MENOR_7", 0);

    }

    private void GuardarRecordMenor7() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("ESCUCHA_NOTAS_MENOR_7", escuhaNotasMayores7Contador + contadorNotasMenoresEscuchadas7);
        editor.apply();
    }
    public void onBackPressed(){
        GuardarRecordMenor7();
        finish();
    }
}
