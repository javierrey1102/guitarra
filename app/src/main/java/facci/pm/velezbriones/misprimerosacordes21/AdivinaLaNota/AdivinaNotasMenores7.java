package facci.pm.velezbriones.misprimerosacordes21.AdivinaLaNota;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;

import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerCincoMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerCuatroMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerDosMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerSeisMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerSieteMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerTresMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerUnoMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7.SpinnerItenCuatroMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7.SpinnerItenDosMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7.SpinnerItenSeisMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7.SpinnerItenSieteMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7.SpinnerItenTresMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7.SpinnerItenUnoMenor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7.SpinneritemCincoMenor;
import facci.pm.velezbriones.misprimerosacordes21.Clases.Sesion;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosAdivinaNotasMenores7;
import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AdivinaNotasMenores7 extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout LLunomenor7, LLdosmenor7, LLtresmenor7, LLcuatromenor7, LLcincomenor7, LLseismenor7, LLsietemenor7;
    /*Creamos instancias de tipo String y las inicializamos en blanco*/
    private String SPdomenor = "";
    private String SPremenor = "";
    private String SPmimenor = "";
    private String SPsolmenor = "";
    private String SPlamenor = "";
    private String SPsimenor = "";
    private String SPfamenor = "";
    private int contadorAdivinaNotasMenores7, contadorPruebaAdivinaNotasMenores7;

    //Todo lo que tenga que ver con firebase :v
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference logros_Adivina_Notas_Menores_7, users;
    private FirebaseAuth.AuthStateListener authStateListener;
    private String userId;

    //local data
    private Sesion sesion;
    private HashMap<String, String> user;


    /* Necesitamos una lista que deseamos que muestre el spinner. Agarramos los métodos de la clase creada (SpinnerItenUnoMenor)
   también llamamos a esta clase los métodos de la clase AdapterSpinnerUno para poder implementarlos.
   Asi mismo con los demás...
   */
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItenUnoMenor> spinnerItenUnoMenors;
    private AdapterSpinnerUnoMenor adapterSpinnerUno;
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItenDosMenor> spinnerItenDosMenors;
    private AdapterSpinnerDosMenor adapterSpinnerDos;
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItenTresMenor> spinnerItenTresMenors;
    private AdapterSpinnerTresMenor adapterSpinnerTres;
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItenCuatroMenor> spinnerItenCuatroMenors;
    private AdapterSpinnerCuatroMenor adapterSpinnerCuatro;
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinneritemCincoMenor> spinnerItenCincoMenors;
    private AdapterSpinnerCincoMenor adapterSpinnerCinco;
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItenSeisMenor> spinnerItenSeisMenors;
    private AdapterSpinnerSeisMenor adapterSpinnerSeis;
    /*Creamos instancia de las clases por medio de un ArrayList y adapter*/
    private ArrayList<SpinnerItenSieteMenor> spinnerItenSieteMenors;
    private AdapterSpinnerSieteMenor adapterSpinnerSiete;

    /* Instanciamos cada objeto de la clase privada Spinner */
    private Spinner spinnerUnoMenor, spinnerDosMenor, spinnerTresMenor, spinnerCuatroMenor,
            spinnerCincoMenor, spinnerSeisMenor, spinnerSieteMenor;

    /* Instanciamos cada objeto de la clase privada Imageview y MediaPlayer */
    private ImageView unoMenor, dosMenor, tresMenor, cuatroMenor, cincoMenor, seisMenor, sieteMenor;
    private MediaPlayer MPunoMenor, MPdosMenor, MPtresMenor, MPcuatroMenor, MPcincoMenor, MPseisMenor, MPsieteMenor;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        setContentView(R.layout.activity_adivina_notas_menores7);

        sesion = new Sesion(this);
        user = sesion.LoggedInUser();



        /*A cada referencia de LinarLiout lo enlazamos con si id*/
        LLunomenor7 = (LinearLayout)findViewById(R.id.LLUnoMenor7);
        LLdosmenor7 = (LinearLayout)findViewById(R.id.LLDosMenor7);
        LLtresmenor7 = (LinearLayout)findViewById(R.id.LLTresMenor7);
        LLcuatromenor7 = (LinearLayout)findViewById(R.id.LLCUatroMenor7);
        LLcincomenor7 = (LinearLayout)findViewById(R.id.LLCincoMenor7);
        LLseismenor7 = (LinearLayout)findViewById(R.id.LLSeisMenor7);
        LLsietemenor7 = (LinearLayout)findViewById(R.id.LLSieteMenor7);

        /* A cada referencia de MediaPlayer la enlazamos con su id */
        MPunoMenor = MediaPlayer.create(this, R.raw.domenor7);
        MPdosMenor = MediaPlayer.create(this, R.raw.remenor7);
        MPtresMenor = MediaPlayer.create(this, R.raw.mimenor7);
        MPcuatroMenor = MediaPlayer.create(this, R.raw.famenor7);
        MPcincoMenor = MediaPlayer.create(this, R.raw.solmenor7);
        MPseisMenor = MediaPlayer.create(this, R.raw.lamenor7);
        MPsieteMenor = MediaPlayer.create(this, R.raw.simenor7);

        /* A cada referencia de las ImageView las enlazamos con su id */
        unoMenor = (ImageView)findViewById(R.id.SPImagenUnoMenor7);
        dosMenor = (ImageView)findViewById(R.id.SPImagenDosMenor7);
        tresMenor = (ImageView)findViewById(R.id.SPImagenTresMenor7);
        cuatroMenor = (ImageView)findViewById(R.id.SPImagenCuatroMenor7);
        cincoMenor = (ImageView)findViewById(R.id.SPImagenCincoMenor7);
        seisMenor = (ImageView)findViewById(R.id.SPImagenSeisMenor7);
        sieteMenor = (ImageView)findViewById(R.id.SPImagenSieteMenor7);

        //Asociamos el lístener a los botones.
        unoMenor.setOnClickListener(this);
        dosMenor.setOnClickListener(this);
        tresMenor.setOnClickListener(this);
        cuatroMenor.setOnClickListener(this);
        cincoMenor.setOnClickListener(this);
        seisMenor.setOnClickListener(this);
        sieteMenor.setOnClickListener(this);

        //Asociamos el listener a los LinearLiout
        LLunomenor7.setOnClickListener(this);
        LLdosmenor7.setOnClickListener(this);
        LLtresmenor7.setOnClickListener(this);
        LLcuatromenor7.setOnClickListener(this);
        LLcincomenor7.setOnClickListener(this);
        LLseismenor7.setOnClickListener(this);

        // Inicializamos los métodos para cada Spinner
        SpinnerUnoMenorPersonalizado();
        SpinnerDosMenorPersonalizado();
        SpinnerTresMenorPersonalizado();
        SpinnerCuatroMenorPersonalizado();
        SpinnerCincoMenorPersonalizado();
        SpinnerSeisMenorPersonalizado();
        SpinnerSieteMenorPersonalizado();

        /* A cada refrencia de los spinner lo enlazamos con su id */
        spinnerUnoMenor = (Spinner) findViewById(R.id.SPUnoMenor7);
        spinnerDosMenor = (Spinner)findViewById(R.id.SPDosMenor7);
        spinnerTresMenor = (Spinner)findViewById(R.id.SPTresMenor7);
        spinnerCuatroMenor = (Spinner)findViewById(R.id.SPCuatroMenor7);
        spinnerCincoMenor = (Spinner)findViewById(R.id.SPCincoMenor7);
        spinnerSeisMenor = (Spinner)findViewById(R.id.SPSeisMenor7);
        spinnerSieteMenor = (Spinner)findViewById(R.id.SPSieteMenor7);


        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerUnoMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenUnoMenor clickedItem = (SpinnerItenUnoMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemMemor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.simenor7))){

                    if (SPsimenor==""){
                        SPsimenor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPsimenor += " - " + "CORRECTO: " + clickedCountryName;
                    }

                    LLunomenor7.setBackgroundResource(R.color.botoncolor);
                    spinnerUnoMenor.setEnabled(false);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {
                    if (SPsimenor==""){
                        SPsimenor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPsimenor += " - " + "INCORRECTO: " + clickedCountryName;
                    }

                    LLunomenor7.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerDosMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenDosMenor clickedItem = (SpinnerItenDosMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemDosMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.lamenor7))){

                    if (SPlamenor==""){
                        SPlamenor = "CORRECTO: " + clickedCountryName;
                    }else {

                        SPlamenor += " - " + "CORRECTO: " + clickedCountryName;
                    }

                    LLdosmenor7.setBackgroundResource(R.color.botoncolor);
                    Correcto();
                    spinnerDosMenor.setEnabled(false);
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {

                    if (SPlamenor==""){
                        SPlamenor = "INCORRECTO: " + clickedCountryName;
                    }else {

                        SPlamenor += " - " + "INCORRECTO: " + clickedCountryName;
                    }

                    LLdosmenor7.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerTresMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenTresMenor clickedItem = (SpinnerItenTresMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemTresMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.solmenor7))){


                    if (SPsolmenor== ""){
                        SPsolmenor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPsolmenor += " - " + "CORRECTO: "+ clickedCountryName;
                    }
                    LLtresmenor7.setBackgroundResource(R.color.botoncolor);
                    spinnerTresMenor.setEnabled(false);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {
                    if (SPsolmenor== ""){
                        SPsolmenor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPsolmenor += " - " + "INCORRECTO: "+ clickedCountryName;
                    }

                    LLtresmenor7.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerCuatroMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenCuatroMenor clickedItem = (SpinnerItenCuatroMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemCuatroMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.famenor7))){

                    if (SPfamenor ==""){
                        SPfamenor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPfamenor += " - " + "CORRECTO: " + clickedCountryName;
                    }
                    LLcuatromenor7.setBackgroundResource(R.color.botoncolor);
                    spinnerCuatroMenor.setEnabled(false);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {


                    if (SPfamenor ==""){
                        SPfamenor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPfamenor += " - " + "INCORRECTO: " + clickedCountryName;
                    }
                    LLcuatromenor7.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerCincoMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinneritemCincoMenor clickedItem = (SpinneritemCincoMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemCincoMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.mimenor7))){

                    if (SPmimenor == ""){
                        SPmimenor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPmimenor += " - " + "CORRECTO: " + clickedCountryName;
                    }
                    LLcincomenor7.setBackgroundResource(R.color.botoncolor);
                    spinnerCincoMenor.setEnabled(false);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {

                    if (SPmimenor == ""){
                        SPmimenor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPmimenor += " - " + "INCORRECTO: " + clickedCountryName;
                    }

                    LLcincomenor7.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerSeisMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenSeisMenor clickedItem = (SpinnerItenSeisMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemSeisMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.remenor7))){
                    if (SPremenor == ""){
                        SPremenor = "CORRECTO: " + clickedCountryName;
                    }else {
                        SPremenor += " - " + "CORRECTO: " + clickedCountryName;
                    }

                    LLseismenor7.setBackgroundResource(R.color.botoncolor);
                    spinnerSeisMenor.setEnabled(false);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {
                    if (SPremenor == ""){
                        SPremenor = "INCORRECTO: " + clickedCountryName;
                    }else {
                        SPremenor += " - " + "INCORRECTO: " + clickedCountryName;
                    }
                    LLseismenor7.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerSieteMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenSieteMenor clickedItem = (SpinnerItenSieteMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemSieteMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.domenor7))){

                    if (SPdomenor == ""){
                        SPdomenor = "CORRECTO: " + clickedCountryName;
                    }else{
                        SPdomenor += " - " + "CORRECTO: " +clickedCountryName;
                    }

                    LLsietemenor7.setBackgroundResource(R.color.botoncolor);
                    spinnerSieteMenor.setEnabled(false);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {

                    if (SPdomenor == ""){
                        SPdomenor = "INCORRECTO: " + clickedCountryName;
                    }else{
                        SPdomenor += " - " + "INCORRECTO: " +clickedCountryName;
                    }

                    LLsietemenor7.setBackgroundResource(R.color.backgroundcolor);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });





        /* Asignados el origen de los datos desde los recursos. */
        adapterSpinnerUno = new AdapterSpinnerUnoMenor(this, spinnerItenUnoMenors);
        adapterSpinnerDos = new AdapterSpinnerDosMenor(this, spinnerItenDosMenors);
        adapterSpinnerTres = new AdapterSpinnerTresMenor(this, spinnerItenTresMenors);
        adapterSpinnerCuatro = new AdapterSpinnerCuatroMenor(this, spinnerItenCuatroMenors);
        adapterSpinnerCinco = new AdapterSpinnerCincoMenor(this, spinnerItenCincoMenors);
        adapterSpinnerSeis = new AdapterSpinnerSeisMenor(this, spinnerItenSeisMenors);
        adapterSpinnerSiete = new AdapterSpinnerSieteMenor(this, spinnerItenSieteMenors);


        /* Seteamos el adaptador */
        spinnerUnoMenor.setAdapter(adapterSpinnerUno);
        spinnerDosMenor.setAdapter(adapterSpinnerDos);
        spinnerTresMenor.setAdapter(adapterSpinnerTres);
        spinnerCuatroMenor.setAdapter(adapterSpinnerCuatro);
        spinnerCincoMenor.setAdapter(adapterSpinnerCinco);
        spinnerSeisMenor.setAdapter(adapterSpinnerSeis);
        spinnerSieteMenor.setAdapter(adapterSpinnerSiete);

        // firebase

        /* Obtenemos la instancia para acceder a la base de datos "firebase"*/
        firebaseDatabase = FirebaseDatabase.getInstance();

        /* Obtenemos instancia para autenticar el usuario*/
        firebaseAuth = FirebaseAuth.getInstance();

        /* Obtenemos instancia y referencias del almacenamiento de firebase */
        /* Obetenemos de la base de datos el usuario actual. */

        /* Le damos el nombre a la tabla que guardará los datos en Firebase*/
        logros_Adivina_Notas_Menores_7 = firebaseDatabase.getReference("ADIVINA_LOGROS_MENORES_7");


    }

    /* Creamos un arreglo para el primer spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerUnoMenorPersonalizado() {
        spinnerItenUnoMenors = new ArrayList<>();
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.simenor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.domenor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.famenor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.remenor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.mimenor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el segundo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerDosMenorPersonalizado() {
        spinnerItenDosMenors = new ArrayList<>();
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.mimenor7), R.drawable.ic_nota_dp));
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.lamenor7), R.drawable.ic_nota_dp));
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.domenor7), R.drawable.ic_nota_dp));
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.simenor7), R.drawable.ic_nota_dp));
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.famenor7), R.drawable.ic_nota_dp));
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.remenor7), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el tercer spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerTresMenorPersonalizado() {
        spinnerItenTresMenors = new ArrayList<>();
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.remenor7), R.drawable.ic_nota_dp));
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.mimenor7), R.drawable.ic_nota_dp));
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.lamenor7), R.drawable.ic_nota_dp));
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.famenor7), R.drawable.ic_nota_dp));
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.domenor7), R.drawable.ic_nota_dp));
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.simenor7), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el cuarto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerCuatroMenorPersonalizado() {
        spinnerItenCuatroMenors = new ArrayList<>();
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.domenor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.famenor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.lamenor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.remenor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.simenor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.mimenor7), R.drawable.ic_nota_dp));
    }


    /* Creamos un arreglo para el quinto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerCincoMenorPersonalizado() {
        spinnerItenCincoMenors = new ArrayList<>();
        spinnerItenCincoMenors.add(new SpinneritemCincoMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenCincoMenors.add(new SpinneritemCincoMenor(getString(R.string.famenor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMenors.add(new SpinneritemCincoMenor(getString(R.string.lamenor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMenors.add(new SpinneritemCincoMenor(getString(R.string.domenor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMenors.add(new SpinneritemCincoMenor(getString(R.string.simenor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMenors.add(new SpinneritemCincoMenor(getString(R.string.remenor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMenors.add(new SpinneritemCincoMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMenors.add(new SpinneritemCincoMenor(getString(R.string.mimenor7), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el sexto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerSeisMenorPersonalizado() {
        spinnerItenSeisMenors = new ArrayList<>();
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.simenor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.lamenor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.famenor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.mimenor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.remenor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.domenor7), R.drawable.ic_nota_dp));
    }
    /* Creamos un arreglo para el septimo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerSieteMenorPersonalizado() {
        spinnerItenSieteMenors = new ArrayList<>();
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.domenor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.famenor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.remenor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.mimenor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.lamenor7), R.drawable.ic_nota_dp));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /* Se reproduce el sonido del primer botón */
            case R.id.SPImagenUnoMenor7:
                MPsieteMenor.start();
                break;

            /* Se reproduce el sonido del segundo botón */
            case R.id.SPImagenDosMenor7:
                MPseisMenor.start();
                break;

            /* Se reproduce el sonido del tercer botón */
            case R.id.SPImagenTresMenor7:
                MPcincoMenor.start();
                break;

            /* Se reproduce el sonido del cuarto botón */
            case R.id.SPImagenCuatroMenor7:
                MPcuatroMenor.start();
                break;

            /* Se reproduce el sonido del quinto botón */
            case  R.id.SPImagenCincoMenor7:
                MPtresMenor.start();
                break;

            /* Se reproduce el sonido del sexto botón */
            case R.id.SPImagenSeisMenor7:
                MPdosMenor.start();
                break;

            /* Se reproduce el sonido del septimo botón */
            case R.id.SPImagenSieteMenor7:
                MPunoMenor.start();
                break;
        }
    }

    /* Método ""InCorreto para mostrar que está incorrecto, es llama cada vez que no se cumplen las condiciones en los botones*/
    private void InCorrecto(){

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.incorrecto));
        imageView.setImageResource(R.drawable.ic_marca_de_cancelar);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
        contadorAdivinaNotasMenores7++;
    }

    /* Método "Correto" para mostrar que está correcto, es llama cada vez que se cumplen las condiciones en los botones*/
    private void Correcto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.correcto));
        imageView.setImageResource(R.drawable.ic_correcto_simbolo);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
        contadorPruebaAdivinaNotasMenores7++;
    }
    private void GuardarRecordAdivinaMenor7() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("RECORD_NOTAS_ADIVINA_MENORES_7", contadorAdivinaNotasMenores7);
        editor.apply();
    }

    private void GuardarAdivinaCorrectoMenor7() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (contadorPruebaAdivinaNotasMenores7 ==7){
            editor.putString("GUARDAR_RECORD_ADIVINA_MENOR_7", "SI");
        }else {
            editor.putString("GUARDAR_RECORD_ADIVINA_MENOR_7", "NO");
        }
        editor.apply();
    }

    /*Método para bloquear el botón BACK*/
    @Override
    public void onBackPressed(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View guardar = layoutInflater.inflate(R.layout.guardar_notas_menores, null);
        final CardView Guardar = (CardView)guardar.findViewById(R.id.CardGuardar);
        final CardView Cancelar = (CardView)guardar.findViewById(R.id.CardCancelar);

        Guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarAdivinaCorrectoMenor7();
                GuardarRecordAdivinaMenor7();

                LogrosAdivinaNotasMenores7 LAdivinaNotasMenores7 = new LogrosAdivinaNotasMenores7();
                LAdivinaNotasMenores7.setADoMenor7(SPdomenor);
                LAdivinaNotasMenores7.setAReMenor7(SPremenor);
                LAdivinaNotasMenores7.setAMiMenor7(SPmimenor);
                LAdivinaNotasMenores7.setAFaMenor7(SPfamenor);
                LAdivinaNotasMenores7.setASolMenor7(SPsolmenor);
                LAdivinaNotasMenores7.setALaMenor7(SPsolmenor);
                LAdivinaNotasMenores7.setASiMenor7(SPsimenor);

                final String id = user.get(sesion.USER_ID);
                logros_Adivina_Notas_Menores_7.child(id).setValue(LAdivinaNotasMenores7).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        startActivity(new Intent(AdivinaNotasMenores7.this, AdivinaLaNota.class));
                        Toast.makeText(AdivinaNotasMenores7.this, "Datos guardados de forma correcta", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                    }
                });

            }
        });

        Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        builder.setView(guardar);
        builder.show();

    }
    //metodo de tipo booleano para comprobar la conexion a internet
    public Boolean isOnlineNet() {
        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");
            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
}
