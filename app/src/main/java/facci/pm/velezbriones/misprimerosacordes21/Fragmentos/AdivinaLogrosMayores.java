package facci.pm.velezbriones.misprimerosacordes21.Fragmentos;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosAdivinaNotasMayores;
import facci.pm.velezbriones.misprimerosacordes21.R;

public class AdivinaLogrosMayores extends Fragment {
    //Instanciamos cada objeto de la clase privada ListView y FirebaseListAdapter
    private ListView listView;
    private FirebaseListAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_adivina_logros_mayores, container, false);
        // a la referencia de listView lo enlazamos con su id
        listView = (ListView) view.findViewById(R.id.LVMostrarNotasAdivinaMayoresFragmento);
        //obtenemos el nombre de la tabla de donde queremos extraer los datos
        final DatabaseReference query = FirebaseDatabase.getInstance().getReference().child("ADIVINA_LOGROS_MAYORES");
        // creamos una instancia "FirebaseListOptions" que se implementa en una lista "logrosMayoresFirebaseListOptions"
        // a esa lista le pasamos una clase que son todos los items que contiene la tabla "LogrosAdivinaMayor"
        // todos esos datos se guardará en un XML "logros_adivina_7_mayor" después le pasamos la referencia
        // de la base de datos mas la clase "logros_adivina_7_mayor"
        final FirebaseListOptions<LogrosAdivinaNotasMayores> logrosAdivinaNotasMayoresFirebaseListOptions = new FirebaseListOptions.Builder<LogrosAdivinaNotasMayores>()
                .setLayout(R.layout.logros_adivina_mayores)
                .setQuery(query,LogrosAdivinaNotasMayores.class).build();
        // inicializamos el "FirebaseListAdapter" y le pasamos la instancia "FirebaseListOptions"
        adapter = new FirebaseListAdapter(logrosAdivinaNotasMayoresFirebaseListOptions) {
            //Inicializamos todos los campos que estén en el XML y le pasamos la vista "v" y por último se setea los campos en el layout.

            @Override
            protected void populateView(View v, Object model, int position) {
                TextInputEditText AdoMayor = (TextInputEditText) v.findViewById(R.id.DoMayorAdivina);
                TextInputEditText AreMayor= (TextInputEditText) v.findViewById(R.id.ReMayorAdivina);
                TextInputEditText AmiMayor = (TextInputEditText) v.findViewById(R.id.MiMayorAdivina);
                TextInputEditText AfaMayor = (TextInputEditText) v.findViewById(R.id.FaMayorAdivina);
                TextInputEditText AsolMayor = (TextInputEditText) v.findViewById(R.id.SolMayorAdivina);
                TextInputEditText AlaMayor= (TextInputEditText) v.findViewById(R.id.LaMayorAdivina);
                TextInputEditText AsiMayor = (TextInputEditText) v.findViewById(R.id.SiMayorAdivina);
                TextInputEditText AnombresUMayor = (TextInputEditText) v.findViewById(R.id.NombreUserAdivinaNotasMayor);

                LogrosAdivinaNotasMayores logrosAdivinaMayor = (LogrosAdivinaNotasMayores) model;
                AdoMayor.setText(logrosAdivinaMayor.getADoMayor());
                AreMayor.setText(logrosAdivinaMayor.getAReMayor());
                AmiMayor.setText(logrosAdivinaMayor.getAMiMayor());
                AfaMayor.setText(logrosAdivinaMayor.getAFaMayor());
                AsolMayor.setText(logrosAdivinaMayor.getASolMayor());
                AlaMayor.setText(logrosAdivinaMayor.getALaMayor());
                AsiMayor.setText(logrosAdivinaMayor.getASiMayor());
                AnombresUMayor.setText(logrosAdivinaMayor.getANombreMayor());


            }
        };

        listView.setAdapter(adapter);
        return view;
    }

    //ciclo de vida de la aplicación, cuando se inicia la aplicación entonces se inicia el adapter
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();

    }
    //ciclo de vida de la aplicación, cuando se finaliza la aplicación entonces se finaliza  el adapter
    // para que si la aplicación está en segundo plano no consuma recursos
    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}

