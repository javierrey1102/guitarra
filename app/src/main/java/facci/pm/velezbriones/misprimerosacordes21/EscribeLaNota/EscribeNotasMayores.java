package facci.pm.velezbriones.misprimerosacordes21.EscribeLaNota;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.UUID;

import facci.pm.velezbriones.misprimerosacordes21.Clases.Sesion;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosEscribesNotasMayores;
import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EscribeNotasMayores extends AppCompatActivity implements View.OnClickListener {
    // Instanciamos cada objeto a la clase privada ImageView, EditText, Button y MediaPlayer.
    private ImageView sonidoMayorUno, sonidoMayorDos, sonidoMayorTres, sonidoMayorCuatro, sonidoMayorCinco, sonidoMayorSeis;
    private EditText respuestaMayorUno, respuestaMayorDos, respuestaMayorTres, respuestaMayorCuatro, respuestaMayorCinco, respuestaMayorSeis;
    private LinearLayout comprobarMUno, comprobarMDos, comprobarMTres, comprobarMCuatro, comprobarMCinco, comprobarMSeis;
    private LinearLayout correctoUnoMayor, correctoDosMayor, correctoTresMayor,correctoCuatroMayor, correctoCincoMayor, correctoSeisMayor;
    private MediaPlayer MPsonidoMuno, MPsonidoMdos, MPsonidoMtres, MpsonidoMcuatro, MPsonidoMcinco, MPsonidoMseis;

    /*Creamos instancias de tipo String y las inicializamos en blanco, esto se hace para obtener todos esos campos de la base de  datos*/
    private String domayor = "";
    private String remayor = "";
    private String mimayor = "";
    private String solmayor = "";
    private String lamayor = "";
    private String simayor = "";
    private int contador;

    //Todo lo que tenga que ver con firebase :v
    private ProgressDialog progressDialog;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference logros_Notas_Mayores, users;
    private FirebaseAuth.AuthStateListener authStateListener;
    private String userId;

    //local data
    private Sesion sesion;
    private HashMap<String, String> user;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        setContentView(R.layout.activity_escribe_notas_mayores);

        sesion = new Sesion(this);
        user = sesion.LoggedInUser();

        // A cada referecia de ImageView lo enlazamos con su respectivo id.
        sonidoMayorUno = (ImageView)findViewById(R.id.SonidoMayorUno);
        sonidoMayorDos = (ImageView)findViewById(R.id.SonidoMayorDos);
        sonidoMayorTres = (ImageView)findViewById(R.id.SonidoMayorTres);
        sonidoMayorCuatro = (ImageView)findViewById(R.id.SonidoMayorCuatro);
        sonidoMayorCinco = (ImageView)findViewById(R.id.SonidoMayorCinco);
        sonidoMayorSeis = (ImageView)findViewById(R.id.SonidoMayorSeis);

        //  A cada referencia de MediaPlayer lo enlazamos con su respectivo sonido
        MPsonidoMuno = MediaPlayer.create(this, R.raw.domayor);
        MPsonidoMdos = MediaPlayer.create(this, R.raw.remayor);
        MPsonidoMtres = MediaPlayer.create(this, R.raw.mimayor);
        MpsonidoMcuatro = MediaPlayer.create(this, R.raw.solmayor);
        MPsonidoMcinco = MediaPlayer.create(this, R.raw.lamayor);
        MPsonidoMseis = MediaPlayer.create(this, R.raw.simayor);

        // A cada referencia de los EditText lo enlazamos con su id.
        respuestaMayorUno = (EditText)findViewById(R.id.TXTRespuestaMayorUno);
        respuestaMayorDos = (EditText)findViewById(R.id.TXTRespuestaMayorDos);
        respuestaMayorTres = (EditText)findViewById(R.id.TXTRespuestaMayorTres);
        respuestaMayorCuatro = (EditText)findViewById(R.id.TXTRespuestaMayorCuatro);
        respuestaMayorCinco = (EditText)findViewById(R.id.TXTRespuestaMayorCinco);
        respuestaMayorSeis = (EditText)findViewById(R.id.TXTRespuestaMayorSeis);

        // A cada referencia de los botones lo enlazamos con su id.
        comprobarMUno = (LinearLayout) findViewById(R.id.BTNComprobarMayorUno);
        comprobarMDos = (LinearLayout) findViewById(R.id.BTNComprobarMayorDos);
        comprobarMTres = (LinearLayout) findViewById(R.id.BTNComprobarMayorTres);
        comprobarMCuatro = (LinearLayout) findViewById(R.id.BTNComprobarMayorCuatro);
        comprobarMCinco = (LinearLayout) findViewById(R.id.BTNComprobarMayorCinco);
        comprobarMSeis = (LinearLayout) findViewById(R.id.BTNComprobarMayorSeis);


        correctoUnoMayor = (LinearLayout)findViewById(R.id.LLUnoMayor);
        correctoDosMayor = (LinearLayout)findViewById(R.id.LLDosMayor);
        correctoTresMayor = (LinearLayout)findViewById(R.id.LLTresMayor);
        correctoCuatroMayor = (LinearLayout)findViewById(R.id.LLCuatroMayor);
        correctoCincoMayor = (LinearLayout)findViewById(R.id.LLCincoMayor);
        correctoSeisMayor = (LinearLayout)findViewById(R.id.LLSeisMayor);

        //Dentro de este método OnCreate asociamos el lístener a los botones.
        sonidoMayorUno.setOnClickListener(this);
        sonidoMayorDos.setOnClickListener(this);
        sonidoMayorTres.setOnClickListener(this);
        sonidoMayorCuatro.setOnClickListener(this);
        sonidoMayorCinco.setOnClickListener(this);
        sonidoMayorSeis.setOnClickListener(this);

        //Al método OnCreate le asociamos el lístener a los botones.
        comprobarMUno.setOnClickListener(this);
        comprobarMDos.setOnClickListener(this);
        comprobarMTres.setOnClickListener(this);
        comprobarMCuatro.setOnClickListener(this);
        comprobarMCinco.setOnClickListener(this);
        comprobarMSeis.setOnClickListener(this);
        // firebase

        /* Obtenemos la instancia para acceder a la base de datos "firebase"*/
        firebaseDatabase = FirebaseDatabase.getInstance();

        /* Obtenemos instancia para autenticar el usuario*/
        firebaseAuth = FirebaseAuth.getInstance();

        /* Obtenemos instancia y referencias del almacenamiento de firebase */
        /* Obetenemos de la base de datos el usuario actual. */

        /* Le damos el nombre a la tabla que guardará los datos en Firebase*/
        logros_Notas_Mayores = firebaseDatabase.getReference("ESCRIBE_LOGROS_MAYORES");



    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            /* Se reproduce el sonido del primer botón */
            case R.id.SonidoMayorUno:
                MPsonidoMuno.start();
                break;

            case R.id.SonidoMayorDos:
                /* Se reproduce el sonido del primer botón */
                MPsonidoMdos.start();
                break;

            case R.id.SonidoMayorTres:
                /* Se reproduce el sonido del primer botón */
                MPsonidoMtres.start();
                break;

            case R.id.SonidoMayorCuatro:
                /* Se reproduce el sonido del primer botón */
                MpsonidoMcuatro.start();
                break;

            case R.id.SonidoMayorCinco:
                /* Se reproduce el sonido del primer botón */
                MPsonidoMcinco.start();
                break;

            case R.id.SonidoMayorSeis:
                /* Se reproduce el sonido del primer botón */
                MPsonidoMseis.start();
                break;


            case R.id.BTNComprobarMayorUno:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaMayorUno.getText().toString().isEmpty()) {
                    respuestaMayorUno.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaMayorUno.getText().toString().equals(getString(R.string.domayor)+" ") ||
                        respuestaMayorUno.getText().toString().equals(getString(R.string.domayor)) ||
                        respuestaMayorUno.getText().toString().equals(getString(R.string.domayorLPMa)+ " ") ||
                        respuestaMayorUno.getText().toString().equals(getString(R.string.domayorLPMa))||
                        respuestaMayorUno.getText().toString().equals(getString(R.string.DdomayorLPMa)+ " ") ||
                        respuestaMayorUno.getText().toString().equals(getString(R.string.DdomayorLPMa))){



                    if (domayor==""){
                        domayor = "CORRECTO: "+respuestaMayorUno.getText().toString();
                    }else {
                        domayor +=  " - " + "CORRECTO: "+respuestaMayorUno.getText().toString();
                    }
                    comprobarMUno.setEnabled(false);
                    comprobarMUno.setBackgroundResource(R.color.botoncolor);
                    correctoUnoMayor.setBackgroundResource(R.color.botoncolor);
                    sonidoMayorUno.setImageResource(R.drawable.correct_note);

                    respuestaMayorUno.setText("CORRECTO");
                    respuestaMayorUno.setEnabled(false);

                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */

                    if (domayor==""){
                        domayor = "INCORRECTO: "+ respuestaMayorUno.getText().toString();
                    }else {
                        domayor +=  " - " + "INCORRECTO: "+respuestaMayorUno.getText().toString();
                    }

                    InCorrecto();
                    respuestaMayorUno.setText("");
                }
                break;

            case R.id.BTNComprobarMayorDos:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaMayorDos.getText().toString().isEmpty()) {
                    respuestaMayorDos.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaMayorDos.getText().toString().equals(getString(R.string.remayor)+" ") ||
                        respuestaMayorDos.getText().toString().equals(getString(R.string.remayor)) ||
                        respuestaMayorDos.getText().toString().equals(getString(R.string.remayorMPMa)+ " ") ||
                        respuestaMayorDos.getText().toString().equals(getString(R.string.remayorMPMa))||
                        respuestaMayorDos.getText().toString().equals(getString(R.string.RremayorMPMa)+ " ") ||
                        respuestaMayorDos.getText().toString().equals(getString(R.string.RremayorMPMa))){


                    if (remayor==""){
                        remayor = "CORRECTO: "+respuestaMayorDos.getText().toString();
                    }else {
                        remayor +=  " - " + "CORRECTO: "+respuestaMayorDos.getText().toString();
                    }
                    comprobarMDos.setBackgroundResource(R.color.botoncolor);
                    correctoDosMayor.setBackgroundResource(R.color.botoncolor);
                    sonidoMayorDos.setImageResource(R.drawable.correct_note);
                    comprobarMDos.setEnabled(false);
                    respuestaMayorDos.setText("CORRECTO");
                    respuestaMayorDos.setEnabled(false);



                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */

                    if (remayor==""){
                        remayor = "INCORRECTO: "+ respuestaMayorDos.getText().toString();
                    }else {
                        remayor +=  " - " + "INCORRECTO: "+respuestaMayorDos.getText().toString();
                    }


                    InCorrecto();
                    respuestaMayorDos.setText("");
                }
                break;

            case R.id.BTNComprobarMayorTres:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaMayorTres.getText().toString().isEmpty()) {
                    respuestaMayorTres.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaMayorTres.getText().toString().equals(getString(R.string.mimayor)+" ")  ||
                        respuestaMayorTres.getText().toString().equals(getString(R.string.mimayorLPMa)+ " ") ||
                        respuestaMayorTres.getText().toString().equals(getString(R.string.mimayorLPMa))||
                        respuestaMayorTres.getText().toString().equals(getString(R.string.MmimayorLPMa)+ " ") ||
                        respuestaMayorTres.getText().toString().equals(getString(R.string.MmimayorLPMa))){


                    if (mimayor==""){
                        mimayor = "CORRECTO: "+respuestaMayorTres.getText().toString();
                    }else {
                        mimayor +=  " - " + "CORRECTO: "+respuestaMayorTres.getText().toString();
                    }
                    comprobarMTres.setBackgroundResource(R.color.botoncolor);
                    correctoTresMayor.setBackgroundResource(R.color.botoncolor);
                    sonidoMayorTres.setImageResource(R.drawable.correct_note);
                    comprobarMTres.setEnabled(false);
                    respuestaMayorTres.setText("CORRECTO");
                    respuestaMayorTres.setEnabled(false);



                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (mimayor==""){
                        mimayor = "INCORRECTO: "+ respuestaMayorTres.getText().toString();
                    }else {
                        mimayor +=  " - " + "INCORRECTO: "+respuestaMayorTres.getText().toString();
                    }


                    InCorrecto();
                    respuestaMayorTres.setText("");
                }
                break;

            case R.id.BTNComprobarMayorCuatro:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaMayorCuatro.getText().toString().isEmpty()) {
                    respuestaMayorCuatro.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaMayorCuatro.getText().toString().equals(getString(R.string.solmayor)+" ")  ||
                        respuestaMayorCuatro.getText().toString().equals(getString(R.string.solmayorLPMa)+ " ") ||
                        respuestaMayorCuatro.getText().toString().equals(getString(R.string.solmayorLPMa))||
                        respuestaMayorCuatro.getText().toString().equals(getString(R.string.SsolmayorLPMa)+ " ") ||
                        respuestaMayorCuatro.getText().toString().equals(getString(R.string.SsolmayorLPMa))){


                    if (solmayor==""){
                        solmayor = "CORRECTO: "+respuestaMayorCuatro.getText().toString();
                    }else {
                        solmayor +=  " - " + "CORRECTO: "+respuestaMayorCuatro.getText().toString();
                    }
                    comprobarMCuatro.setBackgroundResource(R.color.botoncolor);
                    correctoCuatroMayor.setBackgroundResource(R.color.botoncolor);
                    sonidoMayorCuatro.setImageResource(R.drawable.correct_note);
                    comprobarMCuatro.setEnabled(false);
                    respuestaMayorCuatro.setText("CORRECTO");
                    respuestaMayorCuatro.setEnabled(false);


                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */

                    if (solmayor==""){
                        solmayor = "INCORRECTO: "+ respuestaMayorCuatro.getText().toString();
                    }else {
                        solmayor +=  " - " + "INCORRECTO: "+respuestaMayorCuatro.getText().toString();
                    }

                    InCorrecto();
                    respuestaMayorCuatro.setText("");
                }
                break;

            case R.id.BTNComprobarMayorCinco:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaMayorCinco.getText().toString().isEmpty()) {
                    respuestaMayorCinco.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaMayorCinco.getText().toString().equals(getString(R.string.lamayor)+" ")  ||
                        respuestaMayorCinco.getText().toString().equals(getString(R.string.lamayorLPMa)+ " ") ||
                        respuestaMayorCinco.getText().toString().equals(getString(R.string.lamayorLPMa))||
                        respuestaMayorCinco.getText().toString().equals(getString(R.string.LlamayorLPMa)+ " ") ||
                        respuestaMayorCinco.getText().toString().equals(getString(R.string.LlamayorLPMa))){


                    if (lamayor==""){
                        lamayor = "CORRECTO: "+respuestaMayorCinco.getText().toString();
                    }else {
                        lamayor +=  " - " + "CORRECTO: "+respuestaMayorCinco.getText().toString();
                    }
                    comprobarMCinco.setBackgroundResource(R.color.botoncolor);
                    correctoCincoMayor.setBackgroundResource(R.color.botoncolor);
                    sonidoMayorCinco.setImageResource(R.drawable.correct_note);
                    comprobarMCinco.setEnabled(false);
                    respuestaMayorCinco.setText("CORRECTO");
                    respuestaMayorCinco.setEnabled(false);


                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */

                    if (lamayor==""){
                        lamayor = "INCORRECTO: "+ respuestaMayorCinco.getText().toString();
                    }else {
                        lamayor +=  " - " + "INCORRECTO: "+respuestaMayorCinco.getText().toString();
                    }

                    InCorrecto();
                    respuestaMayorCinco.setText("");
                }
                break;

            case R.id.BTNComprobarMayorSeis:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaMayorSeis.getText().toString().isEmpty()) {
                    respuestaMayorSeis.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaMayorSeis.getText().toString().equals(getString(R.string.simayor)+" ")  ||
                        respuestaMayorSeis.getText().toString().equals(getString(R.string.simayorLPMa)+ " ") ||
                        respuestaMayorSeis.getText().toString().equals(getString(R.string.simayorLPMa))||
                        respuestaMayorSeis.getText().toString().equals(getString(R.string.SsimayorLPMa)+ " ") ||
                        respuestaMayorSeis.getText().toString().equals(getString(R.string.SsimayorLPMa))){


                    if (simayor==""){
                        simayor = "CORRECTO: "+respuestaMayorSeis.getText().toString();
                    }else {
                        simayor +=  " - " + "CORRECTO: "+respuestaMayorSeis.getText().toString();
                    }
                    comprobarMSeis.setBackgroundResource(R.color.botoncolor);
                    correctoSeisMayor.setBackgroundResource(R.color.botoncolor);
                    sonidoMayorSeis.setImageResource(R.drawable.correct_note);
                    comprobarMSeis.setEnabled(false);
                    respuestaMayorSeis.setText("CORRECTO");
                    respuestaMayorSeis.setEnabled(false);



                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */

                    if (simayor==""){
                        simayor = "INCORRECTO: "+ respuestaMayorSeis.getText().toString();
                    }else {
                        lamayor +=  " - " + "INCORRECTO: "+respuestaMayorSeis.getText().toString();
                    }
                    InCorrecto();
                    respuestaMayorSeis.setText("");
                }
                break;
        }
    }
    /* Método ""InCorreto para mostrar que está incorrecto, es llama cada vez que no se cumplen las condiciones en los botones*/
    private void InCorrecto(){

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.incorrecto));
        imageView.setImageResource(R.drawable.ic_marca_de_cancelar);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
        contador++;
    }

    /* Método "Correto" para mostrar que está correcto, es llama cada vez que se cumplen las condiciones en los botones*/
    private void Correcto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.correcto));
        imageView.setImageResource(R.drawable.ic_correcto_simbolo);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    private void GuardarRecordNotasMayores() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("RECORD_NOTAS_MAYORES", contador);
        editor.apply();
    }

    private void GuardarCorrectoMayor() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if ((respuestaMayorUno.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaMayorDos.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaMayorTres.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaMayorCuatro.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaMayorCinco.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaMayorSeis.getText().toString().equals(getString(R.string.correcto)))){
            editor.putString("RECORD_MAYOR", "SI");
        }else {
            editor.putString("RECORD_MAYOR", "NO");
        }
        editor.apply();
    }


    /*Método para bloquear el botón BACK*/
    @Override
    public void onBackPressed(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View guardar = layoutInflater.inflate(R.layout.guardar_notas_menores, null);
        final CardView Guardar = (CardView)guardar.findViewById(R.id.CardGuardar);
        final CardView Cancelar = (CardView)guardar.findViewById(R.id.CardCancelar);

        Guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarRecordNotasMayores();
                GuardarCorrectoMayor();

                LogrosEscribesNotasMayores LENMayores = new LogrosEscribesNotasMayores();
                LENMayores.setEDoMayor(domayor);
                LENMayores.setEReMayor(remayor);
                LENMayores.setEMiMayor(mimayor);
                LENMayores.setESolMayor(solmayor);
                LENMayores.setELaMayor(lamayor);
                LENMayores.setESiMayor(simayor);


                if (isOnlineNet()==true) {
                    /*Se guarda el contenido de esta actividad en nuestra base de datos Firebase, le especificamos la ruta en donde debe guardarse los datos*/
                    final String id = user.get(sesion.USER_ID);
                    logros_Notas_Mayores.child(id).child("ESCRIBE_LOGROS_MAYORES - " + id).child(UUID.randomUUID().toString()).setValue(LENMayores).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            finish();
                            Toast.makeText(EscribeNotasMayores.this, "Datos guardados con éxitos", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(EscribeNotasMayores.this, EscribeLaNota.class));
                        }
                    }).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                        }
                    });

                }else
                Toast.makeText(EscribeNotasMayores.this, R.string.sinaccesoainternet, Toast.LENGTH_SHORT).show();


            }
        });

        Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        builder.setView(guardar);
        builder.show();


    }
    //metodo de tipo booleano para comprobar la conexion a internet
    public Boolean isOnlineNet() {
        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");
            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
}
