package facci.pm.velezbriones.misprimerosacordes21.Modelo;

public class LogrosEscribeNotasMenores7 {
private   String EDoMenor7, EReMenor7, EMiMenor7, EFaMenor7, ESolMenor7, ELaMenor7, ESiMenor7, ENombreMenor7;

    public LogrosEscribeNotasMenores7(){

    }

    public String getEDoMenor7() {
        return EDoMenor7;
    }

    public void setEDoMenor7(String EDoMenor7) {
        this.EDoMenor7 = EDoMenor7;
    }

    public String getEReMenor7() {
        return EReMenor7;
    }

    public void setEReMenor7(String EReMenor7) {
        this.EReMenor7 = EReMenor7;
    }

    public String getEMiMenor7() {
        return EMiMenor7;
    }

    public void setEMiMenor7(String EMiMenor7) {
        this.EMiMenor7 = EMiMenor7;
    }

    public String getEFaMenor7() {
        return EFaMenor7;
    }

    public void setEFaMenor7(String EFaMenor7) {
        this.EFaMenor7 = EFaMenor7;
    }

    public String getESolMenor7() {
        return ESolMenor7;
    }

    public void setESolMenor7(String ESolMenor7) {
        this.ESolMenor7 = ESolMenor7;
    }

    public String getELaMenor7() {
        return ELaMenor7;
    }

    public void setELaMenor7(String ELaMenor7) {
        this.ELaMenor7 = ELaMenor7;
    }

    public String getESiMenor7() {
        return ESiMenor7;
    }

    public void setESiMenor7(String ESiMenor7) {
        this.ESiMenor7 = ESiMenor7;
    }

    public String getENombreMenor7() {
        return ENombreMenor7;
    }

    public void setENombreMenor7(String ENombreMenor7) {
        this.ENombreMenor7 = ENombreMenor7;
    }
}
