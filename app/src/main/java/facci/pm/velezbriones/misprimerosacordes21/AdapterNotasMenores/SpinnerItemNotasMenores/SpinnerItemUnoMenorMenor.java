package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.SpinnerItemNotasMenores;

public class SpinnerItemUnoMenorMenor {
    private String NombreItemUNoMenor;
    private int ImagenItemUnoMenor;
    //Creamos el constructor y le pasamos dos parámetros (string e int) similar a los de arriba para no perder la referencia pero no son iguales.
    public SpinnerItemUnoMenorMenor (String nombreItemUNoMenor, int imagenItemUnoMenor){
        this.NombreItemUNoMenor = nombreItemUNoMenor;
        this.ImagenItemUnoMenor = imagenItemUnoMenor;
    }
    //generamos el get de de las instancia del tipo "string" y me retorna el String

    public String getNombreItemUNoMenor() {
        return NombreItemUNoMenor;
    }

    public int getImagenItemUnoMenor() {
        return ImagenItemUnoMenor;
    }
}
