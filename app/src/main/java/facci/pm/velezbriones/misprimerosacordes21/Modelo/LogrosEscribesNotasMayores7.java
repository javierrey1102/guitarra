package facci.pm.velezbriones.misprimerosacordes21.Modelo;

public class LogrosEscribesNotasMayores7 {
    private String EDoMayor7, EReMayor7, EMiMayor7, ESolMayor7, ELaMayor7, ESiMayor7, ENombreMayor7;
    public LogrosEscribesNotasMayores7(){

    }

    public String getEDoMayor7() {
        return EDoMayor7;
    }

    public void setEDoMayor7(String EDoMayor7) {
        this.EDoMayor7 = EDoMayor7;
    }

    public String getEReMayor7() {
        return EReMayor7;
    }

    public void setEReMayor7(String EReMayor7) {
        this.EReMayor7 = EReMayor7;
    }

    public String getEMiMayor7() {
        return EMiMayor7;
    }

    public void setEMiMayor7(String EMiMayor7) {
        this.EMiMayor7 = EMiMayor7;
    }

    public String getESolMayor7() {
        return ESolMayor7;
    }

    public void setESolMayor7(String ESolMayor7) {
        this.ESolMayor7 = ESolMayor7;
    }

    public String getELaMayor7() {
        return ELaMayor7;
    }

    public void setELaMayor7(String ELaMayor7) {
        this.ELaMayor7 = ELaMayor7;
    }

    public String getESiMayor7() {
        return ESiMayor7;
    }

    public void setESiMayor7(String ESiMayor7) {
        this.ESiMayor7 = ESiMayor7;
    }

    public String getENombreMayor7() {
        return ENombreMayor7;
    }

    public void setENombreMayor7(String ENombreMayor7) {
        this.ENombreMayor7 = ENombreMayor7;
    }
}
