package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7;

public class SpinnerItenTresMenor {
    //hacemos dos instancia de tipo string y entero
    private String NombreItemMenor;
    private int ImagenItemMenor;

    //Creamos el constructor y le pasamos dos parámetros (string e int) similar a los de arriba para no perder la referencia pero no son iguales.
    public SpinnerItenTresMenor (String NombreItemMenor, int ImagenItemMenor){
        this.NombreItemMenor = NombreItemMenor;
        this.ImagenItemMenor = ImagenItemMenor;
    }

    //generamos el get de de las instancia del tipo "string" y me retorna el String
    public String getNombreItemTresMenor() {
        return NombreItemMenor;
    }

    public int getImagenItemTresMenor() {
        return ImagenItemMenor;
    }
}
