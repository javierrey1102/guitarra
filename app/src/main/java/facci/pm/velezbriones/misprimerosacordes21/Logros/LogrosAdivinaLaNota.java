package facci.pm.velezbriones.misprimerosacordes21.Logros;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationItem;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationView;
import com.luseen.luseenbottomnavigation.BottomNavigation.OnBottomNavigationItemClickListener;

import facci.pm.velezbriones.misprimerosacordes21.Fragmentos.AdivinaLogrosMayores;
import facci.pm.velezbriones.misprimerosacordes21.Fragmentos.AdivinaLogrosMayoresSiete;
import facci.pm.velezbriones.misprimerosacordes21.Fragmentos.AdivinaLogrosMenores;
import facci.pm.velezbriones.misprimerosacordes21.Fragmentos.AdivinaLogrosMenoresSiete;
import facci.pm.velezbriones.misprimerosacordes21.Fragmentos.EscribesLogrosMayores;
import facci.pm.velezbriones.misprimerosacordes21.Fragmentos.EscribesLogrosMenores;
import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LogrosAdivinaLaNota extends AppCompatActivity {
    //Instanciamos BootommNavigationView como privado
    private BottomNavigationView bottomNavigationView;

    //cambiar
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //de aqui
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        //hasta aqui

        setContentView(R.layout.activity_logros_adivina_la_nota);
    //Obtenemos el id de la referencia "bottomNavigationView"
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigation);

        /*  Creamos un arreglo para los colores */
        int[] image = {R.drawable.ic_guitarrauno, R.drawable.ic_guitarrauno,
                R.drawable.ic_guiatrrados, R.drawable.ic_guiatrrados};
        // Creamos un arreglo para los colores
        int[] color = {ContextCompat.getColor(this, R.color.firstColor), ContextCompat.getColor(this, R.color.secondColor),
                ContextCompat.getColor(this, R.color.thirdColor), ContextCompat.getColor(this, R.color.fourthColor)};


        if (bottomNavigationView != null) {
            bottomNavigationView.isWithText(false);
            bottomNavigationView.isColoredBackground(true);
            bottomNavigationView.setTextActiveSize(getResources().getDimension(R.dimen.text_active));
            bottomNavigationView.setTextInactiveSize(getResources().getDimension(R.dimen.text_inactive));
            bottomNavigationView.setItemActiveColorWithoutColoredBackground(ContextCompat.getColor(this, R.color.firstColor));
        }

        /* Declaramos un conjunto de identificadores dibujables y un conjunto de identificadores de títulos para que coincidan
        con lo que hemos declarado en el archivo XML del menú. Si es verdadero, recorremos los elementos del
        menú y configuramos su icono, título y estado a los valores predeterminados. */
        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorAdivina, new AdivinaLogrosMayores()).commit();
        BottomNavigationItem bottomNavigationItem = new BottomNavigationItem
                ("", color[0], image[0]);
        BottomNavigationItem bottomNavigationItem1 = new BottomNavigationItem
                ("", color[1], image[1]);
        BottomNavigationItem bottomNavigationItem2 = new BottomNavigationItem
                ("", color[2], image[2]);
        BottomNavigationItem bottomNavigationItem3 = new BottomNavigationItem
                ("", color[3], image[3]);


        bottomNavigationView.addTab(bottomNavigationItem);
        bottomNavigationView.addTab(bottomNavigationItem1);
        bottomNavigationView.addTab(bottomNavigationItem2);
        bottomNavigationView.addTab(bottomNavigationItem3);



        /*  Definimos los casos para que ejecuten dependiendo cada uno*/
        bottomNavigationView.setOnBottomNavigationItemClickListener(new OnBottomNavigationItemClickListener() {
            @Override
            public void onNavigationItemClick(int index) {
                switch (index) {
                    case 0:
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorAdivina, new AdivinaLogrosMayores()).commit();
                        break;
                    case 1:
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorAdivina, new AdivinaLogrosMenores()).commit();
                        break;
                    case 2:
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorAdivina, new AdivinaLogrosMayoresSiete()).commit();
                        break;
                    case 3:
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorAdivina, new AdivinaLogrosMenoresSiete()).commit();
                        break;

                }
            }
        });
    }
}