package facci.pm.velezbriones.misprimerosacordes21.EscribeLaNota;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import facci.pm.velezbriones.misprimerosacordes21.Clases.Sesion;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosAdivinaNotasMenores7;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosEscribeNotasMenores7;
import facci.pm.velezbriones.misprimerosacordes21.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EscribeNotasMenores7 extends AppCompatActivity implements View.OnClickListener{

    // Instanciamos cada objeto a la clase privada ImageView, EditText, Button y MediaPlayer.
    private ImageView sonidoUnoMenor7, sonidoDosMenor7, sonidoTresMenor7, sonidoCuatroMenor7, sonidoCincoMenor7, sonidoSeisMenor7;
    private EditText respuestaUnoMenor7, respuestaDosMenor7, respuestaTresMenor7, respuestaCuatroMenor7, respuestaCincoMenor7, respuestaSeisMenor7;
    private LinearLayout comprobarUnoMenor7, comprobarDosMenor7, comprobarTresMenor7, comprobarCuatroMenor7, comprobarCincoMenor7, comprobarSeisMenor7;
    private LinearLayout correctoUnoMenorMenor7, correctoDosMenorMenor7, correctoTresMenorMenor7, correctoCuatroMenorMenor7, correctoCincoMenorMenor7, correctoSeisMenorMenor7;
    private MediaPlayer MPsonidounoMenor7, MPsonidodosMenor7, MPsonidotresMenor7, MpsonidocuatroMenor7, MPsonidocincoMenor7, MPsonidoseisMenor7;


    /*Creamos instancias de tipo String y las inicializamos en blanco*/
    private String domenorMenor7 = "";
    private String remenorMenor7 = "";
    private String mimenorMenor7 = "";
    private String solmenorMenor7 = "";
    private String lamenorMenor7 = "";
    private String simenorMenor7 = "";
    int contadorMenor7;

    //Todo lo que tenga que ver con firebase :v
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference logros_Notas_Menores_7, users;
    private FirebaseAuth.AuthStateListener authStateListener;
    private String userId;

    //local data
    private Sesion sesion;
    private HashMap<String, String> user;



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RoughTherapy.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        setContentView(R.layout.activity_escribe_notas_menores7);

        sesion = new Sesion(this);
        user = sesion.LoggedInUser();


        // A cada referecia de ImageView lo enlazamos con su respectivo id.
        sonidoUnoMenor7 = (ImageView)findViewById(R.id.SonidoMenor7);
        sonidoDosMenor7 = (ImageView)findViewById(R.id.SonidoDosMenor7);
        sonidoTresMenor7 = (ImageView)findViewById(R.id.SonidoTresMenor7);
        sonidoCuatroMenor7 = (ImageView)findViewById(R.id.SonidoCuatroMenor7);
        sonidoCincoMenor7 = (ImageView)findViewById(R.id.SonidoCincoMenor7);
        sonidoSeisMenor7 = (ImageView)findViewById(R.id.SonidoSeisMenor7);

        //  A cada referencia de MediaPlayer lo enlazamos con su respectivo sonido
        MPsonidounoMenor7 = MediaPlayer.create(this, R.raw.domenor7);
        MPsonidodosMenor7 = MediaPlayer.create(this, R.raw.remenor7);
        MPsonidotresMenor7 = MediaPlayer.create(this, R.raw.mimenor7);
        MpsonidocuatroMenor7 = MediaPlayer.create(this, R.raw.solmenor7);
        MPsonidocincoMenor7 = MediaPlayer.create(this, R.raw.lamenor7);
        MPsonidoseisMenor7 = MediaPlayer.create(this, R.raw.simenor7);

        // A cada referencia de los EditText lo enlazamos con su id.
        respuestaUnoMenor7 = (EditText)findViewById(R.id.TXTRespuestaMenor7);
        respuestaDosMenor7 = (EditText)findViewById(R.id.TXTRespuestaDosMenor7);
        respuestaTresMenor7 = (EditText)findViewById(R.id.TXTRespuestaTresMenor7);
        respuestaCuatroMenor7 = (EditText)findViewById(R.id.TXTRespuestaCuatroMenor7);
        respuestaCincoMenor7 = (EditText)findViewById(R.id.TXTRespuestaCincoMenor7);
        respuestaSeisMenor7 = (EditText)findViewById(R.id.TXTRespuestaSeisMenor7);


        // A cada referencia de los botones lo enlazamos con su id.
        comprobarUnoMenor7 = (LinearLayout) findViewById(R.id.BTNComprobarMenorUno7);
        comprobarDosMenor7 = (LinearLayout) findViewById(R.id.BTNComprobarMenorDos7);
        comprobarTresMenor7 = (LinearLayout) findViewById(R.id.BTNComprobarMenorTres7);
        comprobarCuatroMenor7 = (LinearLayout) findViewById(R.id.BTNComprobarMenorCuatro7);
        comprobarCincoMenor7 = (LinearLayout) findViewById(R.id.BTNComprobarMenorCinco7);
        comprobarSeisMenor7 = (LinearLayout) findViewById(R.id.BTNComprobarMenorSeis7);

        correctoUnoMenorMenor7 = (LinearLayout)findViewById(R.id.LLUnoVerdeMenor7);
        correctoDosMenorMenor7 = (LinearLayout)findViewById(R.id.LLDosVerdeMenor7);
        correctoTresMenorMenor7 = (LinearLayout)findViewById(R.id.LLTresVerdeMenor7);
        correctoCuatroMenorMenor7 = (LinearLayout)findViewById(R.id.LLCuatroVerdeMenor7);
        correctoCincoMenorMenor7 = (LinearLayout)findViewById(R.id.LLCincoVerdeMenor7);
        correctoSeisMenorMenor7 = (LinearLayout)findViewById(R.id.LLSeisVerdeMenor7);

        //Dentro de este método OnCreate asociamos el lístener a los botones.
        sonidoUnoMenor7.setOnClickListener(this);
        sonidoDosMenor7.setOnClickListener(this);
        sonidoTresMenor7.setOnClickListener(this);
        sonidoCuatroMenor7.setOnClickListener(this);
        sonidoCincoMenor7.setOnClickListener(this);
        sonidoSeisMenor7.setOnClickListener(this);

        //Al método OnCreate le asociamos el lístener a los botones.
        comprobarUnoMenor7.setOnClickListener(this);
        comprobarDosMenor7.setOnClickListener(this);
        comprobarTresMenor7.setOnClickListener(this);
        comprobarCuatroMenor7.setOnClickListener(this);
        comprobarCincoMenor7.setOnClickListener(this);
        comprobarSeisMenor7.setOnClickListener(this);

        // firebase

        /* Obtenemos la instancia para acceder a la base de datos "firebase"*/
        firebaseDatabase = FirebaseDatabase.getInstance();

        /* Obtenemos instancia para autenticar el usuario*/
        firebaseAuth = FirebaseAuth.getInstance();

        /* Obtenemos instancia y referencias del almacenamiento de firebase */
        /* Obetenemos de la base de datos el usuario actual. */

        /* Le damos el nombre a la tabla que guardará los datos en Firebase*/
        logros_Notas_Menores_7 = firebaseDatabase.getReference("ESCRIBE_LOGROS_MENORES_7");


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            /* Se reproduce el sonido del primer botón */
            case R.id.SonidoMenor7:
                MPsonidounoMenor7.start();
                break;

            case R.id.SonidoDosMenor7:
                /* Se reproduce el sonido del segundo botón */
                MPsonidodosMenor7.start();
                break;

            case R.id.SonidoTresMenor7:
                /* Se reproduce el sonido del tercer botón */
                MPsonidotresMenor7.start();
                break;

            case R.id.SonidoCuatroMenor7:
                /* Se reproduce el sonido del cuarto botón */
                MpsonidocuatroMenor7.start();
                break;

            case R.id.SonidoCincoMenor7:
                /* Se reproduce el sonido del quinto botón */
                MPsonidocincoMenor7.start();
                break;

            case R.id.SonidoSeisMenor7:
                /* Se reproduce el sonido del sexto botón */
                MPsonidoseisMenor7.start();
                break;


            case R.id.BTNComprobarMenorUno7:
                /* Cuando comprobamos y lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaUnoMenor7.getText().toString().isEmpty()) {
                    respuestaUnoMenor7.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                } else if (respuestaUnoMenor7.getText().toString().equals(getString(R.string.domenor7) + " ") ||
                        respuestaUnoMenor7.getText().toString().equals(getString(R.string.domenor7)) ||
                        respuestaUnoMenor7.getText().toString().equals(getString(R.string.domenorLPMe7) + " ") ||
                        respuestaUnoMenor7.getText().toString().equals(getString(R.string.domenorLPMe7)) ||
                        respuestaUnoMenor7.getText().toString().equals(getString(R.string.DdomenorLPMe7) + " ") ||
                        respuestaUnoMenor7.getText().toString().equals(getString(R.string.DdomenorLPMe7))) {


                    if (domenorMenor7 == "") {
                        domenorMenor7 = "CORRECTO: " + respuestaUnoMenor7.getText().toString();
                    } else {
                        domenorMenor7 += " - " + "CORRECTO: " + respuestaUnoMenor7.getText().toString();
                    }
                    comprobarUnoMenor7.setEnabled(false);
                    comprobarUnoMenor7.setBackgroundResource(R.color.botoncolor);
                    correctoUnoMenorMenor7.setBackgroundResource(R.color.botoncolor);
                    sonidoUnoMenor7.setImageResource(R.drawable.correct_note);
                    respuestaUnoMenor7.setText("CORRECTO");
                    respuestaUnoMenor7.setEnabled(false);
                    Correcto();
                } else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (domenorMenor7 == "") {
                        domenorMenor7 = "INCORRECTO: " + respuestaUnoMenor7.getText().toString();
                    } else {
                        domenorMenor7 += " - " + "INCORRECTO: " + respuestaUnoMenor7.getText().toString();
                    }
                    InCorrecto();
                    respuestaUnoMenor7.setText("");
                }
                break;


            case R.id.BTNComprobarMenorDos7:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaDosMenor7.getText().toString().isEmpty()) {
                    respuestaDosMenor7.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaDosMenor7.getText().toString().equals(getString(R.string.remenor7)+" ")  ||
                        respuestaDosMenor7.getText().toString().equals(getString(R.string.remenor7)) ||
                        respuestaDosMenor7.getText().toString().equals(getString(R.string.remenorLPMe7)+ " ") ||
                        respuestaDosMenor7.getText().toString().equals(getString(R.string.remenorLPMe7))||
                        respuestaDosMenor7.getText().toString().equals(getString(R.string.RremenorLPMe7)+ " ") ||
                        respuestaDosMenor7.getText().toString().equals(getString(R.string.RremenorLPMe7))){

                    if (remenorMenor7==""){
                        remenorMenor7= "CORECTO: "+respuestaDosMenor7.getText().toString();
                    }else {
                        remenorMenor7+= " - "+ "CORRECTO: " +respuestaDosMenor7.getText().toString();
                    }

                    correctoDosMenorMenor7.setBackgroundResource(R.color.botoncolor);
                    respuestaDosMenor7.setText("CORRECTO");
                    respuestaDosMenor7.setEnabled(false);
                    comprobarDosMenor7.setBackgroundResource(R.color.botoncolor);
                    sonidoDosMenor7.setImageResource(R.drawable.correct_note);
                    comprobarDosMenor7.setEnabled(false);
                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (remenorMenor7==""){
                        remenorMenor7= "INCORECTO: "+respuestaDosMenor7.getText().toString();
                    }else {
                        remenorMenor7+= " - "+ "INCORRECTO: " +respuestaDosMenor7.getText().toString();
                    }

                    InCorrecto();
                    respuestaDosMenor7.setText("");
                }
                break;

            case R.id.BTNComprobarMenorTres7:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaTresMenor7.getText().toString().isEmpty()) {
                    respuestaTresMenor7.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaTresMenor7.getText().toString().equals(getString(R.string.mimenor7)+" ")||
                        respuestaTresMenor7.getText().toString().equals(getString(R.string.mimenor7)) ||
                        respuestaTresMenor7.getText().toString().equals(getString(R.string.mimenorLPMe7)+ " ") ||
                        respuestaTresMenor7.getText().toString().equals(getString(R.string.mimenorLPMe7))||
                        respuestaTresMenor7.getText().toString().equals(getString(R.string.MmimenorLPMe7)+ " ") ||
                        respuestaTresMenor7.getText().toString().equals(getString(R.string.MmimenorLPMe7))){

                    if (mimenorMenor7==""){
                        mimenorMenor7= "CORECTO: "+respuestaTresMenor7.getText().toString();
                    }else {
                        mimenorMenor7+= " - "+ "CORRECTO: " +respuestaTresMenor7.getText().toString();
                    }

                    correctoTresMenorMenor7.setBackgroundResource(R.color.botoncolor);
                    sonidoTresMenor7.setImageResource(R.drawable.correct_note);
                    respuestaTresMenor7.setText("CORRECTO");
                    respuestaTresMenor7.setEnabled(false);
                    comprobarTresMenor7.setEnabled(false);
                    comprobarTresMenor7.setBackgroundResource(R.color.botoncolor);
                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */

                    if (mimenorMenor7==""){
                        mimenorMenor7= "INCORECTO: "+respuestaTresMenor7.getText().toString();
                    }else {
                        mimenorMenor7+= " - "+ "INCORRECTO: " +respuestaTresMenor7.getText().toString();
                    }

                    InCorrecto();
                    respuestaTresMenor7.setText("");
                }
                break;

            case R.id.BTNComprobarMenorCuatro7:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaCuatroMenor7.getText().toString().isEmpty()) {
                    respuestaCuatroMenor7.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaCuatroMenor7.getText().toString().equals(getString(R.string.solmenor7)+" ")||
                        respuestaCuatroMenor7.getText().toString().equals(getString(R.string.solmenor7)) ||
                        respuestaCuatroMenor7.getText().toString().equals(getString(R.string.solmenorLPMe7)+ " ") ||
                        respuestaCuatroMenor7.getText().toString().equals(getString(R.string.solmenorLPMe7))||
                        respuestaCuatroMenor7.getText().toString().equals(getString(R.string.SsolmenorLPMe7)+ " ") ||
                        respuestaCuatroMenor7.getText().toString().equals(getString(R.string.SsolmenorLPMe7))){

                    if (solmenorMenor7==""){
                        solmenorMenor7= "CORECTO: "+respuestaCuatroMenor7.getText().toString();
                    }else {
                        solmenorMenor7+= " - "+ "CORRECTO: " +respuestaCuatroMenor7.getText().toString();
                    }

                    correctoCuatroMenorMenor7.setBackgroundResource(R.color.botoncolor);
                    sonidoCuatroMenor7.setImageResource(R.drawable.correct_note);
                    respuestaCuatroMenor7.setText("CORRECTO");
                    respuestaCuatroMenor7.setEnabled(false);
                    comprobarCuatroMenor7.setEnabled(false);
                    comprobarCuatroMenor7.setBackgroundResource(R.color.botoncolor);
                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (solmenorMenor7==""){
                        solmenorMenor7= "INCORECTO: "+respuestaCuatroMenor7.getText().toString();
                    }else {
                        solmenorMenor7+= " - "+ "INCORRECTO: " +respuestaCuatroMenor7.getText().toString();
                    }


                    InCorrecto();
                    respuestaCuatroMenor7.setText("");
                }
                break;

            case R.id.BTNComprobarMenorCinco7:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaCincoMenor7.getText().toString().isEmpty()) {
                    respuestaCincoMenor7.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaCincoMenor7.getText().toString().equals(getString(R.string.lamenor7)+" ") ||
                        respuestaCincoMenor7.getText().toString().equals(getString(R.string.lamenor7)) ||
                        respuestaCincoMenor7.getText().toString().equals(getString(R.string.lamenorLPMe7)+ " ") ||
                        respuestaCincoMenor7.getText().toString().equals(getString(R.string.lamenorLPMe7))||
                        respuestaCincoMenor7.getText().toString().equals(getString(R.string.LlamenorLPMe7)+ " ") ||
                        respuestaCincoMenor7.getText().toString().equals(getString(R.string.LlamenorLPMe7))){


                    if (lamenorMenor7==""){
                        lamenorMenor7= "CORECTO: "+respuestaCincoMenor7.getText().toString();
                    }else {
                        lamenorMenor7+= " - "+ "CORRECTO: " +respuestaCincoMenor7.getText().toString();
                    }

                    correctoCincoMenorMenor7.setBackgroundResource(R.color.botoncolor);
                    sonidoCincoMenor7.setImageResource(R.drawable.correct_note);
                    respuestaCincoMenor7.setText("CORRECTO");
                    respuestaCincoMenor7.setEnabled(false);
                    comprobarCincoMenor7.setEnabled(false);
                    comprobarCincoMenor7.setBackgroundResource(R.color.botoncolor);

                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (lamenorMenor7==""){
                        lamenorMenor7= "INCORECTO: "+respuestaCincoMenor7.getText().toString();
                    }else {
                        lamenorMenor7+= " - "+ "INCORRECTO: " +respuestaCincoMenor7.getText().toString();
                    }

                    InCorrecto();
                    respuestaCincoMenor7.setText("");
                }
                break;

            case R.id.BTNComprobarMenorSeis7:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaSeisMenor7.getText().toString().isEmpty()) {
                    respuestaSeisMenor7.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaSeisMenor7.getText().toString().equals(getString(R.string.simenor7)+" ") ||
                        respuestaSeisMenor7.getText().toString().equals(getString(R.string.simenor7)) ||
                        respuestaSeisMenor7.getText().toString().equals(getString(R.string.simenorLPMe7)+ " ") ||
                        respuestaSeisMenor7.getText().toString().equals(getString(R.string.simenorLPMe7))||
                        respuestaSeisMenor7.getText().toString().equals(getString(R.string.SsimenorLPMe7)+ " ") ||
                        respuestaSeisMenor7.getText().toString().equals(getString(R.string.SsimenorLPMe7))){


                    if (simenorMenor7==""){
                        simenorMenor7= "CORECTO: "+respuestaSeisMenor7.getText().toString();
                    }else {
                        simenorMenor7+= " - "+ "CORRECTO: " +respuestaSeisMenor7.getText().toString();
                    }
                    correctoSeisMenorMenor7.setBackgroundResource(R.color.botoncolor);
                    sonidoSeisMenor7.setImageResource(R.drawable.correct_note);
                    respuestaSeisMenor7.setText("CORRECTO");
                    respuestaSeisMenor7.setEnabled(false);
                    comprobarSeisMenor7.setEnabled(false);
                    comprobarSeisMenor7.setBackgroundResource(R.color.botoncolor);

                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (simenorMenor7==""){
                        simenorMenor7= "INCORECTO: "+respuestaSeisMenor7.getText().toString();
                    }else {
                        simenorMenor7+= " - "+ "INCORRECTO: " +respuestaSeisMenor7.getText().toString();
                    }
                    InCorrecto();
                    respuestaSeisMenor7.setText("");
                }
                break;



        }
    }

    /* Método ""InCorreto para mostrar que está incorrecto, es llama cada vez que no se cumplen las condiciones en los botones*/
    private void InCorrecto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.incorrecto));
        imageView.setImageResource(R.drawable.ic_marca_de_cancelar);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
        contadorMenor7++;
    }

    /* Método "Correto" para mostrar que está correcto, es llama cada vez que se cumplen las condiciones en los botones*/
    private void Correcto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.correcto));
        imageView.setImageResource(R.drawable.ic_correcto_simbolo);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    private void GuardarRecordNotasMenores7() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("RECORD_NOTAS_MENORES_7", contadorMenor7);
        editor.apply();
    }

    private void GuardarCorrectoMenor7() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if ((respuestaUnoMenor7.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaDosMenor7.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaTresMenor7.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaCuatroMenor7.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaCincoMenor7.getText().toString().equals(getString(R.string.correcto))) &&
                (respuestaSeisMenor7.getText().toString().equals(getString(R.string.correcto)))){
            editor.putString("RECORD_MENOR_7", "SI");
        }else {
            editor.putString("RECORD_MENOR_7", "NO");
        }
        editor.apply();
    }


    /*Método para bloquear el botón BACK*/
    @Override
    public void onBackPressed(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View guardar = layoutInflater.inflate(R.layout.guardar_notas_menores, null);
        final CardView Guardar = (CardView)guardar.findViewById(R.id.CardGuardar);
        final CardView Cancelar = (CardView)guardar.findViewById(R.id.CardCancelar);

        Guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarRecordNotasMenores7();
                GuardarCorrectoMenor7();

                LogrosEscribeNotasMenores7 LENMenor7 = new LogrosEscribeNotasMenores7();
                LENMenor7.setEDoMenor7(domenorMenor7);
                LENMenor7.setEReMenor7(remenorMenor7);
                LENMenor7.setEMiMenor7(mimenorMenor7);
                LENMenor7.setESolMenor7(solmenorMenor7);
                LENMenor7.setELaMenor7(lamenorMenor7);
                LENMenor7.setESiMenor7(simenorMenor7);

                if (isOnlineNet()==true) {

                    final String id = user.get(sesion.USER_ID);
                    logros_Notas_Menores_7.child(id).setValue(LENMenor7).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            startActivity(new Intent(EscribeNotasMenores7.this, EscribeLaNota.class));
                            Toast.makeText(EscribeNotasMenores7.this, "Datos guardados con éxito", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                        }
                    });

                }else
                    Toast.makeText(EscribeNotasMenores7.this, R.string.sinaccesoainternet, Toast.LENGTH_SHORT).show();

            }
        });

        Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        builder.setView(guardar);
        builder.show();

    }
    //metodo de tipo booleano para comprobar la conexion a internet
    public Boolean isOnlineNet() {
        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");
            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
}