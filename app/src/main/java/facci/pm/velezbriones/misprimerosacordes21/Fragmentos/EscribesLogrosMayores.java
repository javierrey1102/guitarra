package facci.pm.velezbriones.misprimerosacordes21.Fragmentos;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import facci.pm.velezbriones.misprimerosacordes21.Clases.Sesion;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosEscribesNotasMayores;
import facci.pm.velezbriones.misprimerosacordes21.R;

public class EscribesLogrosMayores extends Fragment {
    //Instanciamos cada objeto de la clase privada ListView y FirebaseListAdapter
    private ListView listView;
    private FirebaseListAdapter adapter;

    private Sesion sesion;
    private HashMap<String, String> user;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //al fragmento le hacemos una vista
        View view = inflater.inflate(R.layout.fragment_escribes_logros_mayores, container, false);
        // a la referencia de listView lo enlazamos con su id
        listView = (ListView)view.findViewById(R.id.LVMostrarNotasMayorFragmento);

        sesion = new Sesion(getActivity());
        user = sesion.LoggedInUser();
        final String id = user.get(sesion.USER_ID);
        final String referencia = "ESCRIBE_LOGROS_MAYORES";


        //obtenemos el nombre de la tabla de donde queremos extraer los datos
        final DatabaseReference query = FirebaseDatabase.getInstance().getReference().child(referencia).child(id).child(referencia + " - " + id);
        // creamos una instancia "FirebaseListOptions" que se implementa en una lista "logrosMayoresFirebaseListOptions"
        // a esa lista le pasamos una clase que son todos los items que contiene la tabla "LogrosMayores"
        // todos esos datos se guardará en un XML "logrosmayores" después le pasamos la referencia de la base de datos mas la clase "logrosmayores"
        final FirebaseListOptions<LogrosEscribesNotasMayores> logrosMayoresFirebaseListOptions =
                new FirebaseListOptions.Builder<LogrosEscribesNotasMayores>()
                .setLayout(R.layout.logros_mayores)
                .setQuery(query, LogrosEscribesNotasMayores.class).build();
        // inicializamos el "FirebaseListAdapter" y le pasamos la instancia "FirebaseListOptions"
        adapter = new FirebaseListAdapter(logrosMayoresFirebaseListOptions) {
            //Inicializamos todos los campos que estén en el XML y le pasamos la vista "v" y por último se setea los campos en el layout.
            @Override
            protected void populateView(View v, Object model, int position) {

                TextInputEditText doMayorR = (TextInputEditText)v.findViewById(R.id.DoMayorR);
                TextInputEditText reMayorR = (TextInputEditText)v.findViewById(R.id.ReMayorR);
                TextInputEditText miMayorR = (TextInputEditText)v.findViewById(R.id.MiMayorR);
                TextInputEditText solMayorR = (TextInputEditText)v.findViewById(R.id.SolMayorR);
                TextInputEditText laMayorR = (TextInputEditText)v.findViewById(R.id.LaMayorR);
                TextInputEditText siMayorR = (TextInputEditText)v.findViewById(R.id.SiMayorR);
                TextInputEditText nombresU = (TextInputEditText)v.findViewById(R.id.NombreUser);

                LogrosEscribesNotasMayores logrosMayores = (LogrosEscribesNotasMayores) model;
                doMayorR.setText(logrosMayores.getEDoMayor());
                reMayorR.setText(logrosMayores.getEReMayor());
                miMayorR.setText(logrosMayores.getEMiMayor());
                solMayorR.setText(logrosMayores.getESolMayor());
                laMayorR.setText(logrosMayores.getELaMayor());
                siMayorR.setText(logrosMayores.getESiMayor());
                nombresU.setText(logrosMayores.getENombreMayor());

            }
        };
        listView.setAdapter(adapter);
        return view;
    }
    //ciclo de vida de la aplicación, cuando se inicia la aplicación entonces se inicia el adapter
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }
    //ciclo de vida de la aplicación, cuando se finaliza la aplicación entonces se finaliza  el adapter
    // para que si la aplicación está en segundo plano no consuma recursos
    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
