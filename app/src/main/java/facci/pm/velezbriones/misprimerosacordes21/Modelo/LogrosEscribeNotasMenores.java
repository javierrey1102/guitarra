package facci.pm.velezbriones.misprimerosacordes21.Modelo;

public class LogrosEscribeNotasMenores {
    private String EDoMenor, EReMenor, EMiMenor, ESolMenor, ELaMenor, ESiMenor, ENombreMenor, ENombreMenorImg;

    public LogrosEscribeNotasMenores() {
    }

    public String getEDoMenor() {
        return EDoMenor;
    }

    public void setEDoMenor(String EDoMenor) {
        this.EDoMenor = EDoMenor;
    }

    public String getEReMenor() {
        return EReMenor;
    }

    public void setEReMenor(String EReMenor) {
        this.EReMenor = EReMenor;
    }

    public String getEMiMenor() {
        return EMiMenor;
    }

    public void setEMiMenor(String EMiMenor) {
        this.EMiMenor = EMiMenor;
    }

    public String getESolMenor() {
        return ESolMenor;
    }

    public void setESolMenor(String ESolMenor) {
        this.ESolMenor = ESolMenor;
    }

    public String getELaMenor() {
        return ELaMenor;
    }

    public void setELaMenor(String ELaMenor) {
        this.ELaMenor = ELaMenor;
    }

    public String getESiMenor() {
        return ESiMenor;
    }

    public void setESiMenor(String ESiMenor) {
        this.ESiMenor = ESiMenor;
    }

    public String getENombreMenor() {
        return ENombreMenor;
    }

    public void setENombreMenor(String ENombreMenor) {
        this.ENombreMenor = ENombreMenor;
    }

    public String getENombreMenorImg() {
        return ENombreMenorImg;
    }

    public void setENombreMenorImg(String ENombreMenorImg) {
        this.ENombreMenorImg = ENombreMenorImg;
    }
}