package facci.pm.velezbriones.misprimerosacordes21.Fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosEscribeNotasMenores;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosEscribeNotasMenores7;
import facci.pm.velezbriones.misprimerosacordes21.R;


public class EscribeLogrosMenoresSiete extends Fragment {
    //Instanciamos cada objeto de la clase privada ListView y FirebaseListAdapter
    private ListView listView;
    private FirebaseListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_escribe_logros_menores_siete, container, false);

        listView = (ListView) view.findViewById(R.id.LVMostrarNotasMenoresSieteFragmento);
        //obtenemos el nombre de la tabla de donde queremos extraer los datos
        final DatabaseReference query = FirebaseDatabase.getInstance().getReference().child("ESCRIBE_LOGROS_MENORES_7");
        // creamos una instancia "FirebaseListOptions" que se implementa en una lista "logrosMenoresFirebaseListOptions"
        // a esa lista le pasamos una clase que son todos los items que contiene la tabla "LogrosMenores"
        // todos esos datos se guardará en un XML "logrosmenores" después le pasamos la referencia de la base de datos mas la clase "logrosmenores"
        final FirebaseListOptions<LogrosEscribeNotasMenores7> logrosMenoresSieteFirebaseListOptions =
                new FirebaseListOptions.Builder<LogrosEscribeNotasMenores7>()
                        .setLayout(R.layout.logros_menores_siete)
                        .setQuery(query, LogrosEscribeNotasMenores7.class).build();

        // inicializamos el "FirebaseListAdapter" y le pasamos la instancia "FirebaseListOptions"
        adapter = new FirebaseListAdapter(logrosMenoresSieteFirebaseListOptions) {

            //Inicializamos todos los campos que estén en el XML y le pasamos la vista "v" y por último se setea los campos en el layout.
            @Override
            protected void populateView(View v, Object model, int position) {

                TextInputEditText EdoMenor7 = (TextInputEditText) v.findViewById(R.id.EDoMenor7);
                TextInputEditText EreMenor7 = (TextInputEditText) v.findViewById(R.id.EReMenor7);
                TextInputEditText EmiMenor7 = (TextInputEditText) v.findViewById(R.id.EMiMenor7);
                TextInputEditText EsolMenor7 = (TextInputEditText) v.findViewById(R.id.ESolMenor7);
                TextInputEditText ElaMenor7 = (TextInputEditText) v.findViewById(R.id.ELaMenor7);
                TextInputEditText EsiMenor7 = (TextInputEditText) v.findViewById(R.id.ESiMenor7);
                TextInputEditText EnombreMenor7 = (TextInputEditText) v.findViewById(R.id.EscribeNombreUserMenor7);

                LogrosEscribeNotasMenores7 logrosEscribeNotasMenores7 = (LogrosEscribeNotasMenores7) model;
                EdoMenor7.setText(logrosEscribeNotasMenores7.getEDoMenor7());
                EreMenor7.setText(logrosEscribeNotasMenores7.getEReMenor7());
                EmiMenor7.setText(logrosEscribeNotasMenores7.getEMiMenor7());
                EsolMenor7.setText(logrosEscribeNotasMenores7.getESolMenor7());
                ElaMenor7.setText(logrosEscribeNotasMenores7.getELaMenor7());
                EsiMenor7.setText(logrosEscribeNotasMenores7.getESiMenor7());
                EnombreMenor7.setText(logrosEscribeNotasMenores7.getENombreMenor7());

            }
        };
        listView.setAdapter(adapter);
        return view;

    }
    //ciclo de vida de la aplicación, cuando se inicia la aplicación entonces se inicia el adapter
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }
    //ciclo de vida de la aplicación, cuando se finaliza la aplicación entonces se finaliza  el adapter
    // para que si la aplicación está en segundo plano no consuma recursos
    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }


}

