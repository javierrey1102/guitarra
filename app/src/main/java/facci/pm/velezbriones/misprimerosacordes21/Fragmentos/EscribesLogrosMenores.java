package facci.pm.velezbriones.misprimerosacordes21.Fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import facci.pm.velezbriones.misprimerosacordes21.Clases.Sesion;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosEscribeNotasMenores;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosEscribesNotasMayores;
import facci.pm.velezbriones.misprimerosacordes21.R;


public class EscribesLogrosMenores extends Fragment {
    //Instanciamos cada objeto de la clase privada ListView y FirebaseListAdapter
    private ListView listView;
    private FirebaseListAdapter adapter;

    private Sesion sesion;
    private HashMap<String, String> user;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_escribes_logros_menores, container, false);
        listView = (ListView) view.findViewById(R.id.LVMostrarNotasMenoresFragmento);


        sesion = new Sesion(getActivity());
        user = sesion.LoggedInUser();



        final String id = user.get(sesion.USER_ID);
        final String referencia = "ESCRIBE_LOGROS_MENORES";

        //obtenemos el nombre de la tabla de donde queremos extraer los datos
        final DatabaseReference query = FirebaseDatabase.getInstance().getReference().child(referencia).child(id).child(referencia + " - " + id);
        // creamos una instancia "FirebaseListOptions" que se implementa en una lista "logrosMenoresFirebaseListOptions"
        // a esa lista le pasamos una clase que son todos los items que contiene la tabla "LogrosMenores"
        // todos esos datos se guardará en un XML "logrosmenores" después le pasamos la referencia de la base de datos mas la clase "logrosmenores"
        final FirebaseListOptions<LogrosEscribeNotasMenores> logrosMenoresFirebaseListOptions =
                new FirebaseListOptions.Builder<LogrosEscribeNotasMenores>()
                .setLayout(R.layout.logros_menores)
                .setQuery(query, LogrosEscribeNotasMenores.class).build();

        // inicializamos el "FirebaseListAdapter" y le pasamos la instancia "FirebaseListOptions"
        adapter = new FirebaseListAdapter(logrosMenoresFirebaseListOptions) {

            //Inicializamos todos los campos que estén en el XML y le pasamos la vista "v" y por último se setea los campos en el layout.
            @Override
            protected void populateView(View v, Object model, int position) {

                final TextInputEditText doMenorR = (TextInputEditText) v.findViewById(R.id.DoMenorR);
                final TextInputEditText reMenorR = (TextInputEditText) v.findViewById(R.id.ReMenorR);
                final TextInputEditText miMenorR = (TextInputEditText) v.findViewById(R.id.MiMenorR);
                final TextInputEditText solMenorR = (TextInputEditText) v.findViewById(R.id.SolMenorR);
                final TextInputEditText laMenorR = (TextInputEditText) v.findViewById(R.id.LaMenorR);
                final TextInputEditText siMenorR = (TextInputEditText) v.findViewById(R.id.SiMenorR);
                final TextView name = (TextView) v.findViewById(R.id.TVLgrosMenores);
                final ImageView userfoto = (ImageView)v.findViewById(R.id.IMGLogrosMenores);

                LogrosEscribeNotasMenores logrosEscribeNotasMenores = (LogrosEscribeNotasMenores) model;
                doMenorR.setText(logrosEscribeNotasMenores.getEDoMenor());
                reMenorR.setText(logrosEscribeNotasMenores.getEReMenor());
                miMenorR.setText(logrosEscribeNotasMenores.getEMiMenor());
                solMenorR.setText(logrosEscribeNotasMenores.getESolMenor());
                laMenorR.setText(logrosEscribeNotasMenores.getELaMenor());
                siMenorR.setText(logrosEscribeNotasMenores.getESiMenor());
                name.setText(logrosEscribeNotasMenores.getENombreMenor());


                int numero = Integer.valueOf(logrosEscribeNotasMenores.getENombreMenorImg());
                if (numero == 1){
                    userfoto.setImageResource(R.drawable.fantasma1);
                }if (numero == 2){
                    userfoto.setImageResource(R.drawable.fantasma2);
                }if (numero == 3){
                    userfoto.setImageResource(R.drawable.fantasma3);
                }if (numero == 4){
                    userfoto.setImageResource(R.drawable.fantasma4);
                }if (numero == 5){
                    userfoto.setImageResource(R.drawable.fantasma5);
                }if (numero == 6){
                    userfoto.setImageResource(R.drawable.fantasma6);
                }if (numero == 7){
                    userfoto.setImageResource(R.drawable.fantasma7);
                }if (numero == 8){
                    userfoto.setImageResource(R.drawable.fantasma8);
                }if (numero == 9){
                    userfoto.setImageResource(R.drawable.fantasma9);
                }if (numero == 10){
                    userfoto.setImageResource(R.drawable.fantasma10);
                }if (numero == 11){
                    userfoto.setImageResource(R.drawable.fantasma11);
                }if (numero == 12){
                    userfoto.setImageResource(R.drawable.fantasma12);
                }if (numero == 13){
                    userfoto.setImageResource(R.drawable.fantasma13);
                }if (numero == 14){
                    userfoto.setImageResource(R.drawable.fantasma14);
                }if (numero == 15){
                    userfoto.setImageResource(R.drawable.fantasma15);
                }


            }
        };
        listView.setAdapter(adapter);
        return view;

    }
    //ciclo de vida de la aplicación, cuando se inicia la aplicación entonces se inicia el adapter
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }
    //ciclo de vida de la aplicación, cuando se finaliza la aplicación entonces se finaliza  el adapter
    // para que si la aplicación está en segundo plano no consuma recursos
    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }


}
