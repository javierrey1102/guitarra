package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores;

public class SpinnerItemCuatroMayorMayor {
    private String NombreItemCuatroMayor;
    private int ImagenItemCuatroMayor;

    public SpinnerItemCuatroMayorMayor (String nombreItemCuatroMayor, int imagenItemCuatroMayor){
        this.NombreItemCuatroMayor = nombreItemCuatroMayor;
        this.ImagenItemCuatroMayor = imagenItemCuatroMayor;
    }

    public String getNombreItemCuatroMayor() {
        return NombreItemCuatroMayor;
    }

    public int getImagenItemCuatroMayor() {
        return ImagenItemCuatroMayor;
    }
}
