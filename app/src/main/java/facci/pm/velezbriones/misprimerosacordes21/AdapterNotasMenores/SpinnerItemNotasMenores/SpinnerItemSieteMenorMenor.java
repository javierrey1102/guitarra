package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores.SpinnerItemNotasMenores;

public class SpinnerItemSieteMenorMenor {
    private String NombreItemSieteMenor;
    private int ImagenItemSieteMenor;

    public SpinnerItemSieteMenorMenor (String nombreItemSieteMenor, int imagenItemSieteMenor){
        this.NombreItemSieteMenor = nombreItemSieteMenor;
        this.ImagenItemSieteMenor = imagenItemSieteMenor;
    }

    public String getNombreItemSieteMenor() {
        return NombreItemSieteMenor;
    }

    public int getImagenItemSieteMenor() {
        return ImagenItemSieteMenor;
    }
}
