package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores.SpinnerItemSeisMayorMayor;
import facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.AdapterSpinnerSeisMenor;
import facci.pm.velezbriones.misprimerosacordes21.R;

public class AdapterSpinnerSeisMayorMayor extends ArrayAdapter<SpinnerItemSeisMayorMayor> {
    public AdapterSpinnerSeisMayorMayor (Context context, ArrayList<SpinnerItemSeisMayorMayor> spinnerItemSeisMayorMayors){
        super(context,0, spinnerItemSeisMayorMayors);
    }
    @NonNull
    //se crea por defecto, no se modifica el código
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    //se crea por defecto, no se modifica el código
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    // también se crea por defecto pero le hacemos algunos ajustes
    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            // como la vista no tiene nada entonces le pasamos el XML
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_uno, parent, false
            );
        }
        // Inicializamos todos los campos que tiene el layout-XML  y a esos campos hay que convertirlos en (convertView)
        ImageView imageView = (ImageView)convertView.findViewById(R.id.IMGSPUnoMenor);
        TextView textView = (TextView)convertView.findViewById(R.id.LBLSPUnomenor);
        SpinnerItemSeisMayorMayor spinnerItemSeisMayorMayor = getItem(position);
        if (spinnerItemSeisMayorMayor != null){
        imageView.setImageResource(spinnerItemSeisMayorMayor.getImagenItemSeisMayor());
        textView.setText(spinnerItemSeisMayorMayor.getNombreItemSeisMayor());
        }
        return convertView;
    }
    }
