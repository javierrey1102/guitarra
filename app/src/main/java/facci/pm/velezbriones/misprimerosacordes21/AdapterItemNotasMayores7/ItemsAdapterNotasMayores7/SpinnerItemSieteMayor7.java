package facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7;

public class SpinnerItemSieteMayor7 {
    private String NombreItemMayor7;
    private int ImagenItemMayor7;

    public SpinnerItemSieteMayor7 (String NombreItemMayor, int ImagenItemMayor){
        this.NombreItemMayor7 = NombreItemMayor;
        this.ImagenItemMayor7 = ImagenItemMayor;
    }

    public String getNombreItemMayor7() {
        return NombreItemMayor7;
    }

    public int getImagenItemMayor7() {
        return ImagenItemMayor7;
    }
}
