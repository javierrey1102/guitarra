package facci.pm.velezbriones.misprimerosacordes21.Modelo;

public class LogrosAdivinaNotasMenores7 {
    private String ADoMenor7, AReMenor7, AMiMenor7, AFaMenor7, ASolMenor7, ALaMenor7, ASiMenor7, ANombreMenor7;

    public LogrosAdivinaNotasMenores7(){


    }

    public String getADoMenor7() {
        return ADoMenor7;
    }

    public void setADoMenor7(String ADoMenor7) {
        this.ADoMenor7 = ADoMenor7;
    }

    public String getAReMenor7() {
        return AReMenor7;
    }

    public void setAReMenor7(String AReMenor7) {
        this.AReMenor7 = AReMenor7;
    }

    public String getAMiMenor7() {
        return AMiMenor7;
    }

    public void setAMiMenor7(String AMiMenor7) {
        this.AMiMenor7 = AMiMenor7;
    }

    public String getAFaMenor7() {
        return AFaMenor7;
    }

    public void setAFaMenor7(String AFaMenor7) {
        this.AFaMenor7 = AFaMenor7;
    }

    public String getASolMenor7() {
        return ASolMenor7;
    }

    public void setASolMenor7(String ASolMenor7) {
        this.ASolMenor7 = ASolMenor7;
    }

    public String getALaMenor7() {
        return ALaMenor7;
    }

    public void setALaMenor7(String ALaMenor7) {
        this.ALaMenor7 = ALaMenor7;
    }

    public String getASiMenor7() {
        return ASiMenor7;
    }

    public void setASiMenor7(String ASiMenor7) {
        this.ASiMenor7 = ASiMenor7;
    }

    public String getANombreMenor7() {
        return ANombreMenor7;
    }

    public void setANombreMenor7(String ANombreMenor7) {
        this.ANombreMenor7 = ANombreMenor7;
    }
}
