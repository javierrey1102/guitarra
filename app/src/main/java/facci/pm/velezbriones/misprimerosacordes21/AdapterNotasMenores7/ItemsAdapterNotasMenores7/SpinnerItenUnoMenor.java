package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMenores7.ItemsAdapterNotasMenores7;

public class SpinnerItenUnoMenor {
    //hacemos dos instancia de tipo string y entero
    private String NombreItemMemor;
    private int ImagenItemMenor;

    //Creamos el constructor y le pasamos dos parámetros (string e int) similar a los de arriba para no perder la referencia pero no son iguales.
    public SpinnerItenUnoMenor (String NombreItemMenor, int ImagenItemMenor){
        this.NombreItemMemor = NombreItemMenor;
        this.ImagenItemMenor = ImagenItemMenor;
    }

    //generamos el get de de las instancia del tipo "string" y me retorna el String


    public String getNombreItemMemor() {
        return NombreItemMemor;
    }

    public int getImagenItemMenor() {
        return ImagenItemMenor;
    }
}
