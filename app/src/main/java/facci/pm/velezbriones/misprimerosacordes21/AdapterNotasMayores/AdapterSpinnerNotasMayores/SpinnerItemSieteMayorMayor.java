package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores;

public class SpinnerItemSieteMayorMayor {
    private String NombreItemSieteMayor;
    private int ImagenSieteMayor;

    public SpinnerItemSieteMayorMayor (String nombreItemSieteMayor, int imagenSieteMayor){
        this.NombreItemSieteMayor = nombreItemSieteMayor;
        this.ImagenSieteMayor = imagenSieteMayor;
    }

    public String getNombreItemSieteMayor() {
        return NombreItemSieteMayor;
    }

    public int getImagenSieteMayor() {
        return ImagenSieteMayor;
    }
}
