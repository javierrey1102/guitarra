package facci.pm.velezbriones.misprimerosacordes21.AdapterItemNotasMayores7.ItemsAdapterNotasMayores7;

public class SpinnerItemUnoMayor7 {
    //hacemos dos instancia de tipo string y entero
    private String NombreItemMayor7;
    private int ImagenItemMayor7;

    //Creamos el constructor y le pasamos dos parámetros (string e int) similar a los de arriba para no perder la referencia pero no son iguales.
    public SpinnerItemUnoMayor7 (String NombreItemMayor7, int ImagenItemMayor7){
        this.NombreItemMayor7 = NombreItemMayor7;
        this.ImagenItemMayor7 = ImagenItemMayor7;

    }
    //generamos el get de de las instancia del tipo "string" y me retorna el String

    public String getNombreItemMayor7() {
        return NombreItemMayor7;
    }

    public int getImagenItemMayor7() {
        return ImagenItemMayor7;
    }
}
