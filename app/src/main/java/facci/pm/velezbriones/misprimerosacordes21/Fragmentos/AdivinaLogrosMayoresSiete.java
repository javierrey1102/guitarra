package facci.pm.velezbriones.misprimerosacordes21.Fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosAdivinaNotasMayores;
import facci.pm.velezbriones.misprimerosacordes21.Modelo.LogrosAdivinaNotasMayores7;
import facci.pm.velezbriones.misprimerosacordes21.R;


public class AdivinaLogrosMayoresSiete extends Fragment {
    //Instanciamos cada objeto de la clase privada ListView y FirebaseListAdapter
    private ListView listView;
    private FirebaseListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_adivina_logros_mayores_siete, container, false);
        // a la referencia de listView lo enlazamos con su id
        listView = (ListView) view.findViewById(R.id.LVMostrarNotasAdivinaMayoresSieteFragmento);
        //obtenemos el nombre de la tabla de donde queremos extraer los datos
        final DatabaseReference query = FirebaseDatabase.getInstance().getReference().child("ADIVINA_LOGROS_MAYORES_7");
        // creamos una instancia "FirebaseListOptions" que se implementa en una lista "logrosMayoresFirebaseListOptions"
        // a esa lista le pasamos una clase que son todos los items que contiene la tabla "LogrosAdivinaMayor"
        // todos esos datos se guardará en un XML "logros_adivina_7_mayor" después le pasamos la referencia
        // de la base de datos mas la clase "logros_adivina_7_mayor"
        final FirebaseListOptions<LogrosAdivinaNotasMayores7>
                logrosAdivinaNotasMayoresFirebaseListOptions =
                new FirebaseListOptions.Builder<LogrosAdivinaNotasMayores7>()
                .setLayout(R.layout.logros_adivina_mayores_siete)
                .setQuery(query,LogrosAdivinaNotasMayores7.class).build();
        // inicializamos el "FirebaseListAdapter" y le pasamos la instancia "FirebaseListOptions"
        adapter = new FirebaseListAdapter(logrosAdivinaNotasMayoresFirebaseListOptions) {
            //Inicializamos todos los campos que estén en el XML y le pasamos la vista "v" y por último se setea los campos en el layout.

            @Override
            protected void populateView(View v, Object model, int position) {
                TextInputEditText AdoMayor7 = (TextInputEditText) v.findViewById(R.id.DoMayorAdivina7);
                TextInputEditText AreMayor7= (TextInputEditText) v.findViewById(R.id.ReMayorAdivina7);
                TextInputEditText AmiMayor7 = (TextInputEditText) v.findViewById(R.id.MiMayorAdivina7);
                TextInputEditText AfaMayor7 = (TextInputEditText) v.findViewById(R.id.FaMayorAdivina7);
                TextInputEditText AsolMayor7 = (TextInputEditText) v.findViewById(R.id.SolMayorAdivina7);
                TextInputEditText AlaMayor7= (TextInputEditText) v.findViewById(R.id.LaMayorAdivina7);
                TextInputEditText AsiMayor7 = (TextInputEditText) v.findViewById(R.id.SiMayorAdivina7);
                TextInputEditText AnombresUMayor7 = (TextInputEditText) v.findViewById(R.id.NombreUserAdivinaNotasMayor7);

                LogrosAdivinaNotasMayores7 logrosAdivinaMayor7 = (LogrosAdivinaNotasMayores7) model;
                AdoMayor7.setText(logrosAdivinaMayor7.getADoMayor7());
                AreMayor7.setText(logrosAdivinaMayor7.getAReMayor7());
                AmiMayor7.setText(logrosAdivinaMayor7.getAMiMayor7());
                AfaMayor7.setText(logrosAdivinaMayor7.getAFaMayor7());
                AsolMayor7.setText(logrosAdivinaMayor7.getASolMayor7());
                AlaMayor7.setText(logrosAdivinaMayor7.getALaMayor7());
                AsiMayor7.setText(logrosAdivinaMayor7.getASiMayor7());
                AnombresUMayor7.setText(logrosAdivinaMayor7.getANombreMayor7());


            }
        };

        listView.setAdapter(adapter);
        return view;
    }

    //ciclo de vida de la aplicación, cuando se inicia la aplicación entonces se inicia el adapter
    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();

    }
    //ciclo de vida de la aplicación, cuando se finaliza la aplicación entonces se finaliza  el adapter
    // para que si la aplicación está en segundo plano no consuma recursos
    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}

