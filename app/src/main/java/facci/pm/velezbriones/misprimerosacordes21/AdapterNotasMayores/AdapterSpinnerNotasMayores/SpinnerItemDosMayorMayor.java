package facci.pm.velezbriones.misprimerosacordes21.AdapterNotasMayores.AdapterSpinnerNotasMayores;

public class SpinnerItemDosMayorMayor {
    private String NombreItemDosMayor;
    private int ImagenItemDosMayor;

public SpinnerItemDosMayorMayor (String nombreItemDosMayor, int imagenItemDosMayor){
    this.NombreItemDosMayor = nombreItemDosMayor;
    this.ImagenItemDosMayor = imagenItemDosMayor;
}

    public String getNombreItemDosMayor() {
        return NombreItemDosMayor;
    }

    public int getImagenItemDosMayor() {
        return ImagenItemDosMayor;
    }
}
